<?php  if ( !defined('BASEPATH')) exit('No direct script access allowed');

class Auth{
    /**
     * Constructor
     */
    function __construct(){
        $this->ci =& get_instance();
        $this->ci->load->database();
        $this->ci->load->library('session');
        $this->ci->load->library('cart');
		$this->ci->load->model('m_auth');
		$this->ci->load->model('m_crud');
    }

	/**
	 * Log In Action
	 *
	 * @param   string      username
	 * @param   string      password
	 * @return  mixed       FALSE on failure, user_id on success
	 */
	public function login($username, $password, $crtz){
		$result = $this->ci->m_auth->login($username, $password);
		if($result != false) {
			$session_array = array();
			foreach($result as $row) {
				$session_array = array(
							'user_authority' 	=> $row->user_authority,
							'user_id' 	=> $row->user_id,
							'email'		=> $row->user_email,
							'token'		=> $this->keys($row->user_id,$row->user_password),
							'login'		=>TRUE,
							'crtz'		=>$crtz,
				);
				$this->ci->session->set_userdata($session_array);
				$id['user_id'] = $row->user_id;
				$data=array(
					'key_access'    => $this->keys($row->user_id,$row->user_password)
				);
				$this->ci->m_crud->update('tm_user', 'user_id', $data, $id['user_id']);
			}
			return true;
			
		}
		return false;
	}

	public function logout($slug){
	  	$id['id'] = $slug; 
      	$this->ci->session->sess_destroy();
      	return true;
	}

  	public function keys($id,$password){
     	$key = hash('sha256',$id.$password.$id.time());
     	return $key;
  	}

}
