<?php
/**
 * Validating for processing users get database
 */
class Processing
{

  function __construct()
  {
       $this->ci =& get_instance();
       $this->ci->load->database();
       $this->ci->load->library('auth');
       $this->ci->load->model('m_public');
       $this->ci->load->model('m_auth');
       
       /** Employeed Models **/
       $this->ci->load->model('m_users');
       $this->ci->load->model('m_premission');
       $this->ci->load->model('m_vendor');
       $this->ci->load->model('m_pelanggan');

       /** Keuangan Models **/
       $this->ci->load->model('m_hutang');

       /** Pengiriman Models **/
       $this->ci->load->model('m_pemesanan');
       $this->ci->load->model('m_kendaraan');
       $this->ci->load->model('m_supir');
        
       /** Pesanan Models **/
       $this->ci->load->model('m_biaya');

  }

  /** Function Public **/

  public function getData($token,$table)
  {
     if($this->ci->m_auth->SearchKeys($token)){
       return $this->ci->m_public->getData($table);
     }
  }

  public function getDataLimit($token,$table,$limit)
  {
     if($this->ci->m_auth->SearchKeys($token)){
       return $this->ci->m_public->getDataLimit($table,$limit);
     }
  }

  public function getDataByOrders($token,$table,$order,$by)
  {
     if($this->ci->m_auth->SearchKeys($token)){
       return $this->ci->m_public->getDataOrdersBy($table,$order,$by);
     }
  }

  public function getDataByOrdersWhere($token,$table,$order,$by,$where)
  {
     if($this->ci->m_auth->SearchKeys($token)){
       return $this->ci->m_public->getDataOrdersByWhere($table,$order,$by,$where);
     }
  }

  public function getDataWhere($token,$table,$data)
  {
     if($this->ci->m_auth->SearchKeys($token)){
       return $this->ci->m_public->getDataWhere($table,$data);
     }
  }

  
  
  public function StoreData($store,$to,$token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
      $insert = $this->ci->db->insert($to,$store);
      if($insert){
        return TRUE;
      }else{
        return FALSE;
      }
    }
  }

  public function PutData($put,$to,$field_key,$token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
      $insert = $this->ci->db->update($to,$put,$field_key);
      if($insert){
        return TRUE;
      }else{
        return FALSE;
      }
    }
  }

  public function DeleteData($token,$table,$select)
  {
    if($this->ci->m_auth->SearchKeys($token)){
      $delete = $this->ci->db->delete($table,$select);
      if($delete){
        return TRUE;
      }else{
        return FALSE;
      }
    }
  }

  public function DataValid($data,$to,$token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
      return $this->ci->m_public->getValidation($to,$data);
    }
  }

  /** End Function Public **/

  /** Employeed Functions **/

  public function getUsers($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_users->getTableUsersLevel();
    }
  }

  public function getPremissionLevels($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_premission->getTablePremissionLevels();
    }
  }

  public function getVendorUsers($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_vendor->getTableVendorUsers();
    }
  }

  public function getVendorUsersVendor($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_vendor->getTableUsersLevelVendor();
    }
  }

  public function getPelangganUsers($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pelanggan->getTablePelangganUsers();
    }
  }

  public function getPelangganUsersVendor($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pelanggan->getTableUsersLevelPelanggan();
    }
  }

  public function CodeVendor($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_vendor->CodeVendor();
    }
  }

  public function CodePelanggan($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pelanggan->CodePelanggan();
    }
  }

  public function CodeAwb($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->CodeAwb();
    }
  }

  public function getDataUpdateStatusPengelola($token,$slug)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getDataUpdateStatusPengelola($slug);
    }
  }

  

  /** Pesanan Functions **/
  
  public function getDataBiaya($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_biaya->getTableBiayaKota();
    }
  }
  
   public function getDataVerifPelanggan($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pelanggan->getDataVerifPelanggan();
    }
  }

  public function getDataKendaraan($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_kendaraan->getTableKendaraanVendor();
    }
  }
  
 

  public function getDataPesanan($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getTableDaftarPesanan($id);
    }
  }

  public function getDataPengiriman($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getTablePengiriman($id);
    }
  }

   public function getDataPengirimanVendor($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getTablePengirimanVendor($id);
    }
  }
  
  public function getDataPembayaran($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->getTablePembayaran($id);
    }
  }

  public function getDataPiutang($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->getTablePiutang($id);
    }
  }

  public function CodePesanan($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->CodePesanan();
    }
  }

  public function getDataPesananKota($token,$slug)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getDataPesananKota($slug);
    }
  }

  public function getDataPesanans($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getDataPesanans();
    }
  }
  
     public function getDataDetailPesanan($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getDataDetailPesanan($id);
    }
  }


  public function getDataPesananKendaraan($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_kendaraan->getDataPesananKendaraan();
    }
  }

  public function getDataPesananSupir($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_supir->getDataPesananSupir();
    }
  }

  public function getDataTagihanDetails($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->getDataTagihanDetail($id);
    }
  }
  
 
   public function getDataTagihanVendorDetails($token,$id)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->getDataTagihanVendorDetail($id);
    }
  }
  
    public function getDataTagihanAll($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->getDataTagihanAll();
    }
  }

  public function getDataOptionsPesanan($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getDataOptionsPesanan();
    }
  }

  public function getDataOptionsKeuangan($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_pemesanan->getDataOptionsKeuangan();
    }
  }

  public function CodeInvoice($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->CodeInvoice();
    }
  }

  public function LaporanVendor($token,$slug1,$slug2)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->LaporanVendor($slug1,$slug2);
    }
  }

  public function LaporanPelanggan($token,$slug1,$slug2)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->LaporanPelanggan($slug1,$slug2);
    }
  }

  public function PelangganInvoice($token)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->PelangganInvoice();
    }
  }

  public function getPesananPelanggan($token,$slug)
  {
    if($this->ci->m_auth->SearchKeys($token)){
        return $this->ci->m_hutang->getPesananPelanggan($slug);
    }
  }

  



} ?>
