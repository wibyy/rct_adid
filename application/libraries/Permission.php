<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permission
{
    /**
     * Constructor
     */
    function __construct()
    {
         $this->ci =& get_instance();
         $this->ci->load->database();
         $this->ci->load->library('session');
 		     $this->ci->load->model('m_auth');
         $this->ci->load->model('m_premission');
    }

    private function keys(){
      return $this->ci->session->userdata('token');
    } 

    public function PermissionUsers($slug)
    {
      $access = $this->ci->m_auth->SearchKeys(self::keys());
      if($access){
        $result = $this->ci->m_auth->permission($slug);

        if($result == true) {
          return $result->result();
        }
      }
    }

    public function Page($slug)
    {
      $access = $this->ci->m_auth->SearchKeys(self::keys());
      if($access){
        $result = $this->ci->m_premission->page($slug);

        if($result) {
           return TRUE;
        }else{
           return FALSE; 
        }
      }
    }

    public function menu($slug)
    {
      $access = $this->ci->m_auth->SearchKeys(self::keys());
      if($access){
        $result = $this->ci->m_premission->main_menu($slug);

        if($result == true) {
           return $result;
        }
      }
    }

    public function submenu($slug)
    { 
      $access = $this->ci->m_auth->SearchKeys(self::keys());
      if($access){
        $result = $this->ci->m_premission->sub_menu($slug);

        if($result == true) {
           return $result;
        }
      }
    }

    public function access($slug,$slug2,$slug3)
    { 
      $access = $this->ci->m_auth->SearchKeys(self::keys());
      if($access){
        $result = $this->ci->m_premission->access($slug,$slug2,$slug3);

        if($result == true) {
           return $result;
        }else{
          return redirect('Oauth/logout');
        }
      }
    }

}
