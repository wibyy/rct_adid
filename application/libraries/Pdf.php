<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use Dompdf\Dompdf;
use Dompdf\Options;

class Pdf {
	function pdf_create($html, $filename, $paper, $orientation, $stream=TRUE){

		$options = new Options();
		$options->set('defaultFont', 'Nunito');
		$options->set('isRemoteEnabled', TRUE);
		$options->set('debugKeepTemp', TRUE);
		$options->set('isHtml5ParserEnabled', true);
 
		$dompdf = new Dompdf($options);

	    $dompdf->load_html($html);//Load HTML File untuk dirender
	    $dompdf->set_paper($paper, $orientation);
	    $dompdf->render();//Proses Rendering File
	    if ($stream) {
				//$stream($filename.".pdf");
				$dompdf->stream($filename.".pdf",array("Attachment"=>0));
	    } else {
				$CI =& get_instance();
				$CI->load->helper('file');
				write_file($filename, $dompdf->output());//file name adalah ABSOLUTE PATH dari tempat menyimpan file PDF
	    }
	}
}
