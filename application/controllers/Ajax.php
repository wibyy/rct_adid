<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{

	private $page 					= '';
	private $ID_USER 				= '';
	private $USER_INFO_API 			= [];
	private $USER_PERMISSION_API 	= '';
	private $ACCESS_PAGE_API 		= '';
	private $IS_USER 				= '';
	private $IS_EO_USER 			= false;
	private $IS_SUPER_USER 			= false;
	private $IS_RCT_USER 			= false;
	private $IS_MANAGER_USER 		= false;
	private $EO 					= false;

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata("sign_in") == TRUE) {

			$this->config->load('rct_config');
			$this->USER_INFO_API 		= $this->session->userdata();

			$this->USER_API  			= getUserGroup([$this->USER_INFO_API['user_group']]);
			$this->IS_USER 				= key($this->USER_API);
			$this->IS_SUPER_USER 		= (in_array($this->IS_USER, $this->config->item('super_user'))) ? true : false;
			$this->IS_RCT_USER 			= (in_array($this->IS_USER, $this->config->item('rct_user'))) ? true : false;
			$this->IS_EO_USER 			= (in_array($this->IS_USER, $this->config->item('eo_user'))) ? true : false;
			$this->IS_MANAGER_USER 		= (in_array($this->IS_USER, $this->config->item('regular_user'))) ? true : false;
			$this->USER_PERMISSION_API  = $this->config->item('user_group')[$this->IS_USER]['access'];

			$this->load->helper('status');
			$this->load->model('m_crud');
			$this->load->model('m_global');
			$this->load->model('m_rct');

			$this->ID_USER = simple_decrypt($this->USER_INFO_API['id']);

			if ($this->IS_EO_USER) {
				$q 	= "SELECT eo.* FROM event_organizer eo JOIN users ON users.id_eo = eo.id WHERE users.id_users = '" . $this->ID_USER . "' AND eo.status = 1";
				$r	= $this->db->query($q)->result();
				$this->EO	= ($r) ? $r[0] : false;
			}
		} else {
			get_redirecting('auth');
		}
	}

	//EVENT LIST - PERMISSION 
	public function get_event()
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';
		$this->page = 'event';

		$this->ACCESS_PAGE_API		= (isset($this->USER_PERMISSION_API[$this->page])) ? $this->USER_PERMISSION_API[$this->page] : false;

		$access_list = (in_array('list', $this->ACCESS_PAGE_API)) ? true : false;

		if ($access_list) {
			$label 		= $this->input->get('label');
			$id 		= $this->input->get('id');
			$sort 		= $this->input->get('sort');
			$status 	= $this->input->get('status');
			$date 		= $this->input->get('date');
			$limit 		= $this->input->get('limit');
			$page 		= $this->input->get('page');
			$class 		= $this->input->get('class');

			$query = "SELECT * FROM event WHERE  1=1 ";

			if ($class && !$id) {
				$query              = "SELECT event.* FROM event JOIN kelas ON kelas.id_event = event.id WHERE kelas.id = '" . $class . "' ";
			}
			if ($this->IS_MANAGER_USER) {
				$query              = "SELECT 
											event.* 
										FROM 
											registrations
										JOIN kategori ON registrations.id_kategori = kategori.id
										JOIN kelas ON kelas.id = kategori.id_kelas
										JOIN event ON event.id = kelas.id_event
										WHERE 
											registrations.id_users = '" . $this->ID_USER . "' 
										";
			}
			if ($this->EO) {
				$query .= " and event.id_eo = '" . $this->EO->id . "'";
			}
			if ($label) {
				$query .= " and event.label like '%" . $label . "%'";
			}
			if ($id) {
				$query .= " and event.id = '" . $id . "'";
			}
			if ($status != '' && $status != 'ALL') {
				$query .= " and event.status = '" . $status . "'";
			}
			if ($date) {
				$query .= " and date(event.date_start) <= '" . $date . "' and date(event.date_end) >= '" . $date . "'";
			}
			if ($this->IS_MANAGER_USER) {
				$query  .= ' GROUP BY event.id';
			}

			$total_all 	= $this->db->query($query)->num_rows();

			if ($sort == 'asc') {
				$query .= " order by event.created_at ASC ";
			} else {
				$query .= " order by event.created_at DESC ";
			}

			if ($limit) {
				$page = ($page) ? intval($page) : 1;
				$limit = ($limit) ? intval($limit) : 10;
				$offset = ($page) ? (($page - 1) * $limit) : 0;
				$query .= " LIMIT " . $limit . " OFFSET " . $offset;
			}

			$total 				= $this->db->query($query)->num_rows();
			$query_result      	= $this->db->query($query)->result();



			foreach ($query_result as $idx => $row) {

				unset($query_result[$idx]->created_at);
				unset($query_result[$idx]->updated_at);

				$query_result[$idx]->date_start 	= date('Y-m-d', strtotime($row->date_start));
				$query_result[$idx]->date_end 	= date('Y-m-d', strtotime($row->date_end));
				if ($row->logo == '') {
					$query_result[$idx]->logo_thumb 	= base_url('assets/uploads/no_image.png');
					$query_result[$idx]->logo 	= base_url('assets/uploads/no_image.png');
				} else {
					$query_result[$idx]->logo_thumb 	= base_url('assets/uploads/images/thumb-144/' . $row->logo);
					$query_result[$idx]->logo 	= base_url('assets/uploads/images/' . $row->logo);
				}

				$query_class = "SELECT * FROM kelas WHERE  id_event = '" . $row->id . "' ";
				if ($class) {
					$query_class .= " and id = '" . $class . "'";
				}
				$t 		    = $this->db->query($query_class)->num_rows();
				$res      	= $this->db->query($query_class)->result();

				foreach ($res as $i => $v) {
					if ($v->logo == '')
						$res[$i]->logo 	= base_url('assets/uploads/no_image.png');
					else
						$res[$i]->logo 	= base_url('assets/uploads/images/thumb-144/' . $v->logo);

					unset($res[$i]->created_at);
					unset($res[$i]->updated_at);
					unset($res[$i]->id_events);

					$query_class_c      = "SELECT * FROM kategori WHERE  id_kelas = '" . $v->id . "' ";
					$t_c 		        = $this->db->query($query_class_c)->num_rows();
					$res_c      	    = $this->db->query($query_class_c)->result();
					foreach ($res_c as $i_c => $v_c) {
						unset($res_c[$i_c]->created_at);
						unset($res_c[$i_c]->updated_at);
						unset($res_c[$i_c]->id_kelas);
					}
					$res[$i]->kategori  = $res_c;
				}
				$query_result[$idx]->kelas = $res;
			}

			if (!!$query_result) {
				if ($limit) {
					$response = list_response_map($query_result, $total_all, $page, $total, $limit);
				} else {
					$response = list_response($query_result, $total_all);
				}
			} else {
				$response['message'] 	= "No data result!";
			}
		} else {
			$response['message'] 	= "No access to request!";
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	public function get_event_bycode($code)
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$query = "SELECT * FROM event WHERE  event_code = '" . $code . "' ";

		$query_result      	= $this->db->query($query)->result();

		foreach ($query_result as $idx => $row) {

			unset($query_result[$idx]->created_at);
			unset($query_result[$idx]->updated_at);

			$query_result[$idx]->date_start 	= date('Y-m-d', strtotime($row->date_start));
			$query_result[$idx]->date_end 	= date('Y-m-d', strtotime($row->date_end));
			if ($row->logo == '') {
				$query_result[$idx]->logo_thumb 	= base_url('assets/uploads/no_image.png');
				$query_result[$idx]->logo 	= base_url('assets/uploads/no_image.png');
			} else {
				$query_result[$idx]->logo_thumb 	= base_url('assets/uploads/images/thumb-144/' . $row->logo);
				$query_result[$idx]->logo 	= base_url('assets/uploads/images/' . $row->logo);
			}

			$query_class = "SELECT * FROM kelas WHERE  id_event = '" . $row->id . "' ";

			$t 		    = $this->db->query($query_class)->num_rows();
			$res      	= $this->db->query($query_class)->result();

			foreach ($res as $i => $v) {
				if ($v->logo == '')
					$res[$i]->logo 	= base_url('assets/uploads/no_image.png');
				else
					$res[$i]->logo 	= base_url('assets/uploads/images/thumb-144/' . $v->logo);

				unset($res[$i]->created_at);
				unset($res[$i]->updated_at);
				unset($res[$i]->id_events);

				$query_class_c      = "SELECT * FROM kategori WHERE  id_kelas = '" . $v->id . "' ";
				$t_c 		        = $this->db->query($query_class_c)->num_rows();
				$res_c      	    = $this->db->query($query_class_c)->result();
				foreach ($res_c as $i_c => $v_c) {
					unset($res_c[$i_c]->created_at);
					unset($res_c[$i_c]->updated_at);
					unset($res_c[$i_c]->id_kelas);
				}
				$res[$i]->kategori  = $res_c;
			}
			$query_result[$idx]->kelas = $res;
		}

		if (!!$query_result) {
			$response = response_map($query_result[0]);
		} else {
			$response['message'] 	= "No event found!";
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//EVENT NEW -  
	public function set_event()
	{
		$response['status'] 	= 'error';
		$response['message'] = '';

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		if ($this->IS_EO_USER || $this->IS_RCT_USER || $this->IS_SUPER_USER) {

			$this->form_validation->set_rules('name', 'Name', 'required');
			$this->form_validation->set_rules('date_start', 'Date Start', 'required');
			$this->form_validation->set_rules('date_end', 'Date End', 'required');
			$this->form_validation->set_rules('location', 'Location', 'required');

			if (!$this->EO)
				$this->form_validation->set_rules('id_eo', 'ID Event Organizer', 'required');

			if (!!$this->form_validation->run()) {

				$name 				= $this->input->post('name');
				$sub_name 			= $this->input->post('sub_name');
				$date_start 		= $this->input->post('date_start');
				$date_end 			= $this->input->post('date_end');
				$location 			= $this->input->post('location');
				$status 			= $this->input->post('status');
				$id_eo				= (!$this->EO) ? $this->input->post('id_eo') : $this->EO->id;
				$upload				= '';

				if (isset($_FILES["image"]["name"])) {
					$upload = $this->m_global->uploadfile('image', '././././assets/uploads/images/', 'gif|jpg|jpeg|png');
				}

				$singkatan = '';
				$arr = explode(' ', $name);
				foreach ($arr as $kata) {
					$singkatan .= substr($kata, 0, 1);
				}

				$event_code = $singkatan . date("dmy");

				$data = array(
					'label' 				=> $name,
					'id_eo' 				=> $id_eo,
					'event_code' 			=> $event_code,
					'sub_label' 			=> $sub_name,
					'date_start' 			=> $date_start,
					'date_end' 				=> $date_end,
					'lokasi' 				=> $location,
					'status' 				=> ($status) ? $status : 1,
					'logo' 					=> $upload
				);

				$query = $this->m_crud->insert('event', $data);

				if (!!$query) {
					$response['status'] 	= 'success';
					$response['message'] = 'Success insert data!';
				} else {
					$response['message'] = 'Failed to insert data!';
				}
			} else {
				$response['message'] = validation_errors();
			}
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	//EVENT EDIT -  
	public function update_event()
	{
		$response['status'] 	= 'error';
		$response['message'] = '';
		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('date_start', 'Date Start', 'required');
		$this->form_validation->set_rules('date_end', 'Date End', 'required');
		$this->form_validation->set_rules('location', 'Location', 'required');

		if (!!$this->form_validation->run()) {
			$id 				= $this->input->post('id');
			$name 				= $this->input->post('name');
			$sub_name 			= $this->input->post('sub_name');
			$date_start 		= $this->input->post('date_start');
			$date_end 			= $this->input->post('date_end');
			$location 			= $this->input->post('location');
			$status 			= 1;
			$upload				= false;
			$image_old			= false;

			$get_old 			= $this->db->query("SELECT logo FROM events WHERE id ='$id' ");
			$row 				= $get_old->row();
			if ($row->logo != '') {
				$image_old 			= $row->logo;
			}

			$data = array(
				'name' 					=> $name,
				'sub_name' 				=> $sub_name,
				'date_start' 			=> $date_start,
				'date_end' 				=> $date_end,
				'location' 				=> $location,
				'status' 				=> $status
			);

			if (isset($_FILES["imageedit"]["name"])) {
				$upload = $this->m_global->uploadfile('imageedit', '././././assets/uploads/images/', 'gif|jpg|jpeg|png');
				if (!!$upload) {
					$data['logo'] = $upload;
				}
			}

			$query = $this->m_crud->update('events', 'id', $data, $id);

			if (!!$query) {
				if (!!$upload && !!$image_old) {
					unlink('././././assets/uploads/images/' . $image_old);
					unlink('././././assets/uploads/images/thumb-144/' . $image_old);
					unlink('././././assets/uploads/images/thumb-400/' . $image_old);
				}

				$response['status'] 	= 'success';
				$response['message'] = 'Success update data!';
			} else {
				$response['message'] = 'Failed to update data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//EVENT CLASS  - PERMISSION 
	public function get_kelas()
	{

		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$limit 		= $this->input->get('limit');
		$page 		= $this->input->get('page');
		$label 		= $this->input->get('label');
		$id 		= $this->input->get('id');
		$event 		= $this->input->get('event');
		$sort 		= $this->input->get('sort');
		$status 	= $this->input->get('status');

		$query 	= "SELECT * FROM kelas WHERE id_event = '" . $event . "' ";
		// dd($query);
		if ($id) {
			$query .= " and id = '" . $id . "'";
		}
		if ($label) {
			$query .= " and label like '%" . $label . "%'";
		}

		if ($status != '' && $status != 'ALL') {
			$query .= " and status = '" . $status . "'";
		}

		if ($sort == 'asc') {
			$query .= " order by created_at ASC ";
		} else {
			$query .= " order by created_at DESC ";
		}

		$total_all 	= $this->db->query($query)->num_rows();

		if ($limit) {
			$page = ($page) ? intval($page) : 1;
			$limit = ($limit) ? intval($limit) : 10;
			$offset = ($page) ? (($page - 1) * $limit) : 0;
			$query .= " LIMIT " . $limit . " OFFSET " . $offset;
		}

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();

		foreach ($query_result as $idx => $row) {

			unset($query_result[$idx]->created_at);
			unset($query_result[$idx]->updated_at);
			unset($query_result[$idx]->logo);
			unset($query_result[$idx]->result_doc);

			$q1 = "SELECT full_name  FROM users WHERE  id_users = '" . $row->scrutineering_admin . "'";
			$r1 = $this->db->query($q1)->result();
			$query_result[$idx]->scrutineering_admin 	= ($r1) ? $r1[0]->full_name : '';

			$q2 = "SELECT full_name  FROM users WHERE  id_users = '" . $row->scrutineering_teknis . "'";
			$r2 = $this->db->query($q2)->result();
			$query_result[$idx]->scrutineering_teknis 	= ($r2) ? $r2[0]->full_name : '';
		}

		if (!!$query_result) {
			if ($limit) {
				$response = list_response_map($query_result, $total_all, $page, $total, $limit);
			} else {
				$response = list_response($query_result, $total_all);
			}
		} else {
			$response['message'] 	= "No data result!";
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	//EVENT CLASS NEW -  
	public function set_kelas()
	{
		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('label', 'Label', 'required');

		if (!!$this->form_validation->run()) {
			$event 				    = $this->input->post('event');
			$label 				    = $this->input->post('label');
			$sub_label 			    = $this->input->post('sub_label');
			$biaya 		            = $this->input->post('biaya');
			$status 			    = $this->input->post('status');

			$data = array(
				'id_event' 				=> $event,
				'label' 				=> $label,
				'sub_label' 			=> $sub_label,
				'biaya' 			    => $biaya,
				'status' 				=> $status
			);

			$query = $this->m_crud->insert('kelas', $data);

			if (!!$query) {
				$response['status'] 	= 'success';
				$response['message'] = 'Success insert data!';
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	//EVENT CLASS EDIT-  
	public function update_kelas()
	{
		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('label', 'Label', 'required');

		if (!!$this->form_validation->run()) {
			$id 				    = $this->input->post('id');
			$label 				    = $this->input->post('label');
			$sub_label 			    = $this->input->post('sub_label');
			$biaya 		            = $this->input->post('biaya');
			$status 	            = $this->input->post('status');

			$data = array(
				'label' 		=> $label,
				'sub_label' 	=> $sub_label,
				'biaya' 		=> $biaya,
				'status'		=> $status
			);

			$query = $this->m_crud->update('kelas', 'id', $data, $id);

			if (!!$query) {

				$response['status'] 	= 'success';
				$response['message'] = 'Success update data!';
			} else {
				$response['message'] = 'Failed to update data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//EVENT KATEGORI
	public function get_kategori()
	{

		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$limit 		= $this->input->get('limit');
		$page 		= $this->input->get('page');
		$label 		= $this->input->get('label');
		$id 		= $this->input->get('id');
		$kelas 		= $this->input->get('kelas');
		$sort 		= $this->input->get('sort');
		$status 	= $this->input->get('status');

		if ($id) { }

		$query 	= "SELECT * FROM kategori WHERE id_kelas = '" . $kelas . "' ";
		// dd($query);
		if ($id) {
			$query .= " and id = '" . $id . "'";
		}
		if ($label) {
			$query .= " and label like '%" . $label . "%'";
		}

		if ($status != '' && $status != 'ALL') {
			$query .= " and status = '" . $status . "'";
		}

		if ($sort == 'asc') {
			$query .= " order by created_at ASC ";
		} else {
			$query .= " order by created_at DESC ";
		}

		$total_all 	= $this->db->query($query)->num_rows();

		if ($limit) {
			$page = ($page) ? intval($page) : 1;
			$limit = ($limit) ? intval($limit) : 10;
			$offset = ($page) ? (($page - 1) * $limit) : 0;
			$query .= " LIMIT " . $limit . " OFFSET " . $offset;
		}

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();

		foreach ($query_result as $idx => $row) {

			unset($query_result[$idx]->created_at);
			unset($query_result[$idx]->updated_at);
		}

		if (!!$query_result) {
			if ($limit) {
				$response = list_response_map($query_result, $total_all, $page, $total, $limit);
			} else {
				$response = list_response($query_result, $total_all);
			}
		} else {
			$response['message'] 	= "No data result!";
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	public function set_kategori()
	{
		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('label', 'Label', 'required');

		if (!!$this->form_validation->run()) {
			$kelas 				    = $this->input->post('kelas');
			$label 				    = $this->input->post('label');
			$status 			    = $this->input->post('status');

			$data = array(
				'id_kelas' 				=> $kelas,
				'label' 				=> $label
			);

			$query = $this->m_crud->insert('kategori', $data);

			if (!!$query) {
				$response['status'] 	= 'success';
				$response['message'] = 'Success insert data!';
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	public function update_kategori()
	{
		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('label', 'Label', 'required');

		if (!!$this->form_validation->run()) {
			$id 				    = $this->input->post('id');
			$label 				    = $this->input->post('label');

			$data = array(
				'label' 		=> $label,
			);

			$query = $this->m_crud->update('kategori', 'id', $data, $id);

			if (!!$query) {

				$response['status'] 	= 'success';
				$response['message'] = 'Success update data!';
			} else {
				$response['message'] = 'Failed to update data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}


	//REGISTRATION
	public function get_registration()
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';
		$this->page = 'registration';

		$this->ACCESS_PAGE_API		= (isset($this->USER_PERMISSION_API[$this->page])) ? $this->USER_PERMISSION_API[$this->page] : false;

		$access_list = (in_array('list', $this->ACCESS_PAGE_API)) ? true : false;

		if ($access_list) {
			$limit 		= $this->input->get('limit');
			$page 		= $this->input->get('page');
			$id 		= $this->input->get('id');

			$nama 		= $this->input->get('nama');
			$tim 		= $this->input->get('tim');
			$kelas 		= $this->input->get('kelas');
			$no_reg 	= $this->input->get('no_reg');
			$sort 		= $this->input->get('sort');
			$query 	= "SELECT 
							r.id, 
							r.no_registration, 
							r.status,
							r.no_start, 
							r.kendaraan_merek, 
							r.kendaraan_tipe,
							r.status_pembayaran,

							usr.full_name as register,
							usr.email as email,

							ev.label as event,
							ev.logo as logo,

							cl.label as class,

							ct.label as category,

							rc.full_name as racer_nama,
							rc.kis_imi as racer_kis,
							rc.pengprov_imi as racer_pengprov,

							tm.name as team,

							tm.mng_name as manager,

							tm.entrant_name as nama_entrant

						FROM registrations as r
						LEFT JOIN users as usr ON usr.id_users = r.id_users
						LEFT JOIN pembalap as rc ON rc.id = r.id_pembalap
						LEFT JOIN tim as tm ON tm.id = r.id_tim
						LEFT JOIN kategori as ct ON ct.id = r.id_kategori
						LEFT JOIN kelas as cl ON cl.id = ct.id_kelas
						LEFT JOIN event as ev ON ev.id = cl.id_event
						LEFT JOIN event_organizer as eo ON eo.id = ev.id_eo

						WHERE 
							ev.id = '" . $id . "' 
						";
			// dd($query);
			if ($this->EO) {
				$query .= " and eo.id = '" . $this->EO->id . "'";
			}
			if ($this->IS_MANAGER_USER) {
				$query .= " and r.id_users = '" . $this->ID_USER . "'";
			}
			if ($no_reg) {
				$query .= " and r.no_registration = '" . $no_reg . "'";
			}
			if ($nama) {
				$query .= " and rc.full_name like '%" . $nama . "%'";
			}
			if ($tim) {
				$query .= " and tm.name like '%" . $tim . "%'";
			}
			if ($kelas) {
				$query .= " and cl.id = '" . $kelas . "'";
			}

			$total_all 	= $this->db->query($query)->num_rows();

			if ($sort == 'asc') {
				$query .= " order by r.created_at ASC ";
			} else {
				$query .= " order by r.created_at DESC ";
			}

			if ($limit) {
				$page = ($page) ? intval($page) : 1;
				$limit = ($limit) ? intval($limit) : 10;
				$offset = ($page) ? (($page - 1) * $limit) : 0;
				$query .= " LIMIT " . $limit . " OFFSET " . $offset;
			}
			$total 				= $this->db->query($query)->num_rows();
			$query_result      	= $this->db->query($query)->result();

			foreach ($query_result as $idx => $row) {

				if ($row->logo == '')
					$query_result[$idx]->logo 	= base_url('assets/uploads/no_image.png');
				else
					$query_result[$idx]->logo 	= base_url('assets/uploads/images/thumb-144/' . $row->logo);
			}

			if (!!$query_result) {
				if ($limit) {
					$response = list_response_map($query_result, $total_all, $page, $total, $limit);
				} else {
					$response = list_response($query_result, $total_all);
				}
			} else {
				$response['message'] 	= "No data result!";
			}
		} else {
			$response['message'] 	= "No access to request!";
		}
		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	//NEW REGISTRATION
	public function get_registration_detail($id)
	{
		$status 	= 'error';
		$message 	= '';

		$query_reg     	= $this->m_global->get_by_id('registrations', 'id', $id);
		if ($query_reg) {
			$result				= $query_reg[0];
			$query_user		    = "SELECT id_users, full_name, email, phone FROM users WHERE id_users = '" . $result->id_users . "'";
			$result_user     	= $this->db->query($query_user)->result();
			$result->user		= ($result_user[0]) ? $result_user[0] : '';

			$query_rcr		    = "SELECT id, full_name, email, phone, kis_imi, pengprov_imi FROM pembalap WHERE id = '" . $result->id_pembalap . "'";
			$result_rcr     	= $this->db->query($query_rcr)->result();
			$result->pembalap		= ($result_rcr[0]) ? $result_rcr[0] : '';

			$query_team		    = "SELECT id, name, mng_name, mng_license, mng_phone, entrant, entrant_name, entrant_license, entrant_license_expire  FROM tim WHERE id = '" . $result->id_tim . "'";
			$result_team     	= $this->db->query($query_team)->result();
			$result->tim		= ($result_team[0]) ? $result_team[0] : '';

			$query_event	= " SELECT
									kategori.id AS id,
									kategori.label AS kategori,
									kelas.label AS kelas,
									event.id AS event_id,
									event.label AS event,
									event.sub_label AS event_sub,
									event.logo AS event_logo,
									event.lokasi AS lokasi,
									event.date_start AS event_start,
									event.date_end AS event_end
								FROM
									kategori
								JOIN kelas ON kelas.id = kategori.id_kelas
								JOIN event ON event.id = kelas.id_event
								WHERE
									kategori.id = '" . $result->id_kategori . "'";
			$result_event     	= $this->db->query($query_event)->result();
			$result->event	= $result_event[0];

			unset($result->id_users);
			unset($result->id_pembalap);
			unset($result->id_tim);
			unset($result->id_manager);
			unset($result->id_entrant);
			unset($result->id_kategori);

			if ($result->event->event_logo == '')
				$result->event->event_logo 	= base_url('assets/uploads/no_image.png');
			else {
				$result->event->event_logo_thumb 	= base_url('assets/uploads/images/thumb-144/' . $result->event->event_logo);
				$result->event->event_logo 			= base_url('assets/uploads/images/' . $result->event->event_logo);
			}

			$status 	= "success";
		} else {
			$result 		= '';
			$message 		= "No data result!";
		}

		$response = response_map($result, $status, $message);

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	public function set_registration()
	{
		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->page = 'new_reg';

		$this->ACCESS_PAGE_API		= (isset($this->USER_PERMISSION_API[$this->page])) ? $this->USER_PERMISSION_API[$this->page] : false;

		$access = (in_array('new', $this->ACCESS_PAGE_API)) ? true : false;

		if ($access) {

			$this->form_validation->set_rules('pembalap', 'Pembalap', 'required');
			$this->form_validation->set_rules('kategori', 'Event, Kelas & Kategori', 'required');
			$this->form_validation->set_rules('no_start', 'Nomor Start', 'required');
			$this->form_validation->set_rules('tim', 'Tim', 'required');
			$this->form_validation->set_rules('merek', 'Merek Mobil', 'required');
			$this->form_validation->set_rules('tipe', 'Tipe Mobil', 'required');

			if (!!$this->form_validation->run()) {

				$id_users 			= $this->ID_USER;
				$id_kategori 		= $this->input->post('kategori');
				$id_pembalap 		= $this->input->post('pembalap');
				$id_tim 			= $this->input->post('tim');
				$no_start 			= $this->input->post('no_start');
				$kendaraan_merek 	= $this->input->post('merek');
				$kendaraan_tipe 	= $this->input->post('tipe');
				$kendaraan_tahun 	= $this->input->post('tahun');
				$kendaraan_no_mesin 	= $this->input->post('mesin');
				$kendaraan_no_rangka 	= $this->input->post('rangka');

				$no_registration 	= date("dmy") . $id_kategori . substr(str_shuffle('0123456789'), 1, 4);

				$data = array(
					'no_registration'		=> $no_registration,
					'id_users'				=> $id_users,
					'id_pembalap'			=> $id_pembalap,
					'id_tim'				=> $id_tim,
					'id_kategori'			=> $id_kategori,
					'status'				=> 2,
					'no_start'				=> $no_start,
					'kendaraan_merek'		=> $kendaraan_merek,
					'kendaraan_tipe'		=> $kendaraan_tipe,
					'kendaraan_no_mesin'	=> $kendaraan_no_mesin,
					'kendaraan_no_rangka'	=> $kendaraan_no_rangka,
					'kendaraan_tahun'		=> $kendaraan_tahun
				);


				$query = $this->m_crud->insert_id('registrations', $data);

				if (!!$query) {
					$response['status'] 	= 'success';
					$response['message'] 	= 'Success insert data!';
					$response['data'] 		= $query;
				} else {
					$response['message'] = 'Failed to insert data!';
				}
			} else {
				$response['message'] = validation_errors();
			}
		} else {
			$response['message'] 	= "No access to request!";
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//PEMBALAP
	public function get_pembalap()
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$limit 		= $this->input->get('limit');
		$page 		= $this->input->get('page');
		$id 		= $this->input->get('id');
		$nama 		= $this->input->get('nama');
		$pengprov 		= $this->input->get('pengprov');
		$sort 		= $this->input->get('sort');

		$query 	= "SELECT * FROM pembalap WHERE 1 = 1 ";

		if ($this->IS_MANAGER_USER) {
			$query 	= "SELECT * FROM pembalap WHERE user_created = '" . $this->ID_USER . "' ";
		}

		if ($id) {
			$query .= " and id = '" . $id . "'";
		}
		if ($nama) {
			$query .= " and full_name like '%" . $nama . "%'";
		}
		if ($nama) {
			$query .= " and pengprov_imi like '%" . $pengprov . "%'";
		}
		if ($sort == 'asc') {
			$query .= " order by created_at ASC ";
		} else {
			$query .= " order by created_at DESC ";
		}
		$total_all 	= $this->db->query($query)->num_rows();

		if ($limit) {
			$page = ($page) ? intval($page) : 1;
			$limit = ($limit) ? intval($limit) : 10;
			$offset = ($page) ? (($page - 1) * $limit) : 0;
			$query .= " LIMIT " . $limit . " OFFSET " . $offset;
		}

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();

		if (!!$query_result) {
			if ($limit) {
				$response = list_response_map($query_result, $total_all, $page, $total, $limit);
			} else {
				$response = list_response($query_result, $total_all);
			}
		} else {
			$response['message'] 	= "No data result!";
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	public function set_pembalap()
	{

		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('kis_imi', 'KIS IMI', 'required');

		if (!!$this->form_validation->run()) {

			$email 				= $this->input->post('email');
			$name 				= $this->input->post('name');
			$pengprov 			= $this->input->post('pengprov');
			$phone 				= $this->input->post('phone');
			$kis_imi 			= $this->input->post('kis_imi');

			$data = array(
				'user_created'		=> $this->ID_USER,
				'email'				=> $email,
				'full_name'			=> $name,
				'kis_imi'			=> $kis_imi,
				'pengprov_imi'		=> $pengprov,
				'phone'				=> $phone,
			);


			$query = $this->m_crud->insert_id('pembalap', $data);

			if (!!$query) {
				$response['status'] 		= 'success';
				$response['message'] 		= 'Success insert data!';
				$response['data']['id'] 	= simple_encrypt($query);
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//TIM
	public function get_tim()
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$limit 		= $this->input->get('limit');
		$page 		= $this->input->get('page');
		$id 		= $this->input->get('id');
		$nama 		= $this->input->get('nama');
		$sort 		= $this->input->get('sort');

		$query 	= "SELECT * FROM tim WHERE 1 = 1 ";

		if ($this->IS_MANAGER_USER) {
			$query 	= "SELECT * FROM tim WHERE user_created = '" . $this->ID_USER . "' ";
		}
		// dd($query);
		if ($id) {
			$query .= " and id = '" . $id . "'";
		}
		if ($nama) {
			$query .= " and name like '%" . $nama . "%'";
		}
		if ($sort == 'asc') {
			$query .= " order by created_at ASC ";
		} else {
			$query .= " order by created_at DESC ";
		}

		$total_all 	= $this->db->query($query)->num_rows();

		if ($limit) {
			$page = ($page) ? intval($page) : 1;
			$limit = ($limit) ? intval($limit) : 10;
			$offset = ($page) ? (($page - 1) * $limit) : 0;
			$query .= " LIMIT " . $limit . " OFFSET " . $offset;
		}

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();

		if (!!$query_result) {
			if ($limit) {
				$response = list_response_map($query_result, $total_all, $page, $total, $limit);
			} else {
				$response = list_response($query_result, $total_all);
			}
		} else {
			$response['message'] 	= "No data result!";
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	public function set_tim()
	{

		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('name', 'Name', 'required');

		if (!!$this->form_validation->run()) {

			$name 				= $this->input->post('name');

			$data = array(
				'user_created'		=> $this->ID_USER,
				'name'			=> $name
			);

			$query = $this->m_crud->insert_id('tim', $data);

			if (!!$query) {
				$response['status'] 		= 'success';
				$response['message'] 		= 'Success insert data!';
				$response['data']['id'] 	= simple_encrypt($query);
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//EO  - PERMISSION 
	public function get_eo()
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$this->page = 'event';
		$this->ACCESS_PAGE_API		= (isset($this->USER_PERMISSION_API[$this->page])) ? $this->USER_PERMISSION_API[$this->page] : false;
		$access_list = (in_array('list', $this->ACCESS_PAGE_API)) ? true : false;
		if ($access_list) {

			$limit 		= $this->input->get('limit');
			$page 		= $this->input->get('page');
			$id 		= $this->input->get('id');
			$nama 		= $this->input->get('nama');
			$sort 		= $this->input->get('sort');

			if ($this->EO) {
				$limit 	= false;
				$page	= false;
				$id 	=  $this->EO->id;
			}

			$query 	= "SELECT * FROM event_organizer WHERE 1 = 1 ";
			// dd($query);
			if ($id) {
				$query .= " and id = '" . $id . "'";
			}
			if ($nama) {
				$query .= " and name like '%" . $nama . "%'";
			}

			if ($sort == 'asc') {
				$query .= " order by created_at ASC ";
			} else {
				$query .= " order by created_at DESC ";
			}

			$total_all 	= $this->db->query($query)->num_rows();

			if ($limit) {
				$page = ($page) ? intval($page) : 1;
				$limit = ($limit) ? intval($limit) : 10;
				$offset = ($page) ? (($page - 1) * $limit) : 0;
				$query .= " LIMIT " . $limit . " OFFSET " . $offset;
			}

			$total 				= $this->db->query($query)->num_rows();
			$query_result      	= $this->db->query($query)->result();

			if (!!$query_result) {
				if ($limit) {
					$response = list_response_map($query_result, $total_all, $page, $total, $limit);
				} else {
					$response = list_response($query_result, $total_all);
				}
			} else {
				$response['message'] 	= "No data result!";
			}
		} else {
			$response['message'] 	= "No access to request!";
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	public function set_eo()
	{

		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('name', 'Name', 'required');

		if (!!$this->form_validation->run()) {

			$name 				= $this->input->post('name');
			$status				= $this->input->post('status');

			$data = array(
				'name'			=> $name,
				'status'		=> $status,
			);

			$query = $this->m_crud->insert_id('event_organizer', $data);

			if (!!$query) {
				$response['status'] 		= 'success';
				$response['message'] 		= 'Success insert data!';
				$response['data']['id'] 	= simple_encrypt($query);
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
	public function set_eo_status()
	{

		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');

		if (!!$this->form_validation->run()) {

			$id 				= $this->input->post('id');
			$status 			= $this->input->post('status');

			$data = array(
				'status'			=> $status,
			);

			$query = $this->m_crud->update('event_organizer', 'id', $data, $id);

			if (!!$query) {
				$response['status'] 		= 'success';
				$response['message'] 		= 'Success insert data!';
				$response['data']['id'] 	= $id;
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}


	//USERS

	public function get_user()
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$limit 		= $this->input->get('limit');
		$page 		= $this->input->get('page');
		$id 		= $this->input->get('id');
		$nama 		= $this->input->get('nama');
		$event 		= $this->input->get('event');
		$eo 		= $this->input->get('eo');
		$email 		= $this->input->get('email');
		$sort 		= $this->input->get('sort');

		if ($event) {
			$q1 = "SELECT id_eo  FROM event WHERE  id = '" . $event . "'";
			$r1 = $this->db->query($q1)->result();
			$eo	= ($r1) ? $r1[0]->id_eo : false;
		}

		$query 	= "SELECT 
						users.*, 
						event_organizer.name as eo_name 
					FROM 
						users 
					LEFT JOIN 
						event_organizer ON event_organizer.id = users.id_eo 
					WHERE 1 = 1 ";
		// dd($query);
		if ($id) {
			$query .= " and id = '" . $id . "'";
		}
		if ($eo) {
			$query .= " and id_eo = '" . $eo . "'";
		}
		if ($nama) {
			$query .= " and full_name like '%" . $nama . "%'";
		}
		if ($email) {
			$query .= " and email = '" . $nama . "'";
		}

		if ($sort == 'asc') {
			$query .= " order by created_at ASC ";
		} else {
			$query .= " order by created_at DESC ";
		}

		$total_all 	= $this->db->query($query)->num_rows();

		if ($limit) {
			$page = ($page) ? intval($page) : 1;
			$limit = ($limit) ? intval($limit) : 10;
			$offset = ($page) ? (($page - 1) * $limit) : 0;
			$query .= " LIMIT " . $limit . " OFFSET " . $offset;
		}

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();

		foreach ($query_result as $idx => $row) {
			$query_result[$idx]->id_users = simple_encrypt($row->id_users);
			unset($query_result[$idx]->confirmation_code);
			unset($query_result[$idx]->password);

			$q1 = "SELECT label  FROM event WHERE  id = '" . $row->event_active . "'";
			$r1 = $this->db->query($q1)->result();
			$query_result[$idx]->event_name 	= ($r1) ? $r1[0]->label : '';
		}

		if (!!$query_result) {
			if ($limit) {
				$response = list_response_map($query_result, $total_all, $page, $total, $limit);
			} else {
				$response = list_response($query_result, $total_all);
			}
		} else {
			$response['message'] 	= "No data result!";
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	public function get_user_detail($id)
	{
		$status 	= 'error';
		$message 	= '';

		$query_reg     	= $this->m_global->get_by_id('users', 'id_users', simple_decrypt($id));

		if ($query_reg) {
			$result				= $query_reg[0];
			$result->id_users 	= simple_encrypt($result->id_users);
			unset($result->confirmation_code);
			unset($result->password);
			$status 	= "success";
		} else {
			$result 		= '';
			$message 		= "No data result!";
		}

		$response = response_map($result, $status, $message);

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	public function set_user()
	{

		$response['status'] 	= 'error';
		$response['message'] = '';

		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('user_group', 'User Group', 'required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'trim|required|matches[password]');

		if (!!$this->form_validation->run()) {

			$email 				= $this->input->post('email');
			$name 				= $this->input->post('name');
			$password 			= $this->input->post('password');
			$phone 				= $this->input->post('phone');
			$user_group 		= $this->input->post('user_group');

			$confreq 	= sha1(uniqid() . $email);


			$data = array(
				'email'				=> $email,
				'full_name'			=> $name,
				'user_group'		=> $user_group,
				'password'			=> hashPassword($password),
				'confirmation_code'	=> $confreq,
				'status'			=> 2,
				'phone'				=> $phone,
				'last_login' 		=> date('Y-m-d H:i:s'),
				'last_change_password' 		=> date('Y-m-d H:i:s')
			);

			$query = $this->m_crud->insert_id('users', $data);

			if (!!$query) {
				$response['status'] 		= 'success';
				$response['message'] 		= 'Success insert data!';
				$response['data']['id'] 	= simple_encrypt($query);
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	public function set_user_status()
	{

		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$this->form_validation->set_rules('id', 'ID', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');

		if (!!$this->form_validation->run()) {

			$id 				= simple_decrypt($this->input->post('id'));
			$status 			= $this->input->post('status');

			$data = array(
				'status'			=> $status,
			);

			$query = $this->m_crud->update('users', 'id_users', $data, $id);

			if (!!$query) {
				$response['status'] 		= 'success';
				$response['message'] 		= 'Success insert data!';
				$response['data']['id'] 	= simple_encrypt($id);
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	public function set_user_eo()
	{

		$response['status'] 	= 'error';
		$response['message'] 	= '';

		$this->form_validation->set_rules('id', 'Name', 'required');
		$this->form_validation->set_rules('eo', 'Event Orginizer Id', 'required');

		if (!!$this->form_validation->run()) {

			$id 				= simple_decrypt($this->input->post('id'));
			$eo 				= $this->input->post('eo');

			$data = array(
				'id_eo'			=> $eo,
			);

			$query = $this->m_crud->update('users', 'id_users', $data, $id);

			if (!!$query) {
				$response['status'] 		= 'success';
				$response['message'] 		= 'Success insert data!';
				$response['data']['id'] 	= simple_encrypt($id);
			} else {
				$response['message'] = 'Failed to insert data!';
			}
		} else {
			$response['message'] = validation_errors();
		}

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//Payment
	public function get_payment_list()
	{
		$response['status'] 	= 'error';
		$response['message'] 	= '';
		$this->page = 'payment';

		$this->ACCESS_PAGE_API		= (isset($this->USER_PERMISSION_API[$this->page])) ? $this->USER_PERMISSION_API[$this->page] : false;

		$access_list = (in_array('list', $this->ACCESS_PAGE_API)) ? true : false;

		if ($access_list) {
			$limit 		= $this->input->get('limit');
			$page 		= $this->input->get('page');
			$event 		= $this->input->get('event');

			$no_reg 	= $this->input->get('no_reg');
			$status 	= $this->input->get('status');
			$sort 		= $this->input->get('sort');

			$query 	= "SELECT 
							r.id, 
							r.no_registration, 
							r.status,
							r.no_start, 
							r.status_pembayaran,
							r.biaya,
							r.is_corfirm_payment as konfirmasi,

							usr.full_name as register,
							usr.email as email,

							ev.label as event,
							cl.label as class,
							ct.label as category,

							rc.full_name as racer_nama,
							rc.kis_imi as racer_kis,
							rc.pengprov_imi as racer_pengprov,

							tm.name as team,
							tm.mng_name as manager,
							tm.entrant_name as nama_entrant

						FROM registrations as r
						LEFT JOIN users as usr ON usr.id_users = r.id_users
						LEFT JOIN pembalap as rc ON rc.id = r.id_pembalap
						LEFT JOIN tim as tm ON tm.id = r.id_tim
						LEFT JOIN kategori as ct ON ct.id = r.id_kategori
						LEFT JOIN kelas as cl ON cl.id = ct.id_kelas
						LEFT JOIN event as ev ON ev.id = cl.id_event
						LEFT JOIN event_organizer as eo ON eo.id = ev.id_eo

						WHERE 
							1 = 1 
						";
			// dd($query);
			if ($this->EO) {
				$query .= " and eo.id = '" . $this->EO->id . "'";
			}
			if ($this->IS_MANAGER_USER) {
				$query .= " and r.id_users = '" . $this->ID_USER . "'";
			}

			if ($event) {
				$event .= " and ev.id = '" . $event . "'";
			}
			if ($no_reg) {
				$query .= " and r.no_registration = '" . $no_reg . "'";
			}

			if ($status && $status != 'ALL') {
				$query .= " and r.status_pembayaran = '" . $status . "'";
			}

			$total_all 	= $this->db->query($query)->num_rows();

			if ($sort == 'asc') {
				$query .= " order by r.created_at ASC ";
			} else {
				$query .= " order by r.created_at DESC ";
			}

			if ($limit) {
				$page = ($page) ? intval($page) : 1;
				$limit = ($limit) ? intval($limit) : 10;
				$offset = ($page) ? (($page - 1) * $limit) : 0;
				$query .= " LIMIT " . $limit . " OFFSET " . $offset;
			}
			$total 				= $this->db->query($query)->num_rows();
			$query_result      	= $this->db->query($query)->result();

			if (!!$query_result) {
				if ($limit) {
					$response = list_response_map($query_result, $total_all, $page, $total, $limit);
				} else {
					$response = list_response($query_result, $total_all);
				}
			} else {
				$response['message'] 	= "No data result!";
			}
		} else {
			$response['message'] 	= "No access to request!";
		}
		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}

	//Dashboard 
	public function dashboard()
	{
		$status 	= 'error';
		$message 	= '';
		$result		= [];

		$this->page = 'dashboard';
		$this->ACCESS_PAGE_API		= (isset($this->USER_PERMISSION_API[$this->page])) ? $this->USER_PERMISSION_API[$this->page] : false;

		if ($this->ACCESS_PAGE_API) {
			$status =  'success';

			$qe = "SELECT * FROM event  order by date_start ASC LIMIT 6";
			$event_list      	= $this->db->query($qe)->result();

			foreach ($event_list as $idx => $row) {
				unset($event_list[$idx]->created_at);
				unset($event_list[$idx]->updated_at);

				$event_list[$idx]->date_start 	= date('Y-m-d', strtotime($row->date_start));
				$event_list[$idx]->date_end 	= date('Y-m-d', strtotime($row->date_end));
				if ($row->logo == '') {
					$event_list[$idx]->logo_thumb 	= base_url('assets/uploads/no_image.png');
					$event_list[$idx]->logo 	= base_url('assets/uploads/no_image.png');
				} else {
					$event_list[$idx]->logo_thumb 	= base_url('assets/uploads/images/thumb-144/' . $row->logo);
					$event_list[$idx]->logo 	= base_url('assets/uploads/images/' . $row->logo);
				}

				$query_class = "SELECT * FROM kelas WHERE  id_event = '" . $row->id . "' ";

				$t 		    = $this->db->query($query_class)->num_rows();
				$res      	= $this->db->query($query_class)->result();

				foreach ($res as $i => $v) {
					if ($v->logo == '')
						$res[$i]->logo 	= base_url('assets/uploads/no_image.png');
					else
						$res[$i]->logo 	= base_url('assets/uploads/images/thumb-144/' . $v->logo);

					unset($res[$i]->created_at);
					unset($res[$i]->updated_at);
					unset($res[$i]->id_events);

					$query_class_c      = "SELECT * FROM kategori WHERE  id_kelas = '" . $v->id . "' ";
					$t_c 		        = $this->db->query($query_class_c)->num_rows();
					$res_c      	    = $this->db->query($query_class_c)->result();
					foreach ($res_c as $i_c => $v_c) {
						unset($res_c[$i_c]->created_at);
						unset($res_c[$i_c]->updated_at);
						unset($res_c[$i_c]->id_kelas);
					}
					$res[$i]->kategori  = $res_c;
				}
				$event_list[$idx]->kelas = $res;
			}
			$result['event_list'] 	= $event_list;

			if ($this->IS_MANAGER_USER) {
				$q = "SELECT id FROM registrations WHERE id_users = '" . $this->ID_USER . "' ";
				$reg_total 	= $this->db->query($q)->num_rows();

				$q1 = $q . " AND status_pembayaran = 1";
				$lunas 	= $this->db->query($q1)->num_rows();

				$q2 = $q . " AND status_pembayaran = 2";
				$menunggu_pembayaran 	= $this->db->query($q2)->num_rows();

				$q3 = $q . " AND status_pembayaran = 3";
				$sedang_diproses 	= $this->db->query($q3)->num_rows();

				$pendaftaran = [
					'total' 				=> $reg_total,
					'menunggu_pembayaran' 	=> $menunggu_pembayaran,
					'sedang_diproses' 		=> $sedang_diproses,
					'lunas' 				=> $lunas,
				];

				$result['pendaftaran'] 	= $pendaftaran;
			}
			if ($this->IS_EO_USER) {

					//PENDAFTARAN
					$pendaftaran = [];

					$q = "SELECT 
							ev.label,
							COUNT(r.id) AS 'Total',
							COUNT(CASE WHEN (r.status_pembayaran = '1') THEN 1 ELSE NULL END) AS 'Lunas',
							COUNT(CASE WHEN (r.status_pembayaran = '2') THEN 1 ELSE NULL END) AS 'Menunggu Pembayaran',
							COUNT(CASE WHEN (r.status_pembayaran = '3') THEN 1 ELSE NULL END) AS 'Sedang Diproses',
							COUNT(CASE WHEN (r.status_pembayaran = '0') THEN 1 ELSE NULL END) AS 'Dibatalkan',
							COUNT(CASE WHEN (r.status_pembayaran = '4') THEN 1 ELSE NULL END) AS 'Tidak Valid'
						from
							event as ev 
						left join event_organizer as eo on
							eo.id = ev.id_eo
						left join kelas as cl on
							cl.id_event = ev.id	
						left join kategori as ct on
							ct.id_kelas = cl.id
						left join registrations r on
							r.id_kategori = ct.id 
						WHERE 
							eo.id = '" . $this->EO->id . "' 
						group by 
							ev.id";
			
					$reg_total 	= $this->db->query($q)->result();
					$reg_total = json_decode(json_encode($reg_total), TRUE);
			
					$awakwo = array_slice(array_keys($reg_total[0]), 2);

					foreach($reg_total as $k => $v){
						$pendaftaran['labels'][]   = $v['label'];
					}
			
					foreach($awakwo as $key => $val){
						$pendaftaran['rows'][$key]['label'] = $val;
						foreach($reg_total as $k => $v){
							$pendaftaran['rows'][$key]['data'][] =  $v[$val];
						}
					}
					
					$result['pendaftaran_cart'] 	= $pendaftaran;

				}

			
		} else {
			$message 	= "No access to request!";
		}

		$response = response_map($result, $status, $message);

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}
}
