<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Migrate extends CI_Controller
{
    private $page 					= '';
	private $ID_USER 				= '';
	private $USER_INFO_API 			= [];
	private $USER_PERMISSION_API 	= '';
	private $ACCESS_PAGE_API 		= '';
	private $IS_USER 				= '';
	private $IS_EO_USER 			= false;
	private $IS_SUPER_USER 			= false;
	private $IS_RCT_USER 			= false;
	private $IS_MANAGER_USER 		= false;
	private $EO 					= false;

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata("sign_in") == TRUE) {

			$this->config->load('rct_config');
			$this->USER_INFO_API 		= $this->session->userdata();

			$this->USER_API  			= getUserGroup([$this->USER_INFO_API['user_group']]);
			$this->IS_USER 				= key($this->USER_API);
			$this->IS_SUPER_USER 		= (in_array($this->IS_USER, $this->config->item('super_user'))) ? true : false;
			$this->IS_RCT_USER 			= (in_array($this->IS_USER, $this->config->item('rct_user'))) ? true : false;
			$this->IS_EO_USER 			= (in_array($this->IS_USER, $this->config->item('eo_user'))) ? true : false;
			$this->IS_MANAGER_USER 		= (in_array($this->IS_USER, $this->config->item('regular_user'))) ? true : false;
			$this->USER_PERMISSION_API  = $this->config->item('user_group')[$this->IS_USER]['access'];

			$this->load->helper('status');
			$this->load->model('m_crud');
			$this->load->model('m_global');
			$this->load->model('m_rct');

			$this->ID_USER = simple_decrypt($this->USER_INFO_API['id']);

			if ($this->IS_EO_USER) {
				$q 	= "SELECT eo.* FROM event_organizer eo JOIN users ON users.id_eo = eo.id WHERE users.id_users = '" . $this->ID_USER . "' AND eo.status = 1";
				$r	= $this->db->query($q)->result();
				$this->EO	= ($r) ? $r[0] : false;
			}

		} else {
			get_redirecting('auth');
		}
    }
    public function index(){
        
        // $USER_GROUP = $this->config->item('all');

        // $a = getUserGroup($this->config->item('all_user'));
        // $arr = $this->config->item('menu');
        // foreach($arr as $key => $val){
        //     echo $key."','";
        // }
        // die();
        // $aa = $this->config->item('user_group')[2001]['access'] ;

        // $bb = array_keys($aa);

        // $a = generateMenu($bb);
        // in_array('pembalap',$aa);
        // dd(substr(str_shuffle('0123456789'),1,4));
        $pendaftaran = [];

        $q = "SELECT 
                ev.label,
                COUNT(r.id) AS 'Total',
                COUNT(CASE WHEN (r.status_pembayaran = '1') THEN 1 ELSE NULL END) AS 'Lunas',
                COUNT(CASE WHEN (r.status_pembayaran = '2') THEN 1 ELSE NULL END) AS 'Menunggu Pembayaran',
                COUNT(CASE WHEN (r.status_pembayaran = '3') THEN 1 ELSE NULL END) AS 'Sedang Diproses',
                COUNT(CASE WHEN (r.status_pembayaran = '0') THEN 1 ELSE NULL END) AS 'Dibatalkan',
                COUNT(CASE WHEN (r.status_pembayaran = '4') THEN 1 ELSE NULL END) AS 'Tidak Valid'
            from
                event as ev 
            left join event_organizer as eo on
                eo.id = ev.id_eo
            left join kelas as cl on
                cl.id_event = ev.id	
            left join kategori as ct on
                ct.id_kelas = cl.id
            left join registrations r on
                r.id_kategori = ct.id 
            WHERE 
                eo.id = '" . $this->EO->id . "' 
            group by 
                ev.id";

        $reg_total 	= $this->db->query($q)->result();
        $reg_total = json_decode(json_encode($reg_total), TRUE);

        $awakwo = array_slice(array_keys($reg_total[0]), 2);

        foreach($reg_total as $k => $v){
            $pendaftaran['labels'][]   = $v['label'];
        }

        foreach($awakwo as $key => $val){
            $pendaftaran['rows'][$key]['label'] = $val;
            foreach($reg_total as $k => $v){
                $pendaftaran['rows'][$key]['data'][] =  $v[$val];
            }
        }

        dd($pendaftaran);
        // $html           =  'TEST<br>AAAAA<br>UUUU';
        // $filename       = 'TestPDF';
        // $paper          = 'A4';
        // $orientation    = 'potrait';
        // $this->pdf->pdf_create($html, $filename, $paper, $orientation, $stream=TRUE);

    }
    public function read_excel()
	{

		$inputFileType = 'Xlsx';
		$inputFileName = '././assets/uploads/doc/test.xlsx';

		$reader = IOFactory::createReader($inputFileType);
		$reader->setReadDataOnly(true);

		try {
			$spreadsheet = IOFactory::load($inputFileName);
			$worksheetData = $reader->listWorksheetInfo($inputFileName);
			foreach ($worksheetData as $worksheet) {
				$sheetName = $worksheet['worksheetName'];
				$rows = (int) $worksheet['totalRows'];

				$reader->setLoadSheetsOnly($sheetName);
				$spreadsheet = $reader->load($inputFileName);

				$worksheet = $spreadsheet->getActiveSheet();
                $dataArray = $worksheet->toArray();
                // dd( $dataArray);
                $os = ['10','11','12','13','14','15','16'];
                $expire_entrant = '';

                foreach($dataArray as $index => $value){
                    if($index>0){
                        $date = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value[0]);
                        $date = $date->format('Y-m-d H:i:s');
                        $mobil = explode(' ',$value[7]);
                        $tipe = '';
                        $merek = '';
                        foreach($mobil as $k => $v){
                            if($k==0){
                                $merek = $v;
                            } else {
                                $tipe .= $v." ";
                            }
                        }

                        foreach($value as $idx => $v){
                            if(in_array($idx , $os)){
                                if($v!=''){

                                    $class_result = $this->m_global->get_search('kelas','label', $dataArray[0][$idx]);
                                    $id_class = $class_result[0]->id;
                                    
                                    $query  = "SELECT id FROM kategori WHERE label LIKE '%".$v."%' AND id_kelas = '".$id_class."'";
                                    $category_result      	= $this->db->query($query)->result();
                                    
                                }
                            }
                        }

                        if($value[19]!=''){
                            $expire_entrant = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value[19]);
                            $expire_entrant = $expire_entrant->format('Y-m-d');
                        }

                        $data_users = [
                                'email'     =>  $value[1],
                                'full_name'     =>  $value[23],
                                'password'  =>  '123456',
                                'status'    =>  1,
                                'user_group'    =>  '1000',
                                'phone'     =>  $value[3],
                            ];
                        $cek = $this->m_global->get_by_id('users','email', $value[1]);
                        if(!$cek){
                            $id_users = $this->m_crud->insert_id('users', $data_users);
                        } else {
                            $id_users = $cek[0]->id_users;
                        }

                        $data_racer = [
                                'user_created'  =>  $id_users,
                                'email'         =>  $value[1],
                                'full_name'     =>  $value[5],
                                'phone'         =>  $value[3],
                                'kis_imi'       =>  $value[8],
                                'pengprov_imi'  =>  $value[6],
                            ];

                        $cek = $this->m_global->get_by_id('pembalap','full_name', $value[5]);
                        if(!$cek){
                            $id_racers = $this->m_crud->insert_id('pembalap', $data_racer);
                        } else {
                            $id_racers = $cek[0]->id;
                        }    

                        $data_tim = [
                                'user_created'          =>  $id_users,
                                'name'                  =>  ($value[20]!='')?$value[20]:'-',
                                'mng_name'              =>  $value[23],
                                'mng_license'           =>  '-',
                                'mng_phone'             =>  $value[24],
                                'entrant'               =>  $value[17],
                                'entrant_name'          =>  $value[21],
                                'entrant_license'       =>  $value[18],
                                'entrant_license_expire'   => $expire_entrant,
                                'entrant_phone'         =>  $value[22],
                            ];
                        $cek = $this->m_global->get_by_id('tim','name', $data_tim['name']);
                        if(!$cek){
                            $id_teams = $this->m_crud->insert_id('tim', $data_tim);
                        } else {
                            $id_teams = $cek[0]->id;
                        } 

                        $data_registrations = [
                                'created_at'        =>  $date,
                                'no_registration'   =>  $value[2],
                                'no_start'          =>  $value[4],
                                'kendaraan_merek'   =>  $merek,
                                'kendaraan_tipe'    =>  $tipe,
                                'id_kategori'       =>  $category_result[0]->id,
                                'id_pembalap'       =>  $id_racers,
                                'id_tim'            =>  $id_teams,
                                'id_users'          =>  $id_users,
                                'status'            =>  1,
                            ];

                        $id_registrations = $this->m_crud->insert_id('registrations', $data_registrations);

                    }
                }
                // dd($value);



				// foreach($dataArray as $index => $value){
				// 	foreach($value as $idx => $row){
				// 		if($idx>0){
				// 			if(in_array($idx , $os)){
				// 				if($row!=''){
				// 					$name = $dataArray[0][$idx];
				// 					$q 						= "SELECT id FROM class WHERE  name LIKE '%".$name."%' ";

				// 					$event_result 			= $this->db->query($q)->result();
				// 					$id 			= $event_result[0]->id;

				// 					$data = array(
				// 						'id_class' 				=> $id,
				// 						'name' 					=> $row
				// 					);

				// 					$q 							= "SELECT id FROM category WHERE name LIKE '%".$row."%' AND id_class = '".$id."'";
									
				// 					// print_r($q);
				// 					$category_result 			= $this->db->query($q)->result();

				// 					if(!$category_result){
				// 						$query = $this->m_crud->insert('category', $data);
				// 					}
				// 				}
				// 			}
				// 		}
				// 	}
				// }
			}

		} catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
			die('Error loading file: '.$e->getMessage());
		}

		exit;
	}
}
?>