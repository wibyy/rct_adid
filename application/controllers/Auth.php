<?php if(!defined('BASEPATH')) exit('Akses langsung tidak di perkenankan');

class Auth extends CI_Controller {

    public function __construct() {
    	parent::__construct();
		$this->load->library('form_validation');
		$this->load->helper('status');
		$this->load->model('m_global');
		$this->load->model('m_crud');
	}

    public function index() {
        if ($this->session->userdata("sign_in") == TRUE) {
			get_redirecting('');
        }
        else {
			$this->load->view('front/v_header');
			$this->load->view("front/v_login");
			$this->load->view('front/v_footer');
		  }
    }


    function sign_in() {
		$status 	= 'error';
		$message 	= '';

    	$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|min_length[6]|max_length[25]');
		$this->form_validation->set_rules('email',     'Email', 'trim|required|xss_clean|valid_email|min_length[6]|max_length[30]');


    	if ($this->form_validation->run() == TRUE){
          
            $username = $this->input->post('email');
        	$password = $this->input->post('password');
		
      			if(filter_var($username, FILTER_VALIDATE_EMAIL)){
					$check = $this->m_global->get_by_two_id('users', 'email', $username, 'password', hashPassword($password));
      				if (isset($check[0])){
						if($check[0]->status!=1){
							$result 		= '';
							$message 		= 'User not active!';
						}else{
							$this->session->unset_userdata('login');
							$this->session->unset_userdata('id');
							$data = [
								'last_login' => date('Y-m-d H:i:s')
							];

							$query = $this->m_crud->update('users', 'id_users', $data, $check[0]->id_users );
							
							$data = array(
								'sign_in' 	 			=> TRUE,
								'id' 		 			=> simple_encrypt($check[0]->id_users),
								'name' 		 			=> $check[0]->full_name,
								'eo' 		 			=> $check[0]->id_eo,
								'email' 		 		=> $check[0]->email,
								'user_group' 		 	=> $check[0]->user_group,
							);
						    $this->session->set_userdata($data);

							$status 		= 'success';
							$result 		= $data;
							$message 		= 'Login success!';
						}
      				
      				}
      				else{
						  $result 		= '';
						  $message 		= 'Email or Password is wrong!';
      				}
      			}
      			else{
					  $result 		= '';
					  $message 		= 'Please Insert Valid Email.';
      			}
      	}
      	else{
      		$result 		= '';
			$message 		= validation_errors();
      	}
		
	  	$response = response_map($result, $status, $message);

		$response['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;

    }
    
    function sign_out() {
		$this->session->sess_destroy();
		get_redirecting('auth');
    }


}