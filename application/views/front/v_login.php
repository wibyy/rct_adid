<div class="container-fluid" style="position:fixed;height: 100%;width:100%;z-index:999;" id="app">
    <div class="row align-items-center justify-content-md-center" style="height: 100%;">
        <div class="col-md-auto">
            <div class="box-login">
                <div class="progres-login" v-if="isLoad">
                    <b-progress :max="progress.max" height="6px">
                        <b-progress-bar varian="info" animated :value="progress.value" ></b-progress-bar>
                    </b-progress>
                </div>
                <img src="<?php echo base_url('assets/images/logo.png'); ?>" class="img img-responsive" style="width: 160px; margin: 0 0 25px">
                <h3 class="sign-in mb-4">Sign In</h3>
                <form method='post' v-on:submit.prevent="submitProcess()" class=" mb-4">
                    <div class="form-group mb-4">
                        <input type="email" name="email" class="form-control" id="email" v-model="form.email" placeholder="Email">
                    </div>
                    <div class="form-group mb-4">
                        <input type="password" name="password" class="form-control" id="password" v-model="form.password" placeholder="Password">
                    </div>
                    <div class="form-group ">
                        <p class="sub-title-login">No account? <a href="<?php echo base_url('auth/registration'); ?>" class="create-account-btn">Create one!</a></p>
                    </div>
                    <div class="form-group alert-login">
                        <b-alert variant="danger" :show="message!=''" v-html="message"></b-alert>
                    </div>
                    <div class="form-group button-login-box">
                        <button type="submit" class="btn btn-login " v-cloak :disabled="isLoad">{{buttonSubmit}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>