<script>
        var $constant = {
            API_URL: '<?php echo base_url('auth/'); ?>',
            BASE_URL: '<?php echo base_url(); ?>',
            CSRF_NAME : '<?php echo $this->security->get_csrf_token_name(); ?>',
            CSRF_VALUE : '<?php echo $this->security->get_csrf_hash(); ?>'
        };
        </script>
        <script src="<?php echo base_url('assets/admin/vendor/jquery/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/sb-admin-2.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/toastr.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/sweetalert2.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/vue.js'); ?>" ></script>
        <script src="<?php echo base_url('assets/admin/js/vuex.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/axios.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/moment.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/moment-with-locales.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/vue-select.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/bootstrap-vue.min.js'); ?>""></script>
        <script src="<?php echo base_url('assets/admin/js/bootstrap-vue-icons.min.js'); ?>" ></script>
        <script src="<?php echo base_url('assets/admin/js/dashboard/login.js'); ?> " ></script>
  </body>
</html>