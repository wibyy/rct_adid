<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo base_url('assets/images/icon.png'); ?>">
    <title>Adid RCT Scrutineering Master</title>
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/bootstrap.min.css');?>" >
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/datepicker.min.css');?>" >
    <link rel="stylesheet" href="<?php echo base_url('assets/sites/css/custom.css');?>" >
    <link href="<?php echo base_url('assets/admin/css/main.css'); ?>" rel="stylesheet">
    <style>
        [v-cloak] { display: none; }
        .progres-login {
          display: block;
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          z-index: 999;
        }
        .box-login{
          position: relative;
        }
    </style>
  </head>
  <body >
 
<div class="login-bg"></div>