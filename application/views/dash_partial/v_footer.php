
                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>Copyright &copy; Yannuar Wiby Sudarto 2021</span>
                        </div>
                    </div>
                </footer>
                <!-- End of Footer -->

            <!-- End of Content Wrapper -->

                        </div>
                 </div>
        <!-- End of Page Wrapper --> 
 
 
                     <!-- /.container-fluid -->
                     </div>

</div>

<!-- End of Main Content -->
 
 
 
        <!-- Scroll to Top Button-->
        <a class="scroll-to-top rounded" href="#page-top">
            <i class="fas fa-angle-up"></i>
        </a>

        <!-- Logout Modal-->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Anda yakin ingi keluar?</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">Pilih tombol "Logout" dibawah untuk mengakhiri sesi ini.</div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <a class="btn btn-primary" href="<?php echo base_url('auth/sign_out'); ?>"  onClick="localStorage.clear()">Logout</a>
                    </div>
                </div>
            </div>
        </div>
        <script>
        var $constant = {
            API_URL: '<?php echo base_url('ajax/'); ?>',
            BASE_URL: '<?php echo base_url(); ?>',
            USER: '<?php echo json_encode(array_values($this->USER)[0]); ?>',
            PERMISSION_PAGE : '<?php echo json_encode($this->ACCESS_PAGE); ?>',
            USER_GROUP: '<?php echo json_encode(getUserGroup($this->config->item('all_user'))); ?>',
            CSRF_NAME : '<?php echo $this->security->get_csrf_token_name(); ?>',
            CSRF_VALUE : '<?php echo $this->security->get_csrf_hash(); ?>'
        };
        </script>
        <script src="<?php echo base_url('assets/admin/vendor/jquery/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/vendor/chart.js/Chart.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/vendor/chart.js/palette.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/sb-admin-2.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/toastr.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/sweetalert2.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/vue.js'); ?>" ></script>
        <script src="<?php echo base_url('assets/admin/js/vuex.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/axios.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/moment.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/moment-with-locales.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/vue-select.js'); ?>"></script>
        <script src="<?php echo base_url('assets/admin/js/bootstrap-vue.min.js'); ?>""></script>
        <script src="<?php echo base_url('assets/admin/js/bootstrap-vue-icons.min.js'); ?>" ></script>
        <script src="<?php echo base_url('assets/admin/js/main.js'); ?> " ></script>

    <?php echo (isset($script))?$script:''; ?>

</body>

</html>