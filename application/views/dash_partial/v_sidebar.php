<ul class="navbar-nav bg-gradient-primary sidebar sidebar-light accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('admin'); ?>">
        <img src="<?php echo base_url('assets/images/logo.png'); ?>">
    </a>

    <?php foreach($this->MENU as $key => $val){?>

    <hr class="sidebar-divider">
    <div class="sidebar-heading">
        <?php echo $key; ?>
    </div>

    <?php foreach($val as $k => $v){?>

        <li class="nav-item <?php echo ($page == $k) ? 'active' : ''; ?>">
            <a class="nav-link" href="<?php echo  base_url($v['link']); ?>">
                    <?php echo $v['icon']; ?>
                <span><?php echo $v['label']; ?></span></a>
        </li>
    
    <?php } } ?>

    <hr class="sidebar-divider d-none d-md-block">

    <!-- Divider -->


</ul>