<ul class="navbar-nav bg-gradient-primary sidebar sidebar-light accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo base_url('admin'); ?>">
        <img src="<?php echo base_url('assets/images/logo.png'); ?>">
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        Dashboard
    </div>

    <li class="nav-item <?php echo ($page == 'dashboard') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('dashboard'); ?>">
            <i class="fas fa-tachometer-alt"></i>
            <span>Dashboard</span></a>
    </li>
   <!-- Divider -->
   <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        Scrutineering
    </div>    
    <li class="nav-item <?php echo ($page == 'scrutineering') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('scrutineering'); ?>">
            <i class="far fa-folder-open"></i>
            <span>Scrutineering</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        event
    </div>
    <li class="nav-item <?php echo ($page == 'registration') ? 'active' : ''; ?>">
        <a class="nav-link collapsed" href="<?php echo base_url('registration'); ?>" data-toggle="collapse" data-target="#registrationSidebar" aria-expanded="true" aria-controls="collapseTwo">
            <i class="far fa-file-alt"></i>
            <span>Registration</span>
        </a>
        <div id="registrationSidebar" class="collapse <?php echo ($page == 'registration') ? 'show' : ''; ?>" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item <?php echo (isset($sub_page) && $sub_page == 'new_reg') ? 'active' : ''; ?>" href="<?php echo base_url('registration/new'); ?>">New</a>
                <a class="collapse-item <?php echo (isset($sub_page) && $sub_page == 'registration') ? 'active' : ''; ?>" href="<?php echo base_url('registration'); ?>">Record</a>
            </div>
        </div>
    </li>
    <li class="nav-item <?php echo ($page == 'event') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('event'); ?>">
        <i class="fas fa-fw fa-cog"></i>
            <span>Event</span></a>
    </li>
    <li class="nav-item <?php echo ($page == 'eo') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('eo'); ?>">
            <i class="fas fa-users"></i>
            <span>Event Organizer</span></a>
    </li>
    <li class="nav-item <?php echo ($page == 'result') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('result'); ?>">
            <i class="fas fa-history"></i>
            <span>Result</span></a>
    </li>



    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        Masters
    </div>

    <li class="nav-item <?php echo ($page == 'tim') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('tim'); ?>">
            <i class="fas fa-users"></i>
            <span>Tim</span></a>
    </li>
    <li class="nav-item <?php echo ($page == 'pembalap') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('pembalap'); ?>">
            <i class="fas fa-biking"></i>
            <span>Pembalap</span></a>
    </li>
    <li class="nav-item <?php echo ($page == 'scrutmaster') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('scrutmaster'); ?>">
        <i class="far fa-list-alt"></i>
            <span>Scrutineering Master</span></a>
    </li>

    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">
        Setting
    </div>
    <li class="nav-item <?php echo ($page == 'user') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('user'); ?>">
            <i class="fas fa-users-cog"></i>
            <span>Users Manager</span></a>
    </li>
    <li class="nav-item <?php echo ($page == 'live') ? 'active' : ''; ?>">
        <a class="nav-link" href="<?php echo base_url('live'); ?>">
            <i class="far fa-clock"></i>
            <span>Live Timming</span></a>
    </li>



    <hr class="sidebar-divider d-none d-md-block">

    <!-- Divider -->


</ul>