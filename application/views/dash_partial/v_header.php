<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url('assets/images/icon.png'); ?>">

    <title><?php echo $page_title;?> - Admin Demo Bayarind PG</title>

    <!-- Custom fonts for this template-->
    <link href="<?php echo base_url('assets/admin/vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;1,200;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&display=swap" rel="stylesheet"> 
    <!-- Custom styles for this template-->
    <link href="<?php echo base_url('assets/admin/css/sb-admin-2.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/admin/css/toastr.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/admin/css/sweetalert2.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/admin/css/vue-select.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/admin/css/bootstrap-vue.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/admin/css/main.css'); ?>" rel="stylesheet">
    <style>
        [v-cloak] { display: none; }
    </style>

</head>

<body id="page-top">

        <!-- Page Wrapper -->
        <div id="app">
            <div id="wrapper">

            <!-- Sidebar -->
                <?php include 'v_sidebar.php';?>       
             <!-- End of Sidebar -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar -->
                    <?php include 'v_topbar.php';?>  
                    <!-- End of Topbar -->



