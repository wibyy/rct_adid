<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <b-row>
                        <b-col cols="12" lg="3">
                            <div class="img-profile mt-3">
                                <b-img src="<?php echo base_url('assets/images/undraw_male_avatar_323b.svg'); ?>" fluid alt="Responsive image"></b-img>
                            </div>
                            <h3 class="text-center mt-3">{{profileData.full_name}}</h3>
                            <b-button block variant="info" class="mt-3" v-cloak>Edit Profil</b-button>
                            <b-button block variant="info" class="mt-3" v-cloak>Ganti Password</b-button>
                        </b-col>
                        <b-col cols="12" lg="9">
                            <div class="info-profile" v-cloak>
                                <div class="profile-item">
                                    <p class="label">Full Name</p>
                                    <p class="value">{{profileData.full_name}}</p>
                                </div>
                                <div class="profile-item">
                                    <p class="label">User Group</p>
                                    <p class="value">{{profileData.user_group | userGroup}}</p>
                                </div>
                                <div class="profile-item">
                                    <p class="label">Email</p>
                                    <p class="value">{{profileData.email}}</p>
                                </div>
                                <div class="profile-item">
                                    <p class="label">Phone</p>
                                    <p class="value">{{profileData.phone}}</p>
                                </div>
                                <div class="profile-item">
                                    <p class="label">Last Login</p>
                                    <p class="value">{{profileData.last_login | formatDateHumanize}}</p>
                                </div>
                                <div class="profile-item">
                                    <p class="label">Status</p>
                                    <label-status :status-data="profileData.status"></label-status>
                                </div>
                            </div>
                        </b-col>
                    </b-row>
                </div>
            </div>
        </div>
    </div>
</div>