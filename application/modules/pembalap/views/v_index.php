<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-3" v-cloak>
                            <div class="mt-2 mb-3">
                            <?php if (in_array('new', $this->ACCESS_PAGE)) { ?>
                                <button type="button" @click="addNew" class="btn btn-form-info"><i class="fas fa-fw fa-plus"></i> Pembalap</button>
                                <?php } if (in_array('export', $this->ACCESS_PAGE)) { ?>  
                                <a href="" class="btn btn-form-info"><i class="far fa-file-pdf"></i> PDF</a>
                                <?php } ?>
                            </div>
                            <?php if (in_array('filter', $this->ACCESS_PAGE)) { ?>
                            <h4 class="mb-2">Filter : </h4>
                            <b-form @submit.prevent="filterProcess">
                                <b-row>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="Name" label-for="input-1">
                                            <b-form-input id="input-1" v-model="params.nama"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="Pengprov" label-for="input-2">
                                            <b-form-input id="input-2" v-model="params.pengprov"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="4" cols="12">
                                        <div class="mt-4">
                                            <b-button type="reset" @click="resetFormFilter" squared variant="outline-secondary">Reset</b-button>
                                            <b-button type="submit" class="btn btn-form-info" :disabled="!params.nama && !params.pengprov">Filter</b-button>
                                        </div>
                                    </b-col>
                                </b-row>
                            </b-form>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content">
                            <p v-cloak><small>Click on nama lengkap to view detail.</small></p>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%;">#</th>
                                        <th scope="col" style="width: 25%;">Nama Lengkap</th>
                                        <th scope="col" style="width: 15%;">Email </th>
                                        <th scope="col" style="width: 15%;">KIS IMI</th>
                                        <th scope="col" style="width: 15%;">PengProv IMI </th>
                                        <th scope="col" style="width: 15%;">Hp. </th>
                                        <?php if (in_array('edit', $this->ACCESS_PAGE)) { ?>
                                        <th scope="col" style="width: 10%;">Opt.</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                    <tr>
                                        <td colspan="<?php echo (in_array('edit', $this->ACCESS_PAGE)) ? '7' : '6'; ?>">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='baseUrl' :is-load="isLoad" :is-perm="perm" col-span="<?php echo (in_array('edit', $this->ACCESS_PAGE)) ? '7' : '6'; ?>" @click-inactive="processUpdateStatus"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal NEW-->
<div class="modal fade" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="processNew()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Pembalap</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="full_name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="full_name" v-model="form.name">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="name" v-model="form.email" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="phone">Hp</label>
                                <input type="text" class="form-control" id="phone" v-model="form.phone" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <div class="form-group">
                                <label for="kis_imi">KIS IMI</label>
                                <input type="text" class="form-control" id="kis_imi" v-model="form.kis_imi" required>
                            </div>
                        </div>
                        <div class="col-5">
                            <div class="form-group">
                                <label for="phone">Peng Prov.</label>
                                <v-select id="input-group-3" :options="optionProvinsi" v-model="form.pengprov" :reduce="optionProvinsi => optionProvinsi.code"></v-select>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <b-alert variant="danger" dismissible :show="formNewMessage!=''" v-html="formNewMessage"></b-alert>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" v-cloak>{{buttonSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>