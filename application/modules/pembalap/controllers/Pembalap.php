<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Pembalap extends CI_Controller {
	public $page = 'pembalap';
	public $MENU = [];
	public $USER = [];
	public $USER_INFO = [];
	public $USER_GROUP = [];
	public $USER_PERMISSION = [];

	public function __construct() {
		parent::__construct();
		$this->config->load('rct_config');
		$this->USER_INFO 		= $this->session->userdata();
				
		$this->USER  			= getUserGroup([$this->USER_INFO['user_group']]);
		$this->USER_GROUP 		= $this->config->item('user_group');
		$this->USER_PERMISSION  = $this->config->item('user_group')[$this->USER_INFO['user_group']]['access'];
		$this->ACCESS_PAGE		= (isset($this->USER_PERMISSION[$this->page]))?$this->USER_PERMISSION[$this->page]:false;

			if ($this->session->userdata("sign_in") == TRUE && $this->ACCESS_PAGE) {
				
				$this->MENU = generateMenu(array_keys($this->USER_PERMISSION));
				$this->load->helper('status');
				$this->load->model('m_crud');
				$this->load->model('m_global');
				
			}else{
				get_redirecting('auth');
			}
	}

	public function index(){
		$data['page_title'] = 'Pembalap';
		$data['page'] = $this->page;
		$data['sub_page'] = 'pembalap';
		$data['script'] = '<script src="'.base_url("assets/admin/js/dashboard/pembalap.js").'"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page.'/v_index', $data);
		$this->load->view('../../views/dash_partial/v_footer');
	}

}