<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Region extends CI_Controller {
	public $CONF; 	
	public $CUST;
	public $page = 'region';

	public function __construct() {
		parent::__construct();

			if ($this->session->userdata("sign_in") == TRUE) {
				$this->load->helper('status');
				$this->load->model('m_crud');
				$this->load->model('m_global');
			}else{
				get_redirecting('auth');
				
			}
	}

	public function index(){
		$data['page_title'] = 'Region List';
		$data['page'] = $this->page;
		$data['script'] = '<script src="'.base_url("assets/admin/js/dashboard/region.js").'"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page.'/v_index');
		$this->load->view('../../views/dash_partial/v_footer');
	}

	public function get_provinsi_ajax(){

		$response['status'] 	= 'error';
		$response['message'] = '';
		
		$query = "SELECT * FROM wilayah_provinsi as p WHERE   1=1 ";

		$name 	= $this->input->get('name', '');
		$sort 	= $this->input->get('sort', '');

		if($name){
			$query.=" and p.nama like '%".$name."%'";
		} 		

		$page = $this->input->get('page'); 
		$page = ($page)?intval($page):1; 
		$limit = $this->input->get('limit'); 
		$limit = ($limit)?intval($limit):10;

		$total_all 	= $this->db->query($query)->num_rows();

		if($sort=='za'){
			$query.=" order by p.nama DESC ";
		} else{
			$query.=" order by p.nama ASC ";
		}
		
		$offset = ($page) ? ( ( $page - 1 ) * $limit ) : 0;

		$query .= " LIMIT ".$limit." OFFSET ". $offset;

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();
		
		if(!!$query_result){

			$response = list_response_map($query_result, $total_all, $page, $total, $limit);

		} else {
			$response['message'] 	= "Can't get data!";
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}	

	public function get_kota_ajax(){

		$response['status'] 	= 'error';
		$response['message'] 	= '';
		
		$query = "SELECT * FROM wilayah_kabupaten WHERE   1=1 ";

		$name 	= $this->input->get('name');
		$id 	= $this->input->get('id');
		$sort 	= $this->input->get('sort');

		if($name){
			$query.=" and nama like '%".$name."%'";
		} 		
		if($id){
			$id.=" and provinsi_id = '".$id."'";
		} 		

		$page = $this->input->get('page'); 
		$page = ($page)?intval($page):1; 
		$limit = $this->input->get('limit'); 
		$limit = ($limit)?intval($limit):10;

		$total_all 	= $this->db->query($query)->num_rows();

		if($sort == 'az'){
			$query .= " order by nama ASC ";
		} if($sort == 'za'){
			$query .= " order by  nama DESC ";
		}else{
			$query .= " order by provinsi_id ASC, nama ASC ";
		}
		
		$offset = ($page) ? ( ( $page - 1 ) * $limit ) : 0;

		$query .= " LIMIT ".$limit." OFFSET ". $offset;

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();
		
		if(!!$query_result){
			$response = list_response_map($query_result, $total_all, $page, $total, $limit);
		} else {
			$response['message'] 	= "Can't get data!";
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}	

	public function get_kecamatan_ajax(){

		$response['status'] 	= 'error';
		$response['message'] 	= '';
		
		$query = "SELECT * FROM wilayah_kecamatan WHERE   1=1 ";

		$name 	= $this->input->get('name');
		$id 	= $this->input->get('id');
		$sort 	= $this->input->get('sort');

		if($name){
			$query.=" and nama like '%".$name."%'";
		} 		
		if($id){
			$query.=" and kabupaten_id = '".$id."'";
		} 		

		$page = $this->input->get('page'); 
		$page = ($page)?intval($page):1; 
		$limit = $this->input->get('limit'); 
		$limit = ($limit)?intval($limit):10;

		$total_all 	= $this->db->query($query)->num_rows();

		if($sort == 'az'){
			$query .= " order by nama ASC ";
		} if($sort == 'za'){
			$query .= " order by  nama DESC ";
		}else{
			$query .= " order by kabupaten_id ASC, nama ASC ";
		}
		
		$offset = ($page) ? ( ( $page - 1 ) * $limit ) : 0;

		$query .= " LIMIT ".$limit." OFFSET ". $offset;

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();
		
		if(!!$query_result){
			$response = list_response_map($query_result, $total_all, $page, $total, $limit);
		} else {
			$response['message'] 	= "Can't get data!";
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}	

	public function get_desa_ajax(){

		$response['status'] 	= 'error';
		$response['message'] 	= '';
		
		$query = "SELECT * FROM wilayah_desa WHERE   1=1 ";

		$name 	= $this->input->get('name');
		$id 	= $this->input->get('id');
		$sort 	= $this->input->get('sort');

		if($name){
			$query.=" and nama like '%".$name."%'";
		} 		
		if($id){
			$query.=" and kecamatan_id = '".$id."'";
		} 		

		$page = $this->input->get('page'); 
		$page = ($page)?intval($page):1; 
		$limit = $this->input->get('limit'); 
		$limit = ($limit)?intval($limit):10;

		$total_all 	= $this->db->query($query)->num_rows();

		if($sort == 'az'){
			$query .= " order by nama ASC ";
		} if($sort == 'za'){
			$query .= " order by  nama DESC ";
		}else{
			$query .= " order by kecamatan_id ASC, nama ASC ";
		}
		
		$offset = ($page) ? ( ( $page - 1 ) * $limit ) : 0;

		$query .= " LIMIT ".$limit." OFFSET ". $offset;

		$total 				= $this->db->query($query)->num_rows();
		$query_result      	= $this->db->query($query)->result();
		
		if(!!$query_result){
			$response = list_response_map($query_result, $total_all, $page, $total, $limit);
		} else {
			$response['message'] 	= "Can't get data!";
		}

		$this->output
			->set_status_header(200)
			->set_content_type('application/json', 'utf-8')
			->set_output(json_encode($response, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES))
			->_display();
		exit;
	}	
	public function excel()
	{

		

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Nama');
		
		$result = $this->m_global->get_all('wilayah_provinsi');
		$no = 1;
		$x = 2;
		foreach($result as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->nama);
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'region';
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	public function read_excel()
	{

		$inputFileType = 'Xlsx';
		$inputFileName = '././././assets/uploads/doc/test.xlsx';

		$reader = IOFactory::createReader($inputFileType);
		$reader->setReadDataOnly(true);

		try {
			$spreadsheet = IOFactory::load($inputFileName);
			$worksheetData = $reader->listWorksheetInfo($inputFileName);
			foreach ($worksheetData as $worksheet) {
				$sheetName = $worksheet['worksheetName'];
				$rows = (int) $worksheet['totalRows'];

				$reader->setLoadSheetsOnly($sheetName);
				$spreadsheet = $reader->load($inputFileName);

				$worksheet = $spreadsheet->getActiveSheet();
				$dataArray = $worksheet->toArray();
				$os =['10','11','12','13','14','15','16'];
				foreach($dataArray as $index => $value){
					foreach($value as $idx => $row){
						if($idx>0){
							if(in_array($idx , $os)){
								if($row!=''){
									$name = $dataArray[0][$idx];
									$q 						= "SELECT id FROM class WHERE  name LIKE '%".$name."%' ";

									$event_result 			= $this->db->query($q)->result();
									$id 			= $event_result[0]->id;

									$data = array(
										'id_class' 				=> $id,
										'name' 					=> $row
									);

									$q 							= "SELECT id FROM category WHERE name LIKE '%".$row."%' AND id_class = '".$id."'";
									
									// print_r($q);
									$category_result 			= $this->db->query($q)->result();

									if(!$category_result){
										$query = $this->m_crud->insert('category', $data);
									}
								}
							}
						}
					}
				}
			}

		} catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
			die('Error loading file: '.$e->getMessage());
		}

		exit;
	}
}