<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content d-block">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">ID</th>
                                        <th scope="col">Name</th>
                                    </tr>
                                </thead>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData'>
                                    <tr>
                                        <td colspan="3" style="text-align:center">
                                            <p>Loading data...</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>