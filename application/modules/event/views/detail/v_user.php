<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-3">
                <?php include "v_sidebar_event.php";?>
        </div>
        <div class="col-9">
            <div class="card shadow mb-4">
                <div class="card-body">

                <div class="row">
                        <div class="col-12 mb-3" v-cloak>
                            <div class="mt-2 mb-3">
                                <!-- <button type="button" @click="addNewUser" class="btn btn-form-info"><i class="fas fa-fw fa-plus"></i> User</button> -->
                                <a href="" class="btn btn-form-info"><i class="far fa-file-pdf"></i> PDF</a>
                            </div>
                            <h4 class="mb-2">Filter : </h4>
                            <b-form @submit.prevent="filterProcess">
                                <b-row>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="Name" label-for="input-1">
                                            <b-form-input id="input-1" v-model="params.nama"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="Email" label-for="input-2">
                                            <b-form-input id="input-2" v-model="params.email"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="4" cols="12">
                                        <div class="mt-4">
                                            <b-button type="reset" @click="resetFormFilter" squared variant="outline-secondary" :disabled="!params.nama && !params.email">Reset</b-button>
                                            <b-button type="submit" class="btn btn-form-info" :disabled="!params.nama && !params.email">Filter</b-button>
                                        </div>
                                    </b-col>
                                </b-row>
                            </b-form>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content">
                            <p v-cloak><small>Click on email to view detail.</small></p>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%;">#</th>
                                        <th scope="col" style="width: 15%;">Email </th>
                                        <th scope="col" style="width: 15%;">Full Name</th>
                                        <th scope="col" style="width: 10%;">EO Name</th>
                                        <th scope="col" style="width: 10%;">User Group</th>
                                        <th scope="col" style="width: 10%;">Phone </th>
                                        <th scope="col" style="width: 6%;">Status</th>
                                        <th scope="col" style="width: 10%;">Opt.</th>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                    <tr>
                                        <td colspan="8">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='baseUrl' :is-load="isLoad"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
