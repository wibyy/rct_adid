<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-3">
            <?php include "v_sidebar_event.php"; ?>
        </div>
        <div class="col-9">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-3" v-cloak>
                            <div class="mt-2 mb-3">
                            <?php if (in_array('new', $this->ACCESS_PAGE)) { ?>
                                <button type="button" @click="addNew" class="btn btn-form-info"><i class="fas fa-fw fa-plus"></i> Kategori</button>
                            <?php } if (in_array('export', $this->ACCESS_PAGE)) { ?>    
                                <a href="" class="btn btn-form-info"><i class="far fa-file-pdf"></i> PDF</a>
                                <?php } ?>
                            </div>
                            <b-form @submit.prevent="filterProcess">
                                <b-row>
                                    <b-col col lg="3" cols="12">
                                        <label for="input-group-3">Pilih kelas terlebih dahulu :</label>
                                        <v-select id="input-group-3" :options="optionsClass" v-model="selectedKelas" :reduce="optionsClass => optionsClass.code" @input="kelasProcess"></v-select>
                                    </b-col>
                                </b-row>
                            <?php if (in_array('filter', $this->ACCESS_PAGE)) { ?>
                                <b-row>
                                    <b-col col cols="12" class="mt-3">
                                        <h4 class="mb-2">Filter : </h4>
                                    </b-col>
                                </b-row>
                                <b-row>
                                    <b-col col lg="4" cols="12">
                                        <b-form-group id="input-group-1" label="Nama Kategori" label-for="input-1">
                                            <b-form-input id="input-1" v-model="params.label"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3" cols="12">
                                        <div class="mt-4">
                                            <b-button type="reset" @click="resetFormFilter" squared variant="outline-secondary">Reset</b-button>
                                            <b-button type="submit" class="btn btn-form-info" :disabled="!params.label && !params.status">Filter</b-button>
                                        </div>
                                    </b-col>
                                </b-row>
                                
                            <?php } ?>
                            </b-form>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content d-block">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 10%;">#</th>
                                        <th scope="col" style="width: 60%;">Name</th>
                                        <?php if (in_array('edit', $this->ACCESS_PAGE)) { ?>
                                        <th scope="col" style="width: 30%;text-align:center">Option</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                    <tr>
                                        <td colspan="<?php echo (in_array('edit', $this->ACCESS_PAGE)) ? '3' : '2'; ?>">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='baseUrl' :is-load="isLoad" :is-perm="perm" col-span="<?php echo (in_array('edit', $this->ACCESS_PAGE)) ? '3' : '2'; ?>" @click-edit="updateItems"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="processNew()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="label">Label</label>
                                <input type="text" class="form-control" id="label" v-model="form.label">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b-alert variant="danger" dismissible :show="formNewMessage!=''" v-html="formNewMessage"></b-alert>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" v-cloak :disabled="isLoad">{{buttonSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="updateItemProcess()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="label">Label</label>
                                <input type="text" class="form-control" id="label" v-model="formEdit.label">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b-alert variant="danger" dismissible :show="formEditMessage!=''" v-html="formEditMessage"></b-alert>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" v-cloak :disabled="isLoad">{{buttonEditSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>