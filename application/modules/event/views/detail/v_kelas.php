<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-3">
            <?php include "v_sidebar_event.php"; ?>
        </div>
        <div class="col-9">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-3" v-cloak>
                            <div class="mt-2 mb-3">
                            <?php if (in_array('new', $this->ACCESS_PAGE)) { ?>
                                <button type="button" @click="addNew" class="btn btn-form-info"><i class="fas fa-fw fa-plus"></i> Kelas</button>
                            <?php } if (in_array('export', $this->ACCESS_PAGE)) { ?>
                                <a href="" class="btn btn-form-info"><i class="far fa-file-pdf"></i> PDF</a>
                                <?php } ?>
                            </div>
                            <?php if (in_array('filter', $this->ACCESS_PAGE)) { ?>
                            <h4 class="mb-2">Filter : </h4>
                            <b-form @submit.prevent="filterProcess">
                                <b-row>
                                    <b-col col lg="4" cols="12">
                                        <b-form-group id="input-group-1" label="Nama Kelas" label-for="input-1">
                                            <b-form-input id="input-1" v-model="params.label"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="Status" label-for="input-status">
                                            <b-form-select id="input-status" v-model="params.status" :options="optionsStatus"></b-form-select>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3" cols="12">
                                        <div class="mt-4">
                                            <b-button type="reset" @click="resetFormFilter" squared variant="outline-secondary">Reset</b-button>
                                            <b-button type="submit" class="btn btn-form-info" :disabled="!params.label && !params.status">Filter</b-button>
                                        </div>
                                    </b-col>
                                </b-row>
                            </b-form>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content d-block">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col"  style="width: 10%;">#</th>
                                        <th scope="col"  style="width: 40%;">Name</th>
                                        <th scope="col"  style="width: 20%;">Price</th>
                                        <th scope="col"  style="width: 15%;">Status</th>
                                        <?php if (in_array('edit', $this->ACCESS_PAGE)) { ?>
                                        <th scope="col"  style="width: 15%;text-align:center">Option</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                    <tr>
                                        <td colspan="<?php echo (in_array('edit', $this->ACCESS_PAGE)) ? '5' : '4'; ?>">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='kategoriUrl' :is-load="isLoad" :is-perm="perm" col-span="<?php echo (in_array('edit', $this->ACCESS_PAGE)) ? '5' : '4'; ?>" @click-edit="updateItems"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="newItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="processNew()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="label">Label</label>
                                <input type="text" class="form-control" id="label" v-model="form.label">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="sub_label">Sub Label</label>
                                <input type="text" class="form-control" id="sub_label" v-model="form.sub_label">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="biaya">Biaya</label>
                                <input type="number" class="form-control" id="biaya" v-model="form.biaya">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="form-group col-md-6">
                            <label for="scrutineering_admin">Scruiteneering Admin</label>
                            <select id="scrutineering_admin" class="form-control" v-model="form.scrutineering_admin">
                                <option value="" selected>-No Selected-</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="scrutineering_teknis">Scruiteneering Teknis</label>
                            <select id="scrutineering_teknis" class="form-control" v-model="form.scrutineering_teknis">
                                <option value="" selected>-No Selected-</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-12">
                            <b-alert variant="danger" dismissible :show="formNewMessage!=''" v-html="formNewMessage"></b-alert>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" v-cloak :disabled="isLoad">{{buttonSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="updateItemProcess()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Kelas</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="label">Label</label>
                                <input type="text" class="form-control" id="label" v-model="formEdit.label" >
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="sub_label">Sub Label</label>
                                <input type="text" class="form-control" id="sub_label" v-model="formEdit.sub_label">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="price">Biaya</label>
                                <input type="number" class="form-control" id="price" v-model="formEdit.biaya">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="form-group col-md-6">
                            <label for="scrutineering_admin">Scruiteneering Admin</label>
                            <select id="scrutineering_admin" class="form-control" v-model="formEdit.scrutineering_admin">
                                <option value="" selected>-No Selected-</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="scrutineering_teknis">Scruiteneering Teknis</label>
                            <select id="scrutineering_teknis" class="form-control" v-model="formEdit.scrutineering_teknis">
                                <option value="" selected>-No Selected-</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-12">
                            <b-alert variant="danger" dismissible :show="formEditMessage!=''" v-html="formEditMessage"></b-alert>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" v-cloak :disabled="isLoad">{{buttonEditSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>