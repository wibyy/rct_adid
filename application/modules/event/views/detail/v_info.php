<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-3">
            <?php include "v_sidebar_event.php"; ?>
        </div>
        <div class="col-9">
            <div class="card shadow mb-4"  v-bind:class="{'is-load' : isLoad}">
                <div class="progress-bar-card" v-if="isLoad">
                    <b-progress :max="progress.max">
                        <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                    </b-progress>
                </div>
                <div class="card-body" v-cloak>
                    <b-row class="mb-5">
                        <b-col cols="4">
                            <b-img :src="eventData.logo" fluid></b-img>
                        </b-col>
                    </b-row>
                    <b-row class="mt-2 mb-3">
                        <b-col cols="12">
                            <div class="event-info-item">
                                <p class="title">Code</p>
                                <p class="value">{{eventData.event_code}}</p>
                            </div>
                            <div class="event-info-item">
                                <p class="title">Label</p>
                                <p class="value">{{eventData.label}}</p>
                            </div>
                            <div class="event-info-item">
                                <p class="title">Sub Label</p>
                                <p class="value">{{eventData.sub_label}}</p>
                            </div>
                            <div class="event-info-item">
                                <p class="title">Lokasi</p>
                                <p class="value">{{eventData.lokasi}}</p>
                            </div>
                            <div class="event-info-item">
                                <p class="title">Tanngal </p>
                                <p class="value">{{eventData.date_start | localShortDate }} {{'s/d'}} {{eventData.date_end | localShortDate }}</p>
                            </div>
                        </b-col>
                    </b-row>
                </div>
            </div>
        </div>
    </div>
</div>