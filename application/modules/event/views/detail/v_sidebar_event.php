
<div class="card shadow mb-4">
    <div class="card-body box-filter">
        <div class="header-box-filter">
            <p><i class="fas fa-calendar-week"></i> Event Detail</p>
        </div>
        <div class="body-box-filter">
            <div class="list-group">
                <a href="<?php echo base_url('event/detail/info/'.$id_event);?>" class="list-group-item list-group-item-action <?php echo ($select_page=='info')?'active':'';?>">Info</a>
                <a href="<?php echo base_url('event/detail/kelas/'.$id_event);?>" class="list-group-item list-group-item-action <?php echo ($select_page=='kelas')?'active':'';?>">Kelas</a>
                <a href="<?php echo base_url('event/detail/kategori/'.$id_event);?>" class="list-group-item list-group-item-action <?php echo ($select_page=='kategori')?'active':'';?>">Kategori</a>
                <a href="<?php echo base_url('event/detail/user/'.$id_event);?>" class="list-group-item list-group-item-action <?php echo ($select_page=='user')?'active':'';?>">Event Organizer User</a>
                <a href="<?php echo base_url('event/detail/pendaftaran/'.$id_event);?>" class="list-group-item list-group-item-action <?php echo ($select_page=='pendaftaran')?'active':'';?>">Pendaftaran</a>
                <a href="<?php echo base_url('event/detail/hasil/'.$id_event);?>" class="list-group-item list-group-item-action <?php echo ($select_page=='hasil')?'active':'';?>">Hasil</a>

            </div>
        </div>
    </div>
</div>

<a href="<?php echo base_url('event');?>" class="btn btn-dark mb-3 d-block"><i class="fas fa-arrow-left"></i> Kembali ke list </a>