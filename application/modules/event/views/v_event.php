<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-3" v-cloak>
                            <div class="mt-2 mb-3">
                                <?php if (in_array('new', $this->ACCESS_PAGE)) { ?>
                                    <button type="button" @click="addNew" class="btn btn-form-info"><i class="fas fa-fw fa-plus"></i> Event</button>
                                <?php }
                                if (in_array('export', $this->ACCESS_PAGE)) { ?>
                                    <a href="" class="btn btn-form-info"><i class="far fa-file-pdf"></i> PDF</a>
                                <?php } ?>
                            </div>
                            <?php if (in_array('filter', $this->ACCESS_PAGE)) { ?>
                                <h4 class="mb-2">Filter : </h4>
                                <b-form @submit.prevent="filterProcess">
                                    <b-row>
                                        <b-col col lg="3" cols="12">
                                            <b-form-group id="input-group-1" label="Nama Event" label-for="input-1">
                                                <b-form-input id="input-1" v-model="params.label"></b-form-input>
                                            </b-form-group>
                                        </b-col>
                                        <b-col col lg="3" cols="12">
                                            <b-form-group id="input-group-1" label="Tanggal Event" label-for="input-tanggal">
                                                <b-form-datepicker id="input-tanggal" v-model="params.date" class="mb-2"></b-form-datepicker>
                                            </b-form-group>
                                        </b-col>
                                        <b-col col lg="2" cols="12">
                                            <b-form-group id="input-group-1" label="Status" label-for="input-status">
                                                <b-form-select id="input-status" v-model="params.status" :options="optionsStatus"></b-form-select>
                                            </b-form-group>
                                        </b-col>
                                        <b-col col lg="3" cols="12">
                                            <div class="mt-4">
                                                <b-button type="reset" @click="resetFormFilter" squared variant="outline-secondary">Reset</b-button>
                                                <b-button type="submit" class="btn btn-form-info" :disabled="!params.label && !params.date && !params.status">Filter</b-button>
                                            </div>
                                        </b-col>
                                    </b-row>
                                </b-form>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content d-block">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%;">#</th>
                                        <th scope="col" style="width: 10%;">Logo</th>
                                        <th scope="col" style="width: 10%;">Code</th>
                                        <th scope="col" style="width: 22%;">Name</th>
                                        <th scope="col" style="width: 12%;">Location</th>
                                        <th scope="col" style="width: 15%;">Date</th>
                                        <th scope="col" style="width: 10%; text-align:center">Status</th>
                                        <?php if (in_array('edit', $this->ACCESS_PAGE) && in_array('approval', $this->ACCESS_PAGE)) { ?>
                                            <th scope="col" style="width: 15%; text-align:center">Option</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                <?php $colspan = (in_array('edit', $this->ACCESS_PAGE) && in_array('approval', $this->ACCESS_PAGE)) ? '8' : '7'; ?>
                                    <tr>
                                        <td colspan="<?php echo $colspan; ?>">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='baseUrl' :is-load="isLoad" :is-perm="perm" col-span="<?php echo $colspan; ?>" @click-edit="updateItems" @click-inactive="processUpdateStatus"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="newItems" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="submitProcess()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <img :src="imageURL" alt="" style="max-height: 200px;max-width:200px">
                            <div class="form-group">
                                <label for="logo">Upload Logo</label>
                                <input type="file" class="form-control-file" id="imageFile" name="imageFile" ref="imageFile" @change="onFileChangeImage" accept="image/*">
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Event Name</label>
                                        <input type="text" class="form-control" id="name" v-model="form.name" placeholder="Enter Event Name">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="sub_name">Event Sub Name</label>
                                        <input type="text" class="form-control" id="sub_name" v-model="form.sub_name" placeholder="Enter Sub Event Name">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" class="form-control" id="location" v-model="form.location" placeholder="Enter Location">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="date_start">Start Date</label>
                                        <input type="date" class="form-control" id="date_start" v-model="form.date_start">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="date_end">End Date</label>
                                        <input type="date" class="form-control" id="date_end" v-model="form.date_end">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" v-cloak>{{buttonSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="editItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="updateItemProcess()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-4">
                            <img :src="imageEditURL" alt="" style="max-height: 200px;max-width:200px" class="mb-3">
                            <div class="form-group">
                                <input type="file" class="form-control-file" id="imageEditFile" name="imageEditFile" ref="imageEditFile" @change="onFileChangeEditImage" accept="image/*">
                            </div>
                        </div>
                        <div class="col-8">
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="name">Event Name</label>
                                        <input type="text" class="form-control" id="name" v-model="formEdit.name" placeholder="Enter Event Name">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="sub_name">Event Sub Name</label>
                                        <input type="text" class="form-control" id="sub_name" v-model="formEdit.sub_name" placeholder="Enter Sub Event Name">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="location">Location</label>
                                        <input type="text" class="form-control" id="location" v-model="formEdit.location" placeholder="Enter Location">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="date_start">Start Date</label>
                                        <input type="date" class="form-control" id="date_start" v-model="formEdit.date_start">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="date_end">End Date</label>
                                        <input type="date" class="form-control" id="date_end" v-model="formEdit.date_end">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" v-cloak>{{buttonEditSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>