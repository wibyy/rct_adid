<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller
{
	public $ID_EVENT = '';
	public $page = 'event';
	public $MENU = [];
	public $USER = [];
	public $USER_INFO = [];
	public $USER_GROUP = [];
	public $USER_PERMISSION = [];

	public function __construct() {
		parent::__construct();
		$this->config->load('rct_config');
		$this->USER_INFO 		= $this->session->userdata();
				
		$this->USER  			= getUserGroup([$this->USER_INFO['user_group']]);
		$this->USER_GROUP 		= $this->config->item('user_group');
		$this->USER_PERMISSION  = $this->config->item('user_group')[$this->USER_INFO['user_group']]['access'];
		$this->ACCESS_PAGE		= (isset($this->USER_PERMISSION[$this->page]))?$this->USER_PERMISSION[$this->page]:false;

			if ($this->session->userdata("sign_in") == TRUE && $this->ACCESS_PAGE) {
				
				$this->MENU = generateMenu(array_keys($this->USER_PERMISSION));
				$this->load->helper('status');
				$this->load->model('m_crud');
				$this->load->model('m_global');
				
			}else{
				get_redirecting('auth');
			}
	}

	public function index()
	{
		$data['page_title'] = 'Event List';
		$data['page'] = $this->page;
		$data['sub_page'] = 'event';
		$data['script'] = '<script src="' . base_url("assets/admin/js/dashboard/event.js") . '"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page . '/v_event');
		$this->load->view('../../views/dash_partial/v_footer');
	}
	public function detail($page = 'info', $id)
	{
		$data['page_title'] = 'Event List';
		$data['page'] = $this->page;
		$data['sub_page'] = 'event';
		$data['select_page'] = $page;
		$data['id_event'] = $id;
		$data['script'] = '<script>var PAGE_ID = '.$id.' </script>';
		$data['script'] .= '<script src="' . base_url("assets/admin/js/dashboard/event-detail-".$page.".js") . '"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page . '/detail/v_' .$page);
		$this->load->view('../../views/dash_partial/v_footer', $data);
	}
	public function class($id='')
	{
		$data['page_title'] = 'Class List';
		$data['page'] = $this->page;
		$data['sub_page'] = 'class';
		$data['script'] = '<script> var EVENT_ID = "'.$id.'"</script>';
		$data['script'] .= '<script src="' . base_url("assets/admin/js/dashboard/event-class.js") . '"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page . '/v_class');
		$this->load->view('../../views/dash_partial/v_footer', $data);
	}
	public function category($id='')
	{
		$data['page_title'] = 'Category List';
		$data['page'] = $this->page;
		$data['sub_page'] = 'category';
		$data['script'] = '<script> var CLASS_ID = "'.$id.'"</script>';
		$data['script'] .= '<script src="' . base_url("assets/admin/js/dashboard/event-category.js") . '"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page . '/v_category');
		$this->load->view('../../views/dash_partial/v_footer');
	}
	public function read_txt()
	{
		$inputFileType = 'txt';
		$inputFileName = '././././assets/uploads/doc/test2.txt';

		$str = file_get_contents($inputFileName);
		$row_arr = explode("\n", $str);

		$arr = array();
		$title = array();
		$data = array();

		foreach($row_arr as $idx => $val){
			if($idx==0){
				$title = explode("\t",$val);
			} else {
				$data = explode("\t",$val);
				foreach($data as $i => $v){
					$arr[$idx-1][$title[$i]] = $v;
				}
			}

		}

		echo "<pre>";
		print_r(json_encode($arr));
		echo "</pre>";
	}
}
