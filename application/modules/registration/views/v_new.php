<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-12 col-md-4">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold"> <i class="fas fa-fw fa-edit"></i> Pilih event</h6>
                </div>
                <div class="card-body  pb-5">
                    <b-col col lg="12" class=" mb-2">
                        <label for="input-group-1" v-cloak>Event Code:</label>
                        <b-input-group class="mt-3">
                            <b-form-input id="input-group-1" v-model="form.code" placeholder="Enter Event Code" :state="eventCheck" required></b-form-input>
                            <b-input-group-append>
                                <b-button type="button" variant="info" @click="getEvent">
                                    Get Event
                                    <b-spinner variant="light" label="Spinning" small v-if="isLoadEvent"></b-spinner>
                                </b-button>
                            </b-input-group-append>
                        </b-input-group>
                    </b-col>
                    <b-col col lg="12" class="mb-2">
                        <i style="font-size: 10pt;color;#222;" v-cloak> {{EventSelectedName}} </i>
                    </b-col>
                    <b-col col lg="12" class="mt-3">
                        <label for="input-group-3" v-cloak>Kelas :</label>
                        <v-select id="input-group-3" :options="optionsClass" v-model="form.kelas" :reduce="optionsClass => optionsClass.code" @input="classProcess"></v-select>
                    </b-col>
                    <b-col col lg="12" class="mt-3">
                        <label for="input-group-4" v-cloak>Kategori :</label>
                        <v-select id="input-group-4" :options="optionsCategory" v-model="form.kategori" :reduce="optionsCategory => optionsCategory.code" @input="categoryProcess"></v-select>
                    </b-col>
                    <hr>
                    <b-col col lg="12" class="mb-2">
                        <b style="font-size: 12pt;color;#222;" v-cloak> {{listEvent.label}} </b>
                        <p style="font-size: 10pt;color;#222;" v-cloak> {{listEvent.sub_label}} </p>
                        <p style="font-size: 10pt;color;#222;" v-cloak> {{listEvent.lokasi}} </p>
                        <p style="font-size: 10pt;color;#222;" v-cloak v-if="listEvent"> {{listEvent.date_start | formatDate }} {{ 's/d' }} {{listEvent.date_end | formatDate }}</p>
                    </b-col>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold"> <i class="fas fa-fw fa-edit"></i> Formulir Registrasi</h6>
                </div>
                <div class="card-body">
                    <div class="d-block" style="margin-bottom: 50px;">
                        <b-form-group id="fieldset-horizontal7" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Nomer Start" label-for="input-1">
                            <b-form-input id="input-1" v-model="form.no_start"></b-form-input>
                        </b-form-group>
                        <b-form-group id="fieldset-horizontal" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Nama Pembalap" label-for="input-horizontal" description="Untuk menambahkan nama pembalap di menu pembalap">
                            <v-select id="input-group-1" :options="optionPembalap" v-model="form.pembalap" :reduce="optionPembalap => optionPembalap.code"></v-select>
                        </b-form-group>
                        <b-form-group id="fieldset-horizontal2" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Nama Tim" label-for="input-horizontal2" description="Untuk menambahkan tim di menu tim">
                            <v-select id="input-group-3" :options="optionTim" v-model="form.tim" :reduce="optionTim => optionTim.code"></v-select>
                        </b-form-group>
                        <b-form-group id="fieldset-horizontal4" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Merek Mobil" label-for="input-horizontal4">
                            <b-form-input id="input-horizontal1" v-model="form.merek"></b-form-input>
                        </b-form-group>
                        <b-form-group id="fieldset-horizontal5" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Tipe Mobil" label-for="input-horizontal5">
                            <b-form-input id="input-horizontal2" v-model="form.tipe"></b-form-input>
                        </b-form-group>
                        <b-form-group id="fieldset-horizontal6" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Tahun" label-for="input-horizontal3">
                            <b-form-input id="input-horizontal3" v-model="form.tahun"></b-form-input>
                        </b-form-group>
                        <b-form-group id="fieldset-horizontal7" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Nomor Rangka" label-for="input-horizontal4">
                            <b-form-input id="input-horizontal4" v-model="form.rangka"></b-form-input>
                        </b-form-group>
                        <b-form-group id="fieldset-horizontal8" label-cols-sm="4" label-cols-lg="3" content-cols-sm content-cols-lg="8" label="Nomor Mesin" label-for="input-horizontal5">
                            <b-form-input id="input-horizontal5" v-model="form.mesin"></b-form-input>
                        </b-form-group>
                    </div>
                    <hr>
                    <div class="form-group" v-cloak>
                        <button type="button" class="btn btn-form-default" @click="saveForm">Simpan & Buat Baru</button>
                        <button type="button" class="btn btn-form-info" @click="saveOutForm">Simpan & Keluar</button>
                        <button type="button" class="btn btn-form-reset" @click="resetForm">Reset</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>