<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-lg-4 col-xs-12" v-cloak>
                            <p class="mb-2">Choose a events</p>
                            <v-select :options="option" v-model="params.id" :reduce="option => option.code" @input="eventProcess"></v-select>
                            <p><small>Please choose a event first!</small></p>
                        </div>
                    </div>
                    <div class="row" v-if="params.id">
                        <div class="col-12 mb-3" v-cloak>
                            <b-form >
                                <b-row>
                                    <b-col col lg="2"  cols="12">
                                        <b-form-group id="input-group-1" label="No Reg. :" label-for="input-1">
                                            <b-form-input id="input-1" v-model="params.no_reg" type="text" placeholder="No Reg."></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-22" label="Nama Pembalap :" label-for="input-22">
                                            <b-form-input id="input-22" v-model="params.nama"  type="text" placeholder="Nama"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3"  cols="12">
                                        <b-form-group id="input-group-2" label="Tim :" label-for="input-2">
                                            <b-form-input id="input-2" v-model="params.tim"  type="text" placeholder="Tim"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="2"  cols="12" style="margin-top: -2px;">
                                        <label for="input-group-3">Class :</label>
                                        <v-select id="input-group-3" :options="optionsClass" v-model="params.kelas" :reduce="optionsClass => optionsClass.code"></v-select>
                                    </b-col>
                                    <b-col col lg="2"  cols="12">
                                        <div class="mt-4">
                                            <b-button type="button" @click="resetFormFilter" squared variant="outline-secondary">Reset</b-button>
                                            <b-button type="button" @click="filterProcess" squared  variant="info">Filter</b-button>
                                        </div>
                                    </b-col>
                                </b-row>
                            </b-form>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result" v-if="params.id">
                        <div class="tabel-content">
                            <p v-cloak><small>Click on No Reg. to view detail.</small></p>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 3%;">#</th>
                                        <th scope="col" style="width: 10%;">No Reg. </th>
                                        <th scope="col" style="width: 5%;">No Start</th>
                                        <th scope="col" style="width: 17%;">Pembalap</th>
                                        <th scope="col" style="width: 15%;">Manager </th>
                                        <th scope="col" style="width: 15%;">Merek Tipe</th>
                                        <th scope="col" style="width: 15%;">Kelas / Kategori</th>
                                        <th scope="col" style="width: 10%;">Pembayaran</th>
                                        <th scope="col" style="width: 5%;">Sts.</th>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                    <tr>
                                        <td colspan="9">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='baseUrl' :is-load="isLoad"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>