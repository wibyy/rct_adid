<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-header">
                    <div class="row justify-content-center">
                        <div class="col-10">
                            <a :href="listUrl+listData.event.event_id" class="btn outline-white">Go Back</a>
                            <a :href="listUrl+listData.event.event_id" class="btn outline-white float-right"><i class="far fa-file-pdf"></i> PDF</a>
                        </div>
                    </div>
                </div>
                <div class="card-body mb-5">
                    <div v-if="isLoad">
                        <b-progress :max="progress.max">
                            <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                        </b-progress>
                    </div>

                    <detail-registration :list-data="listData" :is-load="isLoad"></detail-registration>
                </div>
            </div>
        </div>
    </div>
</div>