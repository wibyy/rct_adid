<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Registration extends CI_Controller {
	public $page = 'registration';
	public $MENU = [];
	public $USER = [];
	public $USER_INFO = [];
	public $USER_GROUP = [];
	public $USER_PERMISSION = [];
	public $ACCESS_PAGE = false;

	public function __construct() {
		parent::__construct();


			if ($this->session->userdata("sign_in") == TRUE) {
				$this->config->load('rct_config');
				$this->USER_INFO 		= $this->session->userdata();
						
				$this->USER  			= getUserGroup([$this->USER_INFO['user_group']]);
				$this->USER_GROUP 		= $this->config->item('user_group');
				$this->USER_PERMISSION  = $this->config->item('user_group')[$this->USER_INFO['user_group']]['access'];
				$this->ACCESS_PAGE		= (isset($this->USER_PERMISSION[$this->page]))?$this->USER_PERMISSION[$this->page]:false;

				$this->MENU = generateMenu(array_keys($this->USER_PERMISSION));
				$this->load->helper('status');
				$this->load->model('m_crud');
				$this->load->model('m_global');

				if(!$this->ACCESS_PAGE){
					get_redirecting('auth');
				}
				
			}else{
				get_redirecting('auth');
			}
	}

	public function index(){
		$data['page_title'] = 'Registration Record';
		$data['page'] = $this->page;
		$data['sub_page'] = 'registration';
		$data['script'] = '<script src="'.base_url("assets/admin/js/dashboard/registration.js").'"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page.'/v_index');
		$this->load->view('../../views/dash_partial/v_footer');
	}

	public function new(){
		$this->page = 'new_reg';
		$this->ACCESS_PAGE		= (isset($this->USER_PERMISSION[$this->page]))?$this->USER_PERMISSION[$this->page]:false;

		if (!$this->ACCESS_PAGE) {
			get_redirecting('auth');
		}

		$data['page_title'] = 'New Registration';
		$data['page'] = $this->page;
		$data['sub_page'] = 'new_reg';
		$data['script'] = '<script src="'.base_url("assets/admin/js/dashboard/registration-new.js").'"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view('registration/v_new');
		$this->load->view('../../views/dash_partial/v_footer');
	}

	public function detail($id){

		if (!in_array('detail', $this->ACCESS_PAGE)) {
			get_redirecting('auth');
		}

		$data['page_title'] = 'Detail Record';
		$data['page'] = $this->page;
		$data['sub_page'] = 'registration';
		$data['script'] = "<script> var DETAIL = ".$id."</script>";
		$data['script'] .= '<script src="'.base_url("assets/admin/js/dashboard/registration-detail.js").'"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page.'/v_detail');
		$this->load->view('../../views/dash_partial/v_footer');
	}

	public function export()
	{

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setCellValue('A1', 'No');
		$sheet->setCellValue('B1', 'Nama');
		
		$result = $this->m_global->get_all('wilayah_provinsi');
		$no = 1;
		$x = 2;
		foreach($result as $row)
		{
			$sheet->setCellValue('A'.$x, $no++);
			$sheet->setCellValue('B'.$x, $row->nama);
			$x++;
		}
		$writer = new Xlsx($spreadsheet);
		$filename = 'region';
		
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'. $filename .'.xlsx"'); 
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
	public function read_excel()
	{

		$inputFileType = 'Xlsx';
		$inputFileName = '././././assets/uploads/doc/test.xlsx';

		$reader = IOFactory::createReader($inputFileType);
		$reader->setReadDataOnly(true);

		try {
			$spreadsheet = IOFactory::load($inputFileName);
			$worksheetData = $reader->listWorksheetInfo($inputFileName);
			foreach ($worksheetData as $worksheet) {
				$sheetName = $worksheet['worksheetName'];
				$rows = (int) $worksheet['totalRows'];

				$reader->setLoadSheetsOnly($sheetName);
				$spreadsheet = $reader->load($inputFileName);

				$worksheet = $spreadsheet->getActiveSheet();
				$dataArray = $worksheet->toArray();

				var_dump($rows);
			}

		} catch(\PhpOffice\PhpSpreadsheet\Reader\Exception $e) {
			die('Error loading file: '.$e->getMessage());
		}

		exit;
	}
}