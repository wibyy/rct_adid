<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class Payment extends CI_Controller {
	public $page = 'payment';
	public $MENU = [];
	public $USER = [];
	public $USER_INFO = [];
	public $USER_GROUP = [];
	public $USER_PERMISSION = [];

	public function __construct() {
		parent::__construct();
		$this->config->load('rct_config');
		$this->USER_INFO 		= $this->session->userdata();
				
		$this->USER  			= getUserGroup([$this->USER_INFO['user_group']]);
		$this->USER_GROUP 		= $this->config->item('user_group');
		$this->USER_PERMISSION  = $this->config->item('user_group')[$this->USER_INFO['user_group']]['access'];
		$this->ACCESS_PAGE		= (isset($this->USER_PERMISSION[$this->page]))?$this->USER_PERMISSION[$this->page]:false;

			if ($this->session->userdata("sign_in") == TRUE && $this->ACCESS_PAGE) {
				
				$this->MENU = generateMenu(array_keys($this->USER_PERMISSION));
				$this->load->helper('status');
				$this->load->model('m_crud');
				$this->load->model('m_global');
				$this->load->library('pdf');
				
			}else{
				get_redirecting('auth');
			}
	}

	public function index(){
		$data['page_title'] = 'Pembayaran';
		$data['page'] = $this->page;
		$data['sub_page'] = 'pembayaran';
		$data['script'] = '<script src="'.base_url("assets/admin/js/dashboard/payment.js").'"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view($this->page.'/v_index', $data);
		$this->load->view('../../views/dash_partial/v_footer');
	}
//915
	public function pdf($id){

		$query_reg     	= $this->m_global->get_by_id('registrations', 'id', $id);
		if($query_reg){
			$result				= $query_reg[0];
			$query_user		    = "SELECT id_users, full_name, email, phone FROM users WHERE id_users = '".$result->id_users."'";
			$result_user     	= $this->db->query($query_user)->result();
			$result->user		= ($result_user[0])?$result_user[0]:'';
			
			$query_rcr		    = "SELECT id, full_name, email, phone, kis_imi, pengprov_imi FROM pembalap WHERE id = '".$result->id_pembalap."'";
			$result_rcr     	= $this->db->query($query_rcr)->result();
			$result->pembalap		= ($result_rcr[0])?$result_rcr[0]:'';

			$query_team		    = "SELECT id, name, mng_name, mng_license, mng_phone, entrant, entrant_name, entrant_license, entrant_license_expire  FROM tim WHERE id = '".$result->id_tim."'";
			$result_team     	= $this->db->query($query_team)->result();
			$result->tim		= ($result_team[0])?$result_team[0]:'';

			$query_event	= " SELECT
									kategori.id AS id,
									kategori.label AS kategori,
									kelas.label AS kelas,
									event.id AS event_id,
									event.label AS event,
									event.sub_label AS event_sub,
									event.logo AS event_logo,
									event.lokasi AS lokasi,
									event.date_start AS event_start,
									event.date_end AS event_end
								FROM
									kategori
								JOIN kelas ON kelas.id = kategori.id_kelas
								JOIN event ON event.id = kelas.id_event
								WHERE
									kategori.id = '".$result->id_kategori."'";	
			$result_event     	= $this->db->query($query_event)->result();
			$result->event	= $result_event[0];

			// if($result->event->event_logo=='')
			// 	$result->event->event_logo 	= 'assets/uploads/no_image.png';
			// else {
			// 	$result->event->event_logo_thumb 	= 'assets/uploads/images/thumb-144/'.$result->event->event_logo;
			// 	$result->event->event_logo 			= 'assets/uploads/images/'.$result->event->event_logo;
			// }
		}
		
		$data = [
			'data' => json_decode(json_encode($result), true)
		];

        $html           =  $this->load->view($this->page.'/pdf_invoice', $data, true);
        $filename       = 'TestPDF';
        $paper          = 'A4';
        $orientation    = 'potrait';
        $this->pdf->pdf_create($html, $filename, $paper, $orientation, $stream=TRUE);

		// $this->load->view($this->page.'/pdf_invoice', $data);
		
	}

}