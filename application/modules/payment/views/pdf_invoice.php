<?php
date_default_timezone_set("Asia/Jakarta");
?>

<!DOCTYPE html>
<html>

<head>
    <title>Cadas Table</title>
    <style type="text/css">
        @import url('https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;1,200;1,300;1,400;1,600;1,700;1,800&display=swap');

        p,
        h1,
        h2,
        h3,
        h4,
        h5,
        b,
        i {
            margin: 0;
            padding: 0;
            color: #000;
            font-family: 'Nunito', sans-serif;
        }

        p,
        b {
            font-size: 10pt;
            line-height:0.3;
            font-family: 'Nunito', sans-serif;
        }

        p.text-kecil {
            font-size: 8pt;
            line-height: 0.2;
        }
        p.text-kecil b{
            font-size: 8pt;
            line-height: 0.2;
        }

        .outtable {
            padding: 20px;
            border: 1px solid #e3e3e3;
            width: 100%;
            border-radius: 5px;
        }

        .short {
            width: 50px;
        }

        .normal {
            width: 150px;
        }

        table,
        tr,
        td {
            color: #000;
            font-family: 'Nunito', sans-serif;
        }

        table td {
            vertical-align: top;
        }

        .header,
        .footer {
            max-width: 100%;
        }

        .text-center {
            text-align: center;
        }

        .judul {
            margin-top: 20px;
            width: 100%;
        }

        .img-logo {
            width: 5cm;
        }

        .container {
            width: 100%;
            margin: 0;
            padding: 0.5cm;
            padding-top: 0.3cm;
            padding-bottom: 0.1cm;
        }

        .event-title {
            margin-top: 0.2cm;
            margin-bottom: 0.3cm;
            width: 100%;
        }

        .invoice-info {
            margin-top: 0.3cm;
            width: 100%;
            padding: 0.5cm 0.3cm;
            background-color: #eaeaea;
        }

        .tabel-invoice-to {
            width: 100%;
            margin-top: 0.3cm;
        }

        .invoice-info-conf {
            width: 100%;
            margin-top: 0.3cm;
        }

        .invoice-info-conf2 {
            width: 100%;
            margin-top: 0.3cm;
            margin-bottom: 0.1cm;
        }

        .tabel-invoice {
            margin-top: 0.3cm;
            width: 100%;
            border-collapse: collapse;
            border: 1px solid #ccc;
        }

        .tabel-invoice tr,
        .tabel-invoice tr th,
        .tabel-invoice tr td {
            border-collapse: collapse;
            border: 1px solid #ccc;
        }

        .tabel-invoice tr th {
            color: #fff;
        }

        .tabel-invoice tr.bg-red {
            background-color: #ff6961;
        }

        .tabel-invoice tr.bg-tosca {
            background-color: #34adbd;
        }

        .tabel-invoice tr.bg-aa {
            background-color: #efefef;
        }
        .akun-bank-item {
            margin-bottom: 0.1cm;
        }
        .invoice-info-footer {
            margin-top : 2.5cm;
            text-align: center;
        }
        .tabel-footer {
            margin-top: 1cm;
            width: 100%;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="logo-event">
            <img src="<?php echo base_url('assets/uploads/images/' . $data['event']['event_logo']); ?>" class="img-logo" alt="">
        </div>
        <div class="invoice-info" style="text-align: right;">
            <p style="font-size: 14pt;">Invoice No #123454</p>
            <p class="text-kecil">Tanggal 12 februari 2021</p>
        </div>

        <table class="tabel-invoice-to">
            <tr>
                <td>
                    <div class="event-title">
                        <p><b style="font-size: 14pt"><?php echo $data['event']['event']; ?></b></p>
                        <p><?php echo $data['event']['lokasi']; ?></p>
                        <p class="text-kecil"><?php echo convert_to_dmy($data['event']['event_start']) . ' s/d ' . convert_to_dmy($data['event']['event_start']); ?></p>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <p>Yth.</p>
                    <p><b style="font-size: 11;"><?php echo $data['user']['full_name']; ?></b></p>
                    <p><?php echo $data['user']['email'] . ' / ' . $data['user']['phone']; ?></p>
                    <p>di tempat</p>
                </td>
            </tr>
        </table>

        <table class="tabel-invoice">
            <tr class="bg-tosca">
                <th style="width: 6%;text-align: center;">
                    <p>#</p>
                </th>
                <th style="width: 69%;text-align: center;">
                    <p>Deskripsi</p>
                </th>
                <th style="width: 25%;text-align: center;">
                    <p>Total</p>
                </th>
            </tr>
            <tr>
                <td style="text-align: center;padding:0.3cm 0 0.5cm 0">
                    <p>1</p>
                </td>
                <td style="padding:0.3cm 0 0.5cm 0.3cm">
                    <p style="font-size:12pt">No. <b><?php echo $data['no_registration']; ?></b></p>
                    <p style="font-size:11pt"><?php echo '<b>' . $data['pembalap']['full_name'] . '</b> ( ' . $data['no_start'] . ' )'; ?> </p>
                    <p><?php echo $data['tim']['name']; ?></p>
                    <p><?php echo $data['event']['kelas'] . ' / ' . $data['event']['kategori']; ?></p>
                </td>
                <td style="text-align: center;padding:0.3cm 0 0.5cm 0">
                    <p><?php echo format_rupiah($data['biaya']); ?></p>
                </td>
            </tr>
            <tr class="bg-aa">
                <td colspan="2" style="text-align: center;">
                    <p style="font-size:12pt"><b>Total Bayar</b></p>
                </td>
                <td style="text-align: center;">
                    <p style="font-size:12pt"><b><?php echo format_rupiah($data['biaya']); ?></b></p>
                </td>
            </tr>
        </table>

        <div class="invoice-info-conf">
            <p style="line-height: 12pt;">Jatuh tempo pembayaran pada tanggal 26 APRIL 2020 Mohon segera dibayarkan Konfirmasi kepada DEVANALOGI STUDIO jika anda telah membayarkan seluruhnya.</p>
        </div>
        
        <table class="tabel-footer">
            <tr>
                <td colspan="2" style="text-align: center;">
                    <p style="font-size:10pt">Terima kasih atas partisipanya mengikuti <?php echo $data['event']['event']; ?></p>
                    <br>
                    <br>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="text-kecil">Akun Bank :</p>
                    <div class="akun-bank-item">
                        <p class="text-kecil"><b>Mandiri</b></p>
                        <p class="text-kecil">900-00-135653-88</p>
                        <p class="text-kecil">a/n Yannuar Wiby Sudarto</p>
                    </div>
                    <div class="akun-bank-item">
                        <p class="text-kecil"><b>BCA</b></p>
                        <p class="text-kecil">2890665830</p>
                        <p class="text-kecil">a/n Yannuar Wiby Sudarto</p>
                    </div>
                </td>
                <td style="text-align: right;">
                    <p>Jakarta, <?php echo date("d M Y"); ?></p>
                    <p>Finance <?php echo $data['event']['event']; ?></p>
                    <br>
                    <br>
                    <p><b>Adityo Dwi Nugroho</b></p>
                </td>
            </tr>
        </table>
        <div class="invoice-info-footer">
            <p class="text-kecil">PDF Genegator at <?php echo date("d M Y h:i:s"); ?></p>
        </div>
    </div>
</body>

</html>