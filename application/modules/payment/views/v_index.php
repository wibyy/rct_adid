<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-3" v-cloak>
                            <?php if (in_array('filter', $this->ACCESS_PAGE)) { ?>
                            <h4 class="mb-2">Filter : </h4>
                            <b-form @submit.prevent="filterProcess">
                                <b-row>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="No Registrasi" label-for="input-1">
                                            <b-form-input id="input-1" v-model="params.no_reg"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="2" cols="12">
                                            <b-form-group id="input-group-1" label="Status" label-for="input-status">
                                                <b-form-select id="input-status" v-model="params.status" :options="optionsStatus"></b-form-select>
                                            </b-form-group>
                                        </b-col>
                                    <b-col col lg="4" cols="12">
                                        <div class="mt-4">
                                            <b-button type="reset" @click="resetFormFilter" squared variant="outline-secondary" >Reset</b-button>
                                            <b-button type="submit" class="btn btn-form-info" :disabled="!params.no_reg && !params.status">Filter</b-button>
                                        </div>
                                    </b-col>
                                </b-row>
                            </b-form>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%;text-align:center">#</th>
                                        <th scope="col" style="width: 7%;">No. Registrasi</th>
                                        <th scope="col" style="width: 5%;">No. Start</th>
                                        <th scope="col" style="width: 15%;">Nama Pembalap</th>
                                        <th scope="col" style="width: 15%;">Event </th>
                                        <th scope="col" style="width: 12%;">Total</th>
                                        <th scope="col" style="width: 11%;">Sts. Pembayaran </th>
                                        <th scope="col" style="width: 10%;">Sts. Konf. Pembayaran </th>
                                        <?php if (in_array('export', $this->ACCESS_PAGE)) { ?>
                                        <th scope="col" style="width: 5%;text-align:center">Invoice</th>
                                        <?php } ?>
                                        <?php if (in_array('konfirmasi', $this->ACCESS_PAGE)) { ?>
                                        <th scope="col" style="width: 10%;text-align:center">Konfirmasi</th>
                                        <?php } ?>
                                        <?php if (in_array('approval', $this->ACCESS_PAGE)) { ?>
                                        <th scope="col" style="width: 10%;text-align:center">Opt.</th>
                                        <?php } ?>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                <?php 
                                $colspan = 8;
                                if (in_array('konfirmasi', $this->ACCESS_PAGE)){
                                    $colspan = $colspan + 1;
                                } 
                                if (in_array('approval', $this->ACCESS_PAGE)){
                                    $colspan = $colspan + 1;
                                } 
                                if (in_array('export', $this->ACCESS_PAGE)){
                                    $colspan = $colspan + 1;
                                } 
                                
                                ?>
                                    <tr>
                                        <td colspan="<?php echo $colspan; ?>">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='baseUrl' :is-load="isLoad" :is-perm="perm" col-span="<?php echo $colspan; ?>" @click-inactive="processUpdateStatus" @click-print="printInvoice"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
