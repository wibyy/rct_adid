<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public $page = 'dashboard';
	public $MENU = [];
	public $USER = [];
	public $USER_INFO = [];
	public $USER_GROUP = [];
	public $USER_PERMISSION = [];
	public $ACCESS_PAGE = false;

	public function __construct() {
		parent::__construct();

			if ($this->session->userdata("sign_in") == TRUE) {

				$this->config->load('rct_config');
				$this->USER_INFO 		= $this->session->userdata();
						
				$this->USER  			= getUserGroup([$this->USER_INFO['user_group']]);
				$this->USER_GROUP 		= $this->config->item('user_group');
				$this->USER_PERMISSION  = $this->config->item('user_group')[$this->USER_INFO['user_group']]['access'];
				$this->ACCESS_PAGE		= (isset($this->USER_PERMISSION[$this->page]))?$this->USER_PERMISSION[$this->page]:false;

				$this->MENU = generateMenu(array_keys($this->USER_PERMISSION));
				$this->load->helper('status');
				$this->load->model('m_crud');
				$this->load->model('m_global');
				
				if(!$this->ACCESS_PAGE){
					get_redirecting('auth');
				}
				
			}else{
				get_redirecting('auth');
			}
	}

	public function index(){
		$data['page_title'] = 'Dashboard';
		$data['page'] = $this->page;
		$data['script'] = '<script src="'.base_url("assets/admin/js/dashboard/dashboard.js").'"></script>';

		$this->load->view('../../views/dash_partial/v_header', $data);
		$this->load->view('dashboard/v_index');
		$this->load->view('../../views/dash_partial/v_footer');
	}

	
}