<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>


    <div>
        <list-pendaftaran :list-data="listData" v-if="userInfo.type=='REGULARUSER'"></list-pendaftaran>
    </div>

    <!-- Content Row -->
    <div class="row">

        <div class="col-lg-6 mb-4">

            <!-- Illustrations -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-light">Selamat datang kembali</h6>
                </div>
                <div class="card-body">
                    <div class="text-center">
                        <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 15rem;" src="<?php echo base_url('assets/images/undraw_toy_car_7umw.svg'); ?>" alt="">
                    </div>
                    <div style="text-align: center;">
                        <h3 v-cloak>{{g_userData.name}}</h3>
                        <p v-cloak>{{g_userData.email}}</p>
                        <b v-cloak>{{userInfo.label}}</b>
                    </div>

                </div>
            </div>


            <div class="card shadow mb-4" v-if="userInfo.type=='EOUSER'">
                <!-- Card Header - Dropdown -->
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-light">Grafik Pendaftaran</h6>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <!-- <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                            <div class="dropdown-header">Dropdown Header:</div>
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div> -->
                    </div>
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <div class="chart-area">
                        <canvas id="pendaftaranCart"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <!-- Content Column -->
        <div class="col-lg-6 mb-4">

            <!-- Project Card Example -->
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-light">Event yang Akan Datang</h6>
                </div>
                <div class="card-body">
                    <b-list-group>
                        <b-list-group-item button v-for="(data,index) in listData.event_list">
                            <p class="dash-eventlist-title">{{data.label}}</p>
                            <p class="dash-eventlist-loc">{{data.lokasi}}</p>
                            <p class="dash-eventlist-tgl">{{data.date_start | localShortDate}} s/d {{data.date_end | localShortDate}}</p>
                            <p class="dash-eventlist-code mt-2">{{data.event_code}}</p>
                            <div class="dash-logo-event">
                                <img :src="data.logo_thumb" alt="">
                            </div>
                        </b-list-group-item>
                    </b-list-group>
                </div>
            </div>
        </div>

    </div>

</div>