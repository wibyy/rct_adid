<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 title-page"><?php echo $page_title; ?></h1>
    <p class="mb-4 sub-title-page">Lorem ipsum dolor sit amet consectetur adipisicing elit. </p>

    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-12 mb-3" v-cloak>
                            <div class="mt-2 mb-3">
                                <button type="button" @click="addNewUser" class="btn btn-form-info"><i class="fas fa-fw fa-plus"></i> User</button>
                                <a href="" class="btn btn-form-info"><i class="far fa-file-pdf"></i> PDF</a>
                            </div>
                            <h4 class="mb-2">Filter : </h4>
                            <b-form @submit.prevent="filterProcess">
                                <b-row>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="Name" label-for="input-1">
                                            <b-form-input id="input-1" v-model="params.nama"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="3" cols="12">
                                        <b-form-group id="input-group-1" label="Email" label-for="input-2">
                                            <b-form-input id="input-2" v-model="params.email"></b-form-input>
                                        </b-form-group>
                                    </b-col>
                                    <b-col col lg="4" cols="12">
                                        <div class="mt-4">
                                            <b-button type="reset" @click="resetFormFilter" squared variant="outline-secondary" :disabled="!params.nama && !params.email">Reset</b-button>
                                            <b-button type="submit" class="btn btn-form-info" :disabled="!params.nama && !params.email">Filter</b-button>
                                        </div>
                                    </b-col>
                                </b-row>
                            </b-form>
                        </div>
                    </div>
                    <div class="ticket-list tabel-result">
                        <div class="tabel-content">
                            <p v-cloak><small>Click on email to view detail.</small></p>
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 5%;">#</th>
                                        <th scope="col" style="width: 10%;">Email </th>
                                        <th scope="col" style="width: 10%;">Full Name</th>
                                        <th scope="col" style="width: 10%;">User Group</th>
                                        <th scope="col" style="width: 10%;">Phone </th>
                                        <th scope="col" style="width: 10%;">Event Active </th>
                                        <th scope="col" style="width: 6%;">Status</th>
                                        <th scope="col" style="width: 10%;">Last Login</th>
                                        <th scope="col" style="width: 10%;">Opt.</th>
                                    </tr>
                                </thead>
                                <tbody v-if="isLoad">
                                    <tr>
                                        <td colspan="10">
                                            <b-progress :max="progress.max">
                                                <b-progress-bar varian="info" striped animated :value="progress.value"></b-progress-bar>
                                            </b-progress>
                                        </td>
                                    </tr>
                                </tbody>
                                <tbody is="table-list" :list-data="listData" :pagination-data='paginationData' :base-url='baseUrl' :is-load="isLoad" @click-inactive="processUpdateStatus"></tbody>
                            </table>
                            <pagination :pagination-data="paginationData" @page-select="selectPage" v-if="!isLoad"></pagination>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal NEW-->
<div class="modal fade" id="newUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form method="POST" v-on:submit.prevent="processNewUser()">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="name" v-model="form.email" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="full_name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="full_name" v-model="form.name">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <div class="form-group">
                                <label for="user_group">User Group</label>
                                <select class="form-control" id="user_group" v-model="form.user_group" required>
                                    <option value="" selected disabled>-- No Selected --</option>
                                    <?php foreach ($this->USER_GROUP as $key => $val) {
                                        if ($key != '0000') {
                                            ?>
                                            <option value="<?php echo $key; ?>"><?php echo $val['label']; ?></option>
                                    <?php }
                                    } ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-7">
                            <div class="form-group">
                                <label for="phone">Hp.</label>
                                <input type="text" class="form-control" id="phone" v-model="form.phone">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" id="password" v-model="form.password">
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="passconf">Konfirmasi Password</label>
                                <input type="password" class="form-control" id="passconf" v-model="form.passconf">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <b-alert variant="danger" dismissible :show="formNewMessage!=''" v-html="formNewMessage"></b-alert>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" v-cloak>{{buttonSubmit}}</button>
                </div>
            </form>
        </div>
    </div>
</div>