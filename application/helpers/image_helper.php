<?php
if (!function_exists('image')) {
    function image($image_path, $preset) {
        $CI = &get_instance();
        $CI->load->config("images");
        
        $sizes = $CI->config->item("image_sizes");

        $pathinfo = pathinfo($image_path);
        $new_path = $image_path;
       
        if (isset($sizes[$preset])) {
            $new_path = $pathinfo["dirname"] .  "/thumb"."/" . $pathinfo["filename"].".". $pathinfo["extension"];
        }
        
        return $new_path;
    }
}

if (!function_exists('thumbnail')) {
    function thumbnail($data, $preset) {
        $CI = &get_instance();
        $CI->load->config("images");
        $sizes = $CI->config->item("image_sizes");

        $pathinfo = pathinfo($data);
        $new_path = $data;
       
        $new_path = $pathinfo["dirname"] .  "/thumb"."/" . $pathinfo["filename"].".". $pathinfo["extension"];
       
        $config["source_image"] = $data;
        $config['new_image']    = $new_path;
        $config["width"]        = $sizes[$preset][0];
        $config["height"]       = $sizes[$preset][1];
        
        $CI->load->library('image_lib');
        $CI->image_lib->initialize($config);
        if(!$CI->image_lib->fit()){
            $CI->image_lib->clear();
            return "Fail";
        }
        else{
            $CI->image_lib->clear();
            return $new_path;
        }
    }
}

function upload_add_file($required, $url, $folder, $model, $id, $thumb, $image="userfile") {
	// config upload
	$CI = &get_instance();
	$CI->load->helper('message');
	
	$file 						= "";	
	$upload_path				= folder_upload ."/". $folder;
	$config['upload_path']   	= $upload_path;
	$config['allowed_types'] 	= img_upload; 
	$config['max_size']     	= max_upload;
	$config['file_name'] 		= time(); 
	
	$CI->load->library('upload', $config);

	if ( ! $CI->upload->do_upload($image)) {
		$error = array('error' => $CI->upload->display_errors());
		$CI->session->set_flashdata('message', $error['error']);
		$CI->session->set_flashdata('status', get_notify_status(0));
		
		if($required == 1) {
			redirect($url, 'refresh');
		}
	}
	else {
		$data = array('upload_data' => $CI->upload->data());
		$file = $data['upload_data']['file_name'];
		
		if($thumb != "") {
			$params =  $upload_path .'/'. $file;
			$res 	= thumbnail($params, $thumb);
		} 
	}
	
	return $file;
} 

function upload_edit_file($required, $url, $folder, $model, $id, $thumb, $image="userfile") {
	// config upload
	$CI = &get_instance();
	$CI->load->helper('message');
	
	$file 						= "";	
	$upload_path				= folder_upload ."/". $folder;
	$config['upload_path']   	= $upload_path;
	$config['allowed_types'] 	= img_upload; 
	$config['max_size']     	= max_upload;
	$config['file_name'] 		= time(); 
	
	$CI->load->library('upload', $config);
	$CI->load->model($model);
	$m = preg_replace('/(.*)\/m_/', 'm_', $model);
	$img = $CI->{$m}->get_file_by_id($id);
	if ( ! $CI->upload->do_upload($image)) {
		$file = $img;	
		$error = array('error' => $CI->upload->display_errors());
		$CI->session->set_flashdata('message', $error['error']);
		$CI->session->set_flashdata('status', get_notify_status(0));
		
		if($required == 1) {
			redirect($url, 'refresh');
		}
	}
	else {
		$data = array('upload_data' => $CI->upload->data());
		$file = $data['upload_data']['file_name'];
		
		if($thumb != "") {
			$params =  $upload_path .'/'. $file;
			$res 	= thumbnail($params, $thumb);
		}
		
		// delete image
		if($img != "") {
			$filestring = realpath(APPPATH .'.'. $upload_path .'/'. $img);
			@unlink ($filestring);
			
			if($thumb != "") {
				$filethumb = realpath(APPPATH .'.'. $upload_path .'/thumb'.'/'. $img);
				@unlink ($filethumb);
			}	
		}
	}
	
	return $file;
} 

function upload_editing_file($required, $url, $folder, $table, $sums, $field, $id, $thumb, $image="userfile") {
	// config upload
	$CI = &get_instance();
	$CI->load->helper('message');
	
	$file 						= "";	
	$upload_path				= folder_upload ."/". $folder;
	$config['upload_path']   	= $upload_path;
	$config['allowed_types'] 	= img_upload; 
	$config['max_size']     	= max_upload;
	$config['file_name'] 		= time(); 
	
	$CI->load->library('upload', $config);

	$img = $CI->load->model('m_global')->get_field_by_id($table, $sums, $field, $id);
	if ( ! $CI->upload->do_upload($image)) {
		$file = $img;	
		$error = array('error' => $CI->upload->display_errors());
		$CI->session->set_flashdata('message', $error['error']);
		$CI->session->set_flashdata('status', get_notify_status(0));
		
		if($required == 1) {
			redirect($url, 'refresh');
		}
	}
	else {
		$data = array('upload_data' => $CI->upload->data());
		$file = $data['upload_data']['file_name'];
		
		if($thumb != "") {
			$params =  $upload_path .'/'. $file;
			$res 	= thumbnail($params, $thumb);
		}
		
		// delete image
		if($img != "") {
			$filestring = realpath(APPPATH .'.'. $upload_path .'/'. $img);
			@unlink ($filestring);
			
			if($thumb != "") {
				$filethumb = realpath(APPPATH .'.'. $upload_path .'/thumb'.'/'. $img);
				@unlink ($filethumb);
			}	
		}
	}
	
	return $file;
} 