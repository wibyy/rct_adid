<?php
	function get_status($status) {
		switch($status) {
			case 1 : 
				$status = "Active";
			break;
			case 0 : 
				$status = "Not Active";
			break;
		}
		
		return $status;
	}
	
	function get_yes_no($status) {
		switch($status) {
			case 2 : 
				$status = "Pending";
			break;
			case 1 : 
				$status = "Yes";
			break;
			case 0 : 
				$status = "No";
			break;
		}
		
		return $status;
	}
	
	function get_true_false($status) {
		switch($status) {
			case 1 : 
				$status = "True";
			break;
			case 0 : 
				$status = "False";
			break;
		}
		
		return $status;
	}
	
	function get_authority($status) {
		switch($status) {
			case 1 : 
				$status = "Travel Admin";
			break;
			case 0 : 
				$status = "Super Admin";
			break;
		}
		
		return $status;
	}
	
	function get_contact($status) {
		switch($status) {
			case 1 : 
				$status = "Read";
			break;
			case 0 : 
				$status = "Un Read";
			break;
		}
		
		return $status;
	}
	function get_gender($status) {
		switch($status) {
			case 1 : 
				$status = "Male";
			break;
			case 0 : 
				$status = "Female";
			break;
		}
		
		return $status;
	}
	
	function get_transaction($status) {
		switch($status) {
			case 8 : 
				$status = "Dibatalkan Pembeli";
			break;
			case 7 : 
				$status = "Dibatalkan Admin";
			break;
			case 6 : 
				$status = "Selesai";
			break;
			case 5 : 
				$status = "Dalam Pengiriman";
			break;
			case 4 : 
				$status = "Sedang Diproses";
			break;
			case 3 : 
				$status = "Pembayaran Ditolak";
			break;
			case 2 : 
				$status = "Pembayaran Disetujui";
			break;
			case 1 : 
				$status = "Menunggu Konfirmasi Pembayaran";
			break;
			case 0 : 
				$status = "Menunggu Pembayaran";
			break;
		}
		
		return $status;
	}
?>