<?php
	function dd($q){
		echo '<pre>';
			print_r($q);
		echo '<pre>';
		die();
	}
	function generateMenu(array $array){
		$CI =& get_instance();
		$CI->config->load('rct_config');
		$MENU = $CI->config->item('menu');
		
		foreach($array as $key => $val){
			$out[$MENU[$val]['group']][$val] = array(
				'label'		=> $MENU[$val]['label'],
				'link'		=> $MENU[$val]['link'],
				'icon'		=> $MENU[$val]['icon']
			);
		}

		return $out;
	}
	function getUserGroup(array $array){
		$CI =& get_instance();
		$CI->config->load('rct_config');
		$USER_GROUP = $CI->config->item('user_group');

		foreach($array as $key => $val){
			$out[$val] = array(
				'label'		=> $USER_GROUP[$val]['label'],
				'type'		=> $USER_GROUP[$val]['type']
			);
		}
		return $out;
	}
	function get_redirecting($url) {
		//echo "redirecting...";
		redirect(base_url() . $url, 'refresh');
	}
	function hashPassword($password){
		return  md5('rct'.$password.'AdidProject');
	}
	function list_response($query_result, $total_data, $sts ='success', $msg=''){
		$response = array();
		
		$response['status'] 	= $sts;
		$response['message'] 	= $msg;
		$response['data']['list']		= $query_result;
		$response['data']['total']		= $total_data;

		return $response;

	}
	function list_response_map($query_result, $total_data, $page, $total_page, $limit, $sts ='success', $msg=''){
		$response = array();

		$total_page = (($total_data % $limit)!=0)?1:0;
		$total_page = (int) ($total_data / $limit + $total_page);

		$response['status'] 	= $sts;
		$response['message'] 	= $msg;
		$response['data']['list']		= $query_result;
		$response['data']['total']		= $total_data;
		$response['data']['pagination']['total_page']	= $total_page;
		$response['data']['pagination']['current']	= $page;
		$response['data']['pagination']['total']	= $total_data;
		$response['data']['pagination']['per_page']	= $limit;
		$response['data']['pagination']['prev']		= ($page==1)?false:$page-1;
		$response['data']['pagination']['next']		= ($page < $total_page)?$page+1:false;
		$response['data']['pagination']['list']	= pagination($total_page, $page);

		return $response;

	}

	function response_map($query_result, $sts ='success', $msg=''){
		$response = array();
		
		$response['status'] 	= $sts;
		$response['message'] 	= $msg;
		$response['data']		= $query_result;

		return $response;

	}
	function pagination($total, $current_page)
    {

        if (empty($current_page)) {
            $current_page = 1;
        }

        if ($total <= 5) {
            $awal = 1;
            $akhir = $total;
            $awal_active = 'false';
            $akhir_active = 'false';
        } else if (($total > 5) && ($current_page <= 4)) {
            $awal = 1;
            $akhir = $current_page + 2;
            $awal_active = 'false';
            $akhir_active = 'true';
        } else if (($current_page > ($total - 5)) && ($total > 5)) {
            $awal = $total - 5;
            $akhir = $total;
            $awal_active = 'true';
            $akhir_active = 'false';
        } else if (($total > 5) && ($current_page >= 2)) {
            $awal = $current_page - 2;
            $akhir = $current_page + 2;
            $awal_active = 'true';
            $akhir_active = 'true';
        }
        $paging = array();
        if ($awal_active === 'true') {
            array_push($paging, array(
                    "page" => '1',
                    "is_active" => ($current_page == '1') ? true : false
                )
            );
            array_push($paging, array(
                    "page" => '...',
                    "is_active" => false
                )
            );
        }
        for ($i = $awal; $i <= $akhir; $i++) {
            array_push($paging, array(
                    "page" => $i,
                    "is_active" => ($current_page == $i) ? true : false
                )
            );
        }
        if ($akhir_active === 'true') {
            array_push($paging, array(
                    "page" => '...',
                    "is_active" => false
                )
            );
            array_push($paging, array(
                    "page" => $total,
                    "is_active" => ($current_page == $total) ? true : false
                )
            );
        }
        return $paging;
    }	
	function showTrxResponse($status, $message, $data=array()){
		$res['status']=$status;
		$res['message']=$message;
		$res['data']=$data;
		header('Content-Type: application/json');
		echo json_encode($res);
		exit;

	}
	function isJson($data) {
		if(!!is_array($data) || !!is_object($data)) return false;
		json_decode($data);
		return (json_last_error() == JSON_ERROR_NONE);
	}
	
	function saveJson($data){
		if(!!is_array($data) || is_object($data))
			return addslashes(json_encode($data));
		return addslashes($data);
	}
	
	function sendCurl($Url, $OPT, $cot="30", $to="30", $headers=array(), $verbose = 0, $array=0){
		if (!function_exists('curl_init')){
			die('Sorry cURL is not installed!');
		}

		if(!$array){
			$OPT = (!!is_array($OPT)) ? http_build_query($OPT) : $OPT;
		}
		$ch  = curl_init();
		curl_setopt($ch, CURLOPT_URL, 				$Url);
		curl_setopt($ch, CURLOPT_VERBOSE, 			$verbose);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 		$OPT);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 	0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 	0);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 	1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 	1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER, 	1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT , 	$cot);
		curl_setopt($ch, CURLOPT_TIMEOUT, 			$to);
		curl_setopt($ch, CURLOPT_HEADER,			1);
		
		if(!!$headers && !!is_array($headers) && COUNT($headers) > 0){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		
		$response = curl_exec($ch);

		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$body = substr($response, $header_size);
		if(curl_error($ch)){
			$error = array(
				"CURL.Errno" 	=> curl_errno($ch),
				"CURL.Error" 	=> curl_error($ch)
			);
			if(curl_errno($ch) == 28)
				return false;
			return $error;
		}
		curl_close($ch);
		return $body;
	}

	function order_increment_code() {
		$CI =& get_instance();
		$date = get_invdmy();
	
		$q="SELECT max(SUBSTR(order_code, 11, 5)) as max from tp_order where order_code like 'INV-".$date."%'";
	
		$query = $CI->db->query($q);
		$r=$query->result();
		$max = 0;
		foreach ($r as $key) {
			$max = $key->max;
		}
		if($max==""){$max=0;} $max=$max+1;
		$result =  str_pad($max, 5, "0", STR_PAD_LEFT);		
		return $date.$result;
	}
	function product_increment_code() {
		$CI =& get_instance();
		$date = get_invdmy();
	
		$q="SELECT max(SUBSTR(order_code, 11, 5)) as max from tm_product where product_code like 'ITM-".$date."%'";
	
		$query = $CI->db->query($q);
		$r=$query->result();
		$max = 0;
		foreach ($r as $key) {
			$max = $key->max;
		}
		if($max==""){$max=0;} $max=$max+1;
		$result =  str_pad($max, 5, "0", STR_PAD_LEFT);		
		return $date.$result;
	}
	
	
	function min_disc($price, $disc){
		return ceil($price-($price*($disc/100)));
	}
	



	function ellipsis($length, $subject=""){
		$string="";
		if(strlen($subject) > $length) {
			$string=strip_tags(substr($subject, 0, $length) ." ...");
		}
		else{
			$string=strip_tags($subject);
		}
		return $string;
	}
	
	function slug_helper($title){
		$CI =& get_instance();
		$CI->load->helper(array("url", "text"));
		if(strlen($title) > 80) {
			$title=strip_tags(substr($title, 0, 80));
		}
		else{
			$title=strip_tags($title);
		}
		return convert_accented_characters(url_title($title, "dash", TRUE));
	}
	
	function url_title_helper($title){
		$CI =& get_instance();
		$CI->load->helper(array("url", "text"));
		if(strlen($title) > 80) {
			$title=strip_tags(substr($title, 0, 80));
		}
		else{
			$title=strip_tags($title);
		}
		return convert_accented_characters(url_title($title, "dash", TRUE));
	}


	function get_id_url($title){
		 $title = explode("-", $title);
		 return $title[0];
		
	}

	function get_random_password($l){
		$characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
		$string = '';
		 $max = strlen($characters) - 1;
		 for ($i = 0; $i < $l; $i++) {
		      $string .= $characters[mt_rand(0, $max)];
		 }
		return $string;
	}

	function fix_weight($berat){
		if($berat>1){
		  if(fmod($berat, floor($berat))>0.3){
		    return ceil($berat);
		  }
		  else{
		    return floor($berat);
		  }
		}else{ return 1; }
	}

	

	function handling_characters($str) {
		$str = str_replace("@", "", $str);
		$str = str_replace("#", "", $str);
		$str = str_replace("$", "", $str);
		$str = str_replace("%", "", $str);
		$str = str_replace("^", "", $str);
		$str = str_replace("&", "", $str);
		$str = str_replace("*", "", $str);
		$str = str_replace("'", "", $str);
		$str = str_replace('"', '', $str);
		$str = str_replace('!', '', $str);
		$str = str_replace('?', '', $str);
		
		return $str;
	}
	
	
	function labeling($str) {
		$str = str_replace(" ", "-", strip_tags($str));
		$str = handling_characters($str);
		$str = str_replace('"', '', $str);
		
		$str = substr($str, 0, 100);
		return $str;
	}
	
	function convert_to_nomor($id) {
		$digit = "";
		$nomor = "";
		
		$id = intval($id);
		if($id < 10) {
			$digit = "000000";
		}
		else if($id >= 10 && $id < 99) {
			$digit = "00000";
		}
		else if($id >= 100 && $id < 999) {
			$digit = "0000";
		}	
		else if($id >= 1000 && $id < 9999) {
			$digit = "000";
		}
		else if($id >= 10000 && $id < 99999) {
			$digit = "00";
		}
		else if($id >= 100000 && $id < 999999) {
			$digit = "0";
		}
		else {
			$digit = "";
		}
		
		$nomor = $digit .''. $id;
		
		return $nomor;
	}
	
	function convert_to_star($str) {
		$encryp = "";
		$len 	= strlen($str);
		
		for($i=0; $i<$len; $i++) {
			$encryp = $encryp."*";
		}
		
		return $encryp;
	}
	
	function format_rupiah($value) {
		return 'Rp. '. number_format($value, 0, ',', '.');
	}
	
	function pembulatan_rupiah($value) {
		$value = intval($value);
		if(strlen($value) >= 3) {
			$depan = substr($value, 0, (strlen($value) - 3));
			$ratusan = substr($value, (strlen($value) - 3), 3);
			if($ratusan >= 1 && $ratusan < 250) {
				$ratusan = '000';
			}
			else if($ratusan >= 250 && $ratusan < 500) {
				$ratusan = '250';
			}
			else if($ratusan >= 500 && $ratusan < 750) {
				$ratusan = '500';
			}
			else if($ratusan >= 750 && $ratusan < 1000) {
				if(strlen($value) == 3) {
					$depan = substr($value, 0, (strlen($value) - 2));
					$ratusan = substr($value, (strlen($value) - 2), 2);
					
					if($ratusan >= 1 && $ratusan < 50) {
						$ratusan = '00';
					}
					else {
						$ratusan = '50';
					}
				}
				else {
					$depan = substr($value, 0, (strlen($value) - 2));
					$ratusan = substr($value, (strlen($value) - 2), 2);
					
					if($ratusan >= 51 && $ratusan < 100) {
						$ratusan = '50';
					}
					else {
						$ratusan = '00';
					}
					
				}
			}
			
			$value = $depan.''.$ratusan;
		}
		
		return number_format($value, 2);
	}

	function increment_code($count) {
		$str = "000";
		$count = $count + 1;
		$len = strlen($count);
		
		switch($len) {
			case 1 : 
				$result = "00". $count;
			break;
			case 2 : 
				$result = "0". $count;
			break;
			case 3 : 
				$result = $count;
			break;
		}
		
		return $result;
	}
	

	
	function last_query() {
		$CI =& get_instance();
		echo $CI->db->last_query(); die();
	}
	
	function json($arr) {
		echo json_encode($arr); die();
	}
	



?>