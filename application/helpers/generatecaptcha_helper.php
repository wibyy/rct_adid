<?php
    function generate_captcha($sessionname){
        $CI = &get_instance();
        $CI->load->helper('captcha');

         $vals = array(
            'word_length' => 6,
                'img_path'   => './assets/captcha/',
                'img_url'    => base_url().'assets/captcha/',
                'img_width'  => '200',
                'img_height' => 50,
                'border' => 0,
                'font_path'     => APPPATH .'.'. "./assets/".'/fonts/SpecialElite.ttf',
                //'font_size'     => 20,
                'expiration' => 1800
            );
        $cap = create_captcha($vals);
        $session_array=array(
             'time_'.$sessionname => $cap['time'],
             'ip_'.$sessionname => $CI->input->ip_address(),
             'image_'.$sessionname => $cap['image'],
             $sessionname => $cap['word']

        );
        $CI->session->set_userdata($session_array); 
        return $cap['image'];
    }



    function check_captcha($sessionname, $cap){
        $CI = &get_instance();
        $expiration = time()-7200;
        if($CI->session->userdata($sessionname)== $cap && $CI->session->userdata('ip_'.$sessionname)== $CI->input->ip_address() && $CI->session->userdata('time_'.$sessionname)> $expiration){
            $vals=1;
        }
        else{
            $vals=0;    
        }
        return $vals;
    }

    function remove_captcha($sessionname){
        $CI = &get_instance();
        preg_match("/captcha\/(.*)jpg/", $CI->session->userdata('image_'.$sessionname), $output_array);
        if(count($output_array)>0){
            $img=$output_array[0];
            $filestring = realpath(APPPATH .'.'. "./assets/". $img);
            @unlink ($filestring);
        }
        $CI->session->unset_userdata('time_'.$sessionname); 
        $CI->session->unset_userdata('ip_'.$sessionname); 
        $CI->session->unset_userdata('image_'.$sessionname); 
        $CI->session->unset_userdata($sessionname); 
    }
