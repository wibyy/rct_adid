<?php

class M_global extends CI_Model {

	function get_all($table) {
		$query = $this->db->get($table);
        return $query->result();
	}
	
	function get_list($table, $field) {
		$query = $this->db->get_where($table, array($field => 1));
        return $query->result();
	}
	
	function get_order($table, $by, $order) {
		$query = $this->db->order_by($by, $order)->get($table);
        return $query->result();
	}
	
	function get_by_id($table, $field, $id) {
		$query = $this->db->get_where($table, array($field => $id));
        return $query->result();
	}
	
	function get_by_id_group($table, $field, $id, $fields, $ids, $group) {
		$query = $this->db->group_by($group)->get_where($table, array($field => $id, $fields => $ids));
        return $query->result();
	}
	
	function get_by_id_and_group($table, $field, $id, $group) {
		$query = $this->db->group_by($group)->get_where($table, array($field => $id));
        return $query->result();
	}
	
	function get_by_id_order($table, $field, $id, $fields, $ids, $by, $order) {
		$query = $this->db->order_by($by, $order)->get_where($table, array($field => $id, $fields => $ids));
        return $query->result();
	}
	
	function get_by_id_and_order($table, $field, $id, $by, $order) {
		$query = $this->db->order_by($by, $order)->get_where($table, array($field => $id));
        return $query->result();
	}
	
	function get_by_triple_id_order($table, $field, $id, $fields, $ids, $fieldss, $idss, $by, $order) {
		$query = $this->db->order_by($by, $order)->get_where($table, array($field => $id, $fields => $ids, $fieldss => $idss));
        return $query->result();
	}
	
	function get_by_four_id_order($table, $field, $id, $fields, $ids, $fieldss, $idss, $fieldsss, $idsss, $by, $order) {
		$query = $this->db->order_by($by, $order)->get_where($table, array($field => $id, $fields => $ids, $fieldss => $idss, $fieldsss => $idsss));
        return $query->result();
	}
	
	function get_by_id_limit_order($table, $field, $id, $limit, $by, $order) {
		$query = $this->db->limit($limit)->order_by($by, $order)->get_where($table, array($field => $id));
        return $query->result();
	}
	function get_by_two_id($table, $field, $id, $fields, $ids) {
		$query = $this->db->get_where($table, array($field => $id, $fields => $ids));
        return $query->result();
	}
	
	
	function get_by_two_id_limit_order($table, $field, $id, $fields, $ids, $limit, $by, $order) {
		$query = $this->db->limit($limit)->order_by($by, $order)->get_where($table, array($field => $id, $fields => $ids));
        return $query->result();
	}
	
	function get_by_triple_id_limit_order($table, $field, $id, $fields, $ids, $fieldss, $idss, $limit, $by, $order) {
		$query = $this->db->limit($limit)->order_by($by, $order)->get_where($table, array($field => $id, $fields => $ids, $fieldss => $idss));
        return $query->result();
	}
	
	function get_by_four_id_limit_order($table, $field, $id, $fields, $ids, $fieldss, $idss, $fieldsss, $idsss, $limit, $by, $order) {
		$query = $this->db->limit($limit)->order_by($by, $order)->get_where($table, array($field => $id, $fields => $ids, $fieldss => $idss, $fieldss => $idss));
        return $query->result();
	}
	
	function get_by_limit($table, $field, $id, $limit) {
		$query = $this->db->limit($limit)->get_where($table, array($field => $id));
        return $query->result();
	}
	
	function get_by_limit_order($table, $limit, $by, $order) {
		$query = $this->db->limit($limit)->order_by($by, $order)->get($table);
        return $query->result();
	}
	
	function get_by_status($table, $field, $status) {
		$query = $this->db->get_where($table, array($field => $status));
        return $query->result();
	}
	
	function get_by_status_arr($table, $field, $status) {
		$query = $this->db->where_in($field, $status)->get($table);
        return $query->result();
	}
	
	function get_by_id_arr_order($table, $field, $id, $in, $arr, $by, $order) {
		$query = $this->db->where_in($in, $arr)->order_by($by, $order)->get_where($table, array($field => $id));
        return $query->result();
	}
	
	function get_by_status_arr_order($table, $field, $status, $by, $order) {
		$query = $this->db->where_in($field, $status)->order_by($by, $order)->get($table);
        return $query->result();
	}
	
	function get_by_id_not_in($table, $field, $id, $not, $not_id) {
		$query = $this->db->where_not_in($not, $not_id)->get_where($table, array($field => $id));
        return $query->result();
	}
	
	function get_by_two_id_not_in($table, $field, $id, $fields, $ids, $not, $not_id) {
		$query = $this->db->where_not_in($not, $not_id)->get_where($table, array($field => $id, $fields => $ids));
        return $query->result();
	}
	
	function get_select($table, $field, $id) {
		$query = $this->db->where_not_in($field, $id)->get($table);
        return $query->result();
	}
	
	function get_select_rand_by_limit($table, $field, $id, $limit) {
		$query = $this->db->order_by($field, "random")->where_not_in($field, $id)->limit($limit)->get($table);
        return $query->result();
	}
	
	function get_select_order_by_limit($table, $field, $id, $limit, $by, $order) {
		$query = $this->db->order_by($by, $order)->where_not_in($field, $id)->limit($limit)->get($table);
        return $query->result();
	}
	
	function get_select_where_order_by_limit($table, $field, $id, $fields, $ids, $limit, $by, $order) {
		$query = $this->db->order_by($by, $order)->where($fields, $ids)->where_not_in($field, $id)->limit($limit)->get($table);
        return $query->result();
	}
	
	function get_select_limit($table, $field, $id, $limit) {
		$query = $this->db->where_not_in($field, $id)->limit($limit)->get($table);
        return $query->result();
	}
	
	function get_select_in_id($table, $field, $id, $in, $in_id) {
		$query = $this->db->where_not_in($field, $id)->where($in, $in_id)->get($table);
        return $query->result();
	}
	
	function get_rand_by_limit($table, $field, $limit) {
		$query = $this->db->order_by($field, "random")->limit($limit)->get_where($table);
        return $query->result();
	}
	
	function set_status($table, $field, $id, $stat, $status) {
		$this->db->set($stat, $status);
		$this->db->where($field, $id);
		$this->db->update($table);
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return 0;
		}
		else {
			$this->db->trans_commit();
			return 1;
		}
	}
	
	function check_exist($table, $field, $value) {
		$query = $this->db->get_where($table, array($field => $value), 1, 0);
		
		if ($query->num_rows() > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}	
	}

	function check_exist_not($table, $field, $value, $field1, $value1) {
		$query = $this->db->get_where($table, array($field => $value,  $field1." <> "=> $value1), 1, 0);
		
		if ($query->num_rows() > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}	
	}
	
	function check_existing($table, $field1, $value1, $field2, $value2) {
		$query = $this->db->get_where($table, array($field1 => $value1, $field2 => $value2), 1, 0);
		
		if ($query->num_rows() > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}	
	}
	
	function check_existings($table, $field1, $value1, $field2, $value2, $field3, $value3) {
		$query = $this->db->get_where($table, array($field1 => $value1, $field2 => $value2, $field3 => $value3), 1, 0);
		
		if ($query->num_rows() > 0) {
			return TRUE;
		}
		else {
			return FALSE;
		}	
	}
	
	function get_page_by_num_offset($table, $field, $num, $offset){
        $this->db->select('*');
		$this->db->from($table);
		$this->db->order_by($field, 'DESC');
		$this->db->limit($num, $offset);
		$query = $this->db->get();

        return $query->result();
    }
	
	function get_search($tb, $fd, $key) {
        $this->db->select('*');
		$this->db->from($tb);
		$this->db->like($fd, $key, 'both'); 
		$query = $this->db->get();    
		return $query->result();
    }
	
	function get_search_order($tb, $fd, $key, $by, $order) {
        $this->db->select('*');
		$this->db->from($tb);
		$this->db->like($fd, $key, 'both'); 
		$this->db->order_by($by, $order);
		
		$query = $this->db->get();    
		return $query->result();
    }
	
	function count_by_preq($tb, $fd, $like, $preq) {
        $this->db->select($fd);
		$this->db->from($tb);
		$this->db->like($like, $preq, 'after'); 
		$query = $this->db->get();    

		return $query->num_rows();	
    }
	
	function get_field_by_id($table, $sums, $field, $id) {
        $sum="";
        $q = "select ".$sums." as sums FROM ".$table."  WHERE ".$field."='".$id."'";
		$query =  $this->db->query($q);
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
    }
	
	function get_count_by_id($tb, $fd, $id) {
        $sum = 0;
		$this->db->select('count(*) as sums');
		$this->db->from($tb);
		$this->db->where($fd, $id);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
    }
	
	function get_count_by_two_id($tb, $fd, $id, $fds, $ids) {
        $sum = 0;
		$this->db->select('count(*) as sums');
		$this->db->from($tb);
		$this->db->where($fd, $id);
		$this->db->where($fds, $ids);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
    }
	
	function get_count_by_triple_id($tb, $fd, $id, $fds, $ids, $fdss, $idss) {
        $sum = 0;
		$this->db->select('count(*) as sums');
		$this->db->from($tb);
		$this->db->where($fd, $id);
		$this->db->where($fds, $ids);
		$this->db->where($fdss, $idss);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
    }
	
	function get_count_by_four_id($tb, $fd, $id, $fds, $ids, $fdss, $idss, $fdsss, $idsss) {
        $sum = 0;
		$this->db->select('count(*) as sums');
		$this->db->from($tb);
		$this->db->where($fd, $id);
		$this->db->where($fds, $ids);
		$this->db->where($fdss, $idss);
		$this->db->where($fdsss, $idsss);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
    }
	
	function get_count_by_five_id($tb, $fd, $id, $fds, $ids, $fdss, $idss, $fdsss, $idsss, $fdssss, $idssss) {
        $sum = 0;
		$this->db->select('count(*) as sums');
		$this->db->from($tb);
		$this->db->where($fd, $id);
		$this->db->where($fds, $ids);
		$this->db->where($fdss, $idss);
		$this->db->where($fdsss, $idsss);
		$this->db->where($fdssss, $idssss);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
    }
	
	function sum($query) {
		$total = 0;
		$query = $this->db->query($query);
		foreach ($query->result() as $row) {
			$total = $row->total;
		}
        return $total;
	}
	
	function query($query) {
		$query = $this->db->query($query);
		return $query->result();
	}
	
	function get_id_by_triple_id($tb, $field, $fd, $id, $fds, $ids, $fdss, $idss) {
        $sum = 0;
		$this->db->select($field .' as sums');
		$this->db->from($tb);
		$this->db->where($fd, $id);
		$this->db->where($fds, $ids);
		$this->db->where($fdss, $idss);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
    }
	
	function get_id_by_four_id($tb, $field, $fd, $id, $fds, $ids, $fdss, $idss, $fdsss, $idsss) {
        $sum = 0;
		$this->db->select($field .' as sums');
		$this->db->from($tb);
		$this->db->where($fd, $id);
		$this->db->where($fds, $ids);
		$this->db->where($fdss, $idss);
		$this->db->where($fdsss, $idsss);
		$query = $this->db->get();
		
		foreach ($query->result() as $row) {
			$sum = $row->sums;
		}
        return $sum;
	}
	function uploadfile01($var,$dir,$all){
        $this->load->library('upload');

        $namagambar = "file-".date('Ymdhis');
        $config=array(
            'upload_path' => $dir, //lokasi gambar akan di simpan
            'allowed_types' => $all, //ekstensi gambar yang boleh di uanggah
            'max_size' => '20000', //batas maksimal ukuran gambar
            'max_width' => '2048', //batas maksimal lebar gambar
            'max_height' => '2048', //batas maksimal tinggi gambar
            'file_name' => url_title($namagambar) //nama gambar
        );
        $this->load->library('upload', $config);
        $this->upload->initialize($config);

        if ($this->upload->do_upload($var)){
            $file = $this->upload->data();
            return $file;
        }
        else{
            return 0;
        }
    }
    
    function uploadfile($var,$dir,$all){
        $new_name = time();
        $config2=array(
            'image_library' => 'gd2',
            'upload_path' => $dir, //lokasi gambar akan di simpan
            'allowed_types' => $all, //ekstensi gambar yang boleh di uanggah
            'create_thumb' => TRUE,
            'max_size' => '50000', //batas maksimal ukuran gambar
            'file_name' => $new_name
        );
        $this->load->library('upload');
        $this->upload->initialize($config2);
        if ($this->upload->do_upload($var))
        {
            $file = $this->upload->data()['file_name'];
            $config = array(
                'image_library' => 'gd2',
                'source_image' => $dir."".$file,
                'new_image' => $dir.'thumb-400/',
                'create_thumb' => TRUE,
                'maintain_ratio' => TRUE,
                'width'=>400,
                'thumb_marker' => ''
            );
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->image_lib->clear();
            $config2 = array(
                'image_library' => 'gd2',
                'source_image' => $dir."".$file,
                'new_image' => $dir.'thumb-144/',
                'create_thumb' => TRUE,
                'maintain_ratio' => TRUE,
                'width'=>144,
                'thumb_marker' => ''
            );
            $this->image_lib->initialize($config2);
            $this->image_lib->resize();
            return $file;
        }
        else
        {
            return 0;
        }
    }
}		