<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config = array(
        'menu'          => [
                'dashboard'     => [
                        'label'         => 'Dashboard',
                        'link'          => 'dashboard',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'dashboard',
                        'icon'          => '<i class="fas fa-tachometer-alt"></i>',
                ],
                'new_reg'     => [
                        'label'         => 'Pendaftaran Baru',
                        'link'          => 'registration/new',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'dashboard',
                        'icon'          => '<i class="fas fa-plus"></i>'
                ],
                'registration'     => [
                        'label'         => 'List Pendaftaran',
                        'link'          => 'registration',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'dashboard',
                        'icon'          => '<i class="far fa-file-alt"></i>'
                ],
                'event'     => [
                        'label'         => 'Event',
                        'link'          => 'event',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="fas fa-fw fa-cog"></i>'                       
                ],
                'scrutineering'     => [
                        'label'         => 'Scrutineering',
                        'link'          => 'scrutineering',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="far fa-folder-open"></i>'                        
                ],
                'payment'     => [
                        'label'         => 'Pembayaran',
                        'link'          => 'payment',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="far fa-money-bill-alt"></i>'
                ],
                'eo'     => [
                        'label'         => 'Event Organizer',
                        'link'          => 'eo',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="fas fa-users"></i>'
                ],
                'result'     => [
                        'label'         => 'Hasil Balap',
                        'link'          => 'result',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="fas fa-flag-checkered"></i>'
                ],
                'tim'     => [
                        'label'         => 'Tim',
                        'link'          => 'tim',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="fas fa-users"></i>'
                ],
                'pembalap'     => [
                        'label'         => 'Pembalap',
                        'link'          => 'pembalap',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="fas fa-biking"></i>'
                ],
                'scrutmaster'     => [
                        'label'         => 'Master Scrutineering',
                        'link'          => 'scrutmaster',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'event saya',
                        'icon'          => '<i class="far fa-list-alt"></i>'
                ],
                'user'     => [
                        'label'         => 'Users Manager',
                        'link'          => 'user',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'setting',
                        'icon'          => '<i class="fas fa-users-cog"></i>'
                ],
                'profile'     => [
                        'label'         => 'Profil Saya',
                        'link'          => 'profile',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'setting',
                        'icon'          => '<i class="fas fa-user-cog"></i>'
                ],
                'live'     => [
                        'label'         => 'Live Timing',
                        'link'          => 'live',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'setting',
                        'icon'          => '<i class="far fa-clock"></i>'
                ],
                'report'     => [
                        'label'         => 'Report',
                        'link'          => 'report',
                        'description'   => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
                        'sub_menu'      => false,
                        'group'         => 'report',
                        'icon'          => '<i class="fas fa-print"></i>'
                ]
                ],
        'menu_group'    => ['dashboard','event','masters','setting'],
        'all_user'      => ['0000','1000', '1001', '1002','2000', '2001', '2002', '2003', '2004', '2005','3000'],
        'super_user'    => ['0000'],
        'rct_user'      => ['1000', '1001', '1002'],
        'eo_user'       => ['2000', '2001', '2002', '2003', '2004', '2005'],
        'regular_user'  => ['3000'],
        'user_group'    => [
                '0000'  => [
                        'label'         =>  'Super User',
                        'type'          => 'SUPERUSER',
                        'access'        => [
                                'dashboard'     => ['graph'],
                                'new_reg'       => ['new'],
                                'registration'  => ['list','filter','detail','edit','approval','export'],
                                'scrutineering' => ['list','filter','detail','edit','approval','export'],
                                'result'        => ['list','filter','detail','import','export'],
                                'event'         => ['list','new','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil','edit','approval','export'],
                                'eo'            => ['list','new','filter','detail','edit','approval'],
                                'payment'       => ['list','filter','detail','confirmation','approval','export'],
                                'tim'           => ['list','new','filter','edit','approval'],
                                'pembalap'      => ['list','new','filter','edit','approval'],
                                'user'          => ['list','new','filter','detail','edit','approval','changepassword'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view'],
                                'report'        => ['invoice','record','approval']
                        ],
                ],
                '1000'  => [
                        'label'         =>  'RCT - Admin',
                        'type'          =>  'RCTUSER',
                        'access'        => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail'],
                                'result'        => ['list','filter','detail'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'eo'            => ['list','filter','detail'],
                                'tim'           => ['list','filter'],
                                'pembalap'      => ['list','filter'],
                                'user'          => ['list','new','filter','detail','edit','approval','changepassword'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view'],
                                'report'        => ['invoice','record']
                        ]
                ],
                '1001'  => [
                        'label'     =>  'RCT - Support',
                        'type'      =>  'RCTUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail','edit','approval','export'],
                                'result'        => ['list','filter','detail','import','export'],
                                'event'         => ['list','new','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil','edit','approval','export'],
                                'eo'            => ['list','new','filter','detail','edit','approval'],
                                'payment'       => ['list','filter','detail'],
                                'tim'           => ['list','new','filter','edit','approval'],
                                'pembalap'      => ['list','new','filter','edit','approval'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view'],
                                'report'        => ['invoice','record']
                        ]
                ],
                '1002'  => [
                        'label'     =>  'RCT - Finance',
                        'type'      =>  'RCTUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail'],
                                'result'        => ['list','filter','detail'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'payment'       => ['list','filter','detail','approval','export'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view'],
                                'report'        => ['invoice','record','approval']
                        ]
                ],
                '2000'   =>  [
                        'label'    =>  'EO - Admin',
                        'type'      =>  'EOUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail'],
                                'result'        => ['list','filter','detail'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'eo'            => ['detail'],
                                'tim'           => ['list','filter'],
                                'pembalap'      => ['list','filter'],
                                'user'          => ['list','new','filter','detail','edit','approval','changepassword'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view']
                        ]
                ],
                '2001'   =>  [
                        'label'    =>  'EO - Support',
                        'type'      =>  'EOUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail','edit','approval','export'],
                                'result'        => ['list','filter','detail','import','export'],
                                'event'         => ['list','new','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil','edit','export'],
                                'eo'            => ['detail'],
                                'payment'       => ['list','filter','detail'],
                                'tim'           => ['list','new','filter','edit'],
                                'pembalap'      => ['list','new','filter','edit'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view']                  
                        ]
                ],
                '2002'   =>  [
                        'label'    =>  'EO - Finance',
                        'type'      =>  'EOUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail'],
                                'result'        => ['list','filter','detail'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'eo'            => ['detail'],
                                'payment'       => ['list','filter','detail','approval','export'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view']
                        ]
                ],
                '2003'   =>  [
                        'label'    =>  'EO - Customer Service',
                        'type'      =>  'EOUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail'],
                                'result'        => ['list','filter','detail'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'eo'            => ['detail'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view']
                        ]
                ],
                '2004'   =>  [
                        'label'    =>  'EO - Scrutineering Admin',
                        'type'      =>  'EOUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail'],
                                'scrutineering' => ['list','filter','detail','edit','approval','export'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'eo'            => ['detail'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view']
                        ]
                ],
                '2005'   =>  [
                        'label'    =>  'EO - Scrutineering Teknis',
                        'type'      =>  'EOUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'registration'  => ['list','filter','detail'],
                                'scrutineering' => ['list','filter','detail','edit','approval','export'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'eo'            => ['detail'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view']
                        ]
                ],
                '3000'  =>  [
                        'label'    =>  'Regular - Manager',
                        'type'      =>  'REGULARUSER',
                        'access'    => [
                                'dashboard'     => ['graph'],
                                'new_reg'       => ['new'],
                                'registration'  => ['list','filter','detail'],
                                'result'        => ['list','filter','detail','export'],
                                'event'         => ['list','filter','detail_info','detail_kelas','detail_kategori','detail_peserta','detail_hasil'],
                                'payment'       => ['list','filter','detail','konfirmasi','export'],
                                'tim'           => ['list','new','filter','edit'],
                                'pembalap'      => ['list','new','filter','edit'],
                                'profile'       => ['detail','edit','changepassword'],
                                'live'          => ['view']
                        ]
                ]
                ],
        'scrutineering_list'   => [
                 'kendaraan'    => ['Roll Cage','Cut Off Eng','Pemadam Api','Safety Belt','Towing Hook','Lampu Hujan'],
                 'driver'       => ['Helmet', 'Baju Balap, Glove, Sepatu']
         ]
        

);
