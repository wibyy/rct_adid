var API_URL = $constant.API_URL + '/token/'

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        curent_page: 1,
        isLoad: true,
        detailUrl: 'admin/transaction/detail/',
        params:{},
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        }
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', API_URL+'lists?'+a);
            this.getList()
        },
        getList(){
            this.isLoad = true
            var a = jQuery.param(this.params);
            axios.get(API_URL + '/lists_ajax?'+a, {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.listData = response.data.data.list
                    this.paginationData = response.data.data.pagination
                    this.isLoad = false
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        }
    }
})

Vue.component('table-transactions', {
    props: ['listData','paginationData'],
    template: `
        <tbody>
        <tr v-if="!listData.length">
            <td colspan="9" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData">
                 <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
                <td><p>{{ data.created_at }}</p></td>
                <td><p>{{ data.card_token }}</p></td>
                <td>
                    <p style="line-height:1">
                    {{ data.masked_card_no }} - {{ data.card_type }}<br>
                    <small>{{ data.exp_month }} / {{ data.exp_year }}</small><br>
                    <small><i>{{ data.issuer }}</i></small>
                    </p>
                </td>
                <td><p>
                    {{ data.exp_month }} / {{ data.exp_year }}<br>
                </p></td>
                 <td><p>{{ data.status }}</p></td>
                 <td><p>{{ data.customer_account }}</p></td>
                 <td>
                    <p style="line-height:1">
                        {{ data.name }}<br>
                        <small><b>{{ data.email }}</b></small><br>
                        <small><i>{{ data.phone }}</i></small><br>
                        <small><i>{{ data.address }}, {{ data.city }}, {{ data.state }}, {{ data.country }}, {{ data.zip }}</i></small>
                    </p>
                </td>
                <td>
                    <button class="btn btn-sm btn-danger" v-if="data.status==1">Set Inactive</button>
                    <button class="btn btn-sm btn-success" v-if="data.status==0">Set Active</button>
                </td>
            </tr>

        </tbody>
        `
})

