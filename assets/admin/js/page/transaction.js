var API_URL = $constant.API_URL + '/transaction/'

new Vue({
    el: '#app',
    data: {
        listData:{},
        transactionData:{},
        paginationData:{},
        curent_page: 1,
        buttonSearch: "<i class=\"fas fa-search\"></i> Search",
        isLoad: true,
        detailUrl: API_URL+'detail',
        params:{
            status:''
        },
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        }
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        filterProcess(){
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', API_URL+'?'+a);
            this.getList()           
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', API_URL+'?'+a);
            this.getList()
        },
        getList(){
            this.isLoad = true
            var a = jQuery.param(this.params);
            axios.get(API_URL + '/lists_ajax?'+a, {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.listData = response.data.data.list
                    this.paginationData = response.data.data.pagination
                    this.isLoad = false
                } else {
                    this.isLoad = false
                    this.listData = {}
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        }
    }
})

Vue.component('table-transactions', {
    props: ['listData','paginationData','detailUrl'],
    template: `
        <tbody>
        <tr v-if="!listData.length">
            <td colspan="7" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td><p>{{data.transaction_date}}</p></td>
            <td>
                <a  :href="detailUrl+'?s='+data.trx_id" >{{data.transaction_no}}</a>
            </td>
            <td style="text-align: center;">
                <p>{{data.card_type | valueCheck}}</p>
            </td>
            <td>
                 <p v-html='trxStatus(data.transaction_status)'></p>
            </td>
            <td>
                <p>{{data.transaction_total | rupiah}}</p>
            </td>
            <td>
                <p style="line-height:10pt">
                <b>{{data.name}}</b><br>
                <small><b>{{data.email}}</b></small><br>
                <small><i>{{data.customer_account}}</i></small>
                </p>
            </td>
        </tr>
        </tbody>
        `
})
