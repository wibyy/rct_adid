var API_URL = $constant.API_URL + '/customers/'

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        pagination:{},
        curent_page: 1,
        isLoad: true,
        buttonSubmit:'Save',
        buttonEditSubmit:'Save change',
        country:{},
        params:{},
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        form:{
            country : 'ID'
        },
        formEdit:{},
        formEditPassword:{}
    },
    mounted() {
        this.initPage()
        this.getCountry()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        getCountry(){
            axios.get(API_URL + '/get_country', {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.country = response.data.data
                }
            })
            .catch(error => { })
        },
        resetForm(){
            this.form = {}
            this.buttonSubmit = 'Save'
        },
        getList(){
            this.isLoad = true
            var a = jQuery.param(this.params);
            axios.get(API_URL + 'lists_ajax?'+a, {})
            .then(response => {
                if (response.status == 200) {
                    this.listData = response.data.data.list
                    this.paginationData = response.data.data.pagination
                    this.isLoad = false                    
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', API_URL+'lists?'+a);
            this.getList()
        },
        submitProcess(){
            this.buttonSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();

            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            axios({
                method: 'post',
                url: API_URL + '/insert_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.resetForm()
                    a.getList()
                    if (response.data.status == 'success') {
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.resetForm()
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        updateItems(data){
            this.formEdit = data
            $('#editProduct').modal('show')
        },
        updateItemProcess(){
            this.buttonEditSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.formEdit, function( i, v ) {
                data.append(i, v)
            })
            axios({
                method: 'post',
                url: API_URL + '/update_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.buttonEditSubmit = 'Save change';
                    a.getList()
                    if (response.data.status == 'success') {
                        $('#editProduct').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.buttonEditSubmit = 'Save change';
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        updatePassword(data){
            this.formEditPassword.cust_id = data.cust_id
            $('#editPassword').modal('show')
        },
        updatePasswordProcess(){
            this.buttonEditSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.formEditPassword, function( i, v ) {
                data.append(i, v)
            })
            axios({
                method: 'post',
                url: API_URL + '/update_password_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.buttonEditSubmit = 'Save change';
                    a.formEditPassword = {}
                    a.getList()
                    if (response.data.status == 'success') {
                        $('#editPassword').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.formEditPassword = {}
                    a.buttonEditSubmit = 'Save change';
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        deleteThis(id){
            console.log(id)
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.deleteProcess(id)
                }
            })
        },
        deleteProcess(id){
            var a = this
            var data = new FormData();
            data.append('id', id)
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                url: API_URL + '/delete_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    if (response.status == 200) {
                        a.csrf.name = response.data.csrf.name
                        a.csrf.value = response.data.csrf.hash
                        if(response.data.status == 'success'){
                            Swal.fire(
                                'Deleted!',
                                'Your product has been deleted.',
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Error!',
                                'Failed to deleting product.',
                                'error'
                            )
                        }
                        a.getList()
                    }
                })
                .catch(err => { 
                    Swal.fire(
                        'Error!',
                        'Failed to deleting product.',
                        'error'
                    )
                });           
        }
    }
})


Vue.component('table-customers', {
    props: ['listData','paginationData'],
    template: `
        <tbody>
        <tr v-if="!listData.length">
            <td colspan="6" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <p>{{data.customer_account}}</p>
            </td>
            <td>
                <p><b>{{data.name}}</b></p>
            </td>
            <td>
                <p>{{data.email}}</p>
            </td>
            <td>
                <button class="btn btn-sm btn-info mr-1" @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
                <button class="btn btn-sm btn-secondary mr-1" @click="$emit('click-password', data)"><i class="fas fa-key"></i></button>
                <button class="btn btn-sm btn-danger" @click="deleteThis(data.id)"><i class="far fa-trash-alt"></i></button>
            </td>
        </tr>
        </tbody>
        `
})

