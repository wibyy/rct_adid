var API_URL = $constant.API_URL + '/products/'

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        pagination:{},
        curent_page: 1,
        isLoad: true,
        imageURL:'',
        imageEditURL:'',
        buttonSubmit:'Save',
        buttonEditSubmit:'Save change',
        params:{},
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        form:{},
        formEdit:{}
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', API_URL+'lists?'+a);
            this.getList()
        },
        resetForm(){
            this.form = {}
            this.imageURL = ''
            this.$refs.imageFile.value=null
            this.buttonSubmit = 'Save'
        },
        getList(){
            this.isLoad = true
            var a = jQuery.param(this.params);
            axios.get(API_URL + '/lists_ajax?'+a, {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.listData = response.data.data.list
                    this.paginationData = response.data.data.pagination
                    this.isLoad = false
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        },
        submitProcess(){
            this.buttonSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();

            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            data.append('image', a.imageFile);
            axios({
                method: 'post',
                url: API_URL + '/insert_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.resetForm()
                    a.getList()
                    if (response.data.status == 'success') {
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.resetForm()
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        updateItems(data){
            this.formEdit = data
            this.imageEditURL = data.image
            $('#editProduct').modal('show')
        },
        updateItemProcess(){
            this.buttonEditSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.formEdit, function( i, v ) {
                data.append(i, v)
            })
            if(a.imageEditFile){
                data.append('imageedit', a.imageEditFile);
            }
            axios({
                method: 'post',
                url: API_URL + '/update_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.buttonEditSubmit = 'Save change';
                    a.getList()
                    if (response.data.status == 'success') {
                        $('#editProduct').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.buttonEditSubmit = 'Save change';
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        deleteThis(id){
            console.log(id)
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.deleteProcess(id)
                }
            })
        },
        deleteProcess(id){
            var a = this
            var data = new FormData();
            data.append('id', id)
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                url: API_URL + '/delete_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    if (response.status == 200) {
                        a.csrf.name = response.data.csrf.name
                        a.csrf.value = response.data.csrf.hash
                        if(response.data.status == 'success'){
                            Swal.fire(
                                'Deleted!',
                                'Your product has been deleted.',
                                'success'
                            )
                        } else {
                            Swal.fire(
                                'Error!',
                                'Failed to deleting product.',
                                'error'
                            )
                        }
                        a.getList()
                    }
                })
                .catch(err => { 
                    Swal.fire(
                        'Error!',
                        'Failed to deleting product.',
                        'error'
                    )
                });           
        },
        onFileChangeImage(e){
            const file = e.target.files[0];
            this.imageURL = URL.createObjectURL(file);          
            this.imageFile = this.$refs.imageFile.files[0];   
        },
        onFileChangeEditImage(e){
            const file = e.target.files[0];
            this.imageEditURL = URL.createObjectURL(file);          
            this.imageEditFile = this.$refs.imageEditFile.files[0];   
        },
    }
})

Vue.component('table-products', {
    props: ['listData','paginationData'],
    template: `
        <tbody>
        <tr v-if="!listData.length">
            <td colspan="6" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <img :src="data.image" class='img-ontable' alt="">
            </td>
            <td>
                <p><b>{{data.name}}</b></p>
                <p><small>{{data.description}}</small></p>
            </td>
            <td>
                <p>{{data.price | rupiah}}</p>
            </td>
            <td>
                <p style="text-align: center;">{{data.default_qty}}</p>
            </td>
            <td style="text-align: center;">
                <button class="btn btn-sm btn-info mr-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
                <button class="btn btn-sm btn-danger"  @click="$emit('click-delete', data.id)"><i class="far fa-trash-alt"></i></button>
            </td>
        </tr>
        </tbody>


        `
})
