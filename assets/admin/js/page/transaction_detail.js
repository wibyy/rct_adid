var API_URL = $constant.API_URL + '/transaction/'

new Vue({
    el: '#app',
    data: {
        transactionData:{},
        isLoad: true,
        backUrl: API_URL,
        params:{},
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        }
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
            console.log(this.params)
        },
        getList(){
            this.isLoad = true
            axios.get(API_URL + '/get_detail_ajax/'+this.params.s, {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.transactionData = response.data.data
                    this.isLoad = false
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        }
    }
})
