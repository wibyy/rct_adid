var API_URL = $constant.API_URL + '/login/'

new Vue({
    el: '#app',
    data: {
        isLoad: true,
        buttonSubmit:'Login',
        message:'',
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        form:{}
    },
    mounted() {
    },
    methods: {
        submitProcess(){
            this.buttonSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            axios({
                method: 'post',
                url: API_URL + 'sign_in_ajax',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.buttonSubmit = 'Login'
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    if(response.data.status=='success'){
                        a.buttonSubmit = 'Success, redirecting ...';
                        setTimeout(function() {
                            window.location.href = $constant.BASE_URL+'/admin'
                        }, 1500)
                    } else {
                        a.form = {}
                        a.message       = response.data.message
                    }
                })
                .catch(err => {
                    a.form = {}
                    if(err.response.data.message)
                        a.message       = err.response.data.message
                    else
                        a.message       = err
                });
        }
    }
})