var BASE_URL = $constant.API_URL
var WEB_URL =  $constant.BASE_URL
var USER =  JSON.parse($constant.USER)
var USER_GROUP =  JSON.parse($constant.USER_GROUP)
var USER_DATA =  JSON.parse(localStorage.userdata)
var PERMISSION =  JSON.parse($constant.PERMISSION_PAGE)

Vue.mixin({
    data: function() {
        return {
            get g_userData() {
                return USER_DATA
            },
        }
    },
    methods: {
        url: function(link){
            return BASE_URL + link;
        },
        toastr: function(type, message) {
            toastr.options = {
                "closeButton": false,
                "debug": true,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-bottom-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            if (type == 'warning')
                toastr.warning(message);
            else if (type == 'error')
                toastr.error(message);
            else if (type == 'info')
                toastr.info(message);
            else if (type == 'success')
                toastr.success(message);
        },
        getCurrentDate: function() {
            var currentDate = moment().format('YYYY-MM-DD')
            return currentDate
        },
        getPermission: function (params) {
           if($.inArray(params, PERMISSION)>0) 
                return true
            else 
                return false
        }
    },
})

Vue.filter('formatDate', function(value) {
    moment.locale('en-gb');
    if (value) {
        return moment(String(value)).format('ll hh:mm a')
    }
});

Vue.filter('localShortDate', function(value) {
    moment.locale('id');
    if (value) {
        return moment(String(value)).format('LL')
    }
});

Vue.filter('formatDateHumanize', function(value) {
    moment.locale('en-gb');
    if (value) { 
        
        return moment(String(value)).fromNow();
        // return moment(String(value)).format('ll hh:mm a')
    }
});

Vue.filter('userGroup', function(value) {
    if (!value || value == '' || typeof value === 'undefined' || typeof value === null ) {
        return "-"
    } else {
        var r = USER_GROUP[value].label
        return r
    }
});

Vue.filter('valueCheck', function(value) {
    if (!value || value == '' || typeof value === 'undefined' || typeof value === null ) {
        return "-"
    } else {
        return value
    }
});

Vue.filter('statusKonfirmasi', function(value) {
    if (!value || value == '' || typeof value === 'undefined' || typeof value === null ) {
        return "-"
    } else if (value == 2){
        return "Terkirim"
    } else if (value == 1){
        return "Diterima"
    } else {
        return "Belum"
    }
});

Vue.filter('imageCheck', function(value) {
    if (!value || value == '' || typeof value === 'undefined' || typeof value === null ) {
        return "no_image.png"
    } else {
        return value
    }
});

Vue.filter('rupiah', function(value) {
    let val = (value / 1).toFixed(2).replace('.', ',')
    return 'Rp ' + val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
});

var pagination = {
    props: ['paginationData','urlData'],
    template: `
    <div>
        <p class="justify-content-start">Page <b>{{paginationData.current}}</b> of <b>{{paginationData.total_page}}</b>, total <b>{{paginationData.total}}</b> data.</p>
        <nav aria-label="Page navigation example">
            <ul class="pagination pagination-sm justify-content-center">
                <li class="page-item" v-bind:class="{'disabled': !paginationData.prev}">
                    <a class="page-link" style="cursor:pointer"  tabindex="-1" :aria-disabled="!paginationData.prev"  @click="$emit('page-select', paginationData.prev)"><i class="fas fa-angle-double-left"></i></a>
                </li>
                <li class="page-item"  v-for="(data,index) in paginationData.list" v-bind:class="[{'active': data.is_active},{'disabled': data.page == '...'}]"><a class="page-link" style="cursor:pointer" @click="$emit('page-select', data.page)" >{{data.page}}</a></li>
                <li class="page-item" v-bind:class="{'disabled': !paginationData.next}">
                    <a class="page-link" style="cursor:pointer" tabindex="-1" :aria-disabled="!paginationData.next"  @click="$emit('page-select', paginationData.next)"><i class="fas fa-angle-double-right"></i></a>
                </li>
            </ul>
        </nav>
    </div>`
}

var labelStatus = {
    props: ['statusData'],
    template: `
    <div>
        <span class=\"badge badge-danger\" v-if="statusData==0">Inactive</span>
        <span class=\"badge badge-success\" v-if="statusData==1">Active</span>
        <span class=\"badge badge-warning\" v-if="statusData==2">Need Approval</span>
    </div> `
}

var labelPayment = {
    props: ['statusData'],
    template: `
    <div>
        <span class=\"priority-status text-dark\" v-if="statusData==0">Dibatalkan</span>
        <span class=\"priority-status text-success\" v-if="statusData==1">Lunas</span>
        <span class=\"priority-status text-warning\" v-if="statusData==2">Menunggu Pembayaran</span>
        <span class=\"priority-status text-info\" v-if="statusData==3">Sedang diproses</span>
        <span class=\"priority-status text-danger\" v-if="statusData==4">Tidak Valid</span>
    </div> `
}

