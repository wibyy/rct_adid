var BASE_URL = $constant.BASE_URL + 'eo/'
var API_URL = $constant.API_URL


Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);

var TableList = {
    props: ['listData', 'paginationData', 'baseUrl', 'isLoad', 'isPerm','colSpan'],
    template: `
                <tbody>
                    <tr v-if="!listData.length && !isLoad">
                        <td colspan="colSpan" style="text-align:center">
                            <p>No data record</p>
                        </td>
                    </tr>
                    <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
                        <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
                        <td>
                            <a style="cursor:pointer" :href="baseUrl+'?eo='+data.id"><b>{{data.name | valueCheck}}</b></a>
                        </td>
                        <td>
                            <label-status :status-data="data.status"></label-status>
                        </td>
                        <td style="text-align:center" v-if="isPerm.isEdited || isPerm.isApproved">
                            <button class="btn btn-sm btn-success mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="data.status==2||data.status==0&&isPerm.isApproved"><i class="fas fa-check"></i></button>
                            <button class="btn btn-sm btn-danger mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="data.status==1&&isPerm.isApproved"><i class="fas fa-ban"></i></button>
                            <button class="btn btn-sm btn-info mr-2 mb-2"  @click="$emit('click-edit', data)" v-if="isPerm.isEdited"><i class="fas fa-edit"></i></button>
                        </td>
                    </tr>
                </tbody>`
}

new Vue({
    el: '#app',
    data: {
        listData: {},
        paginationData: {},
        curent_page: 1,
        isLoad: true,
        perm:{
            isEdited:'',
            isApproved:'',
        },
        baseUrl: $constant.BASE_URL + 'user',
        params: {
            limit: 10
        },
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        progress: {
            value: 0,
            max: 100
        },
        form: {
            status: 1
        },
        buttonSubmit: 'Simpan',
        formNewMessage: ''
    },
    mounted() {
        this.initPage()
        this.perm.isEdited = this.getPermission('edit');
        this.perm.isApproved = this.getPermission('approval');
    },
    methods: {
        initPage() {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] != '')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page) {
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        filterProcess() {
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        resetFormFilter() {
            this.params = {}
            this.params.limit = 10
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        getList() {
            this.isLoad = true
            var a = this
            var params = jQuery.param(this.params);
            a.progress.value = 0
            axios({
                method: 'get',
                url: API_URL + 'get_eo?' + params
            })
                .then(response => {
                    if (response.data.status == 'success') {
                        a.progress.value = 100
                        setTimeout(function () {
                            a.listData = response.data.data.list
                            a.paginationData = response.data.data.pagination
                            a.isLoad = false
                        }, 500)
                    } else {
                        a.isLoad = false
                        a.listData = {}
                        a.paginationData = {}
                    }
                })
                .catch(error => {
                    a.isLoad = false
                    a.listData = {}
                    a.paginationData = {}
                    //this.submitClaim(type)
                })
        },
        addNew() {
            $('#newItem').modal({backdrop: 'static', keyboard: false})
        },
        processNew() {
            var a = this
            a.isLoad = true
            var data = new FormData();
            $.each(a.form, function (i, v) {
                data.append(i, v)
            })
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                data: data,
                url: API_URL + 'set_eo'
            }).then(response => {
                a.csrf.name = response.data.csrf.name
                a.csrf.value = response.data.csrf.hash
                if (response.data.status == 'success') {
                    Swal.fire('Sukses!', response.data.message, 'success')
                    a.isLoad = false
                    $('#newItem').modal('hide')
                    a.resetFormFilter()
                } else {
                    a.resetFormFilter()
                    a.formNewMessage = (response.data.message) ? response.data.message : 'undifined error!'
                    a.isLoad = false
                }
            }).catch(err => {
                a.isLoad = false
                a.formNewMessage = 'undifined error!'
                if (err.response.data.message)
                    a.formNewMessage = err.response.data.message
                else
                    a.formNewMessage = err

                setTimeout(function () {
                    window.location.reload
                }, 2000)
            })
        },
        processUpdateStatus(e) {
            var a = this
            var data = new FormData();
            var status = e.status
            var msg 

            if(status=='1')
                status = 0
            else
                status = 1

            data.append('id', e.id)
            data.append('status', status)
            data.append(a.csrf.name, a.csrf.value)
            Swal.fire({
                title: 'Anda yakin?',
                text: "Status Event Organizer akan dirubah!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, lanjutkan!'
            }).then((result) => {
                if(result.isConfirmed){
                    axios({
                        method: 'post',
                        data: data,
                        url: API_URL + 'set_eo_status'
                    }).then(response => {
                        a.csrf.name = response.data.csrf.name
                        a.csrf.value = response.data.csrf.hash
                        if (response.data.status == 'success') {
                            msg = response.data.message
                            Swal.fire('Sukses!',  msg, 'success')
                            a.getList()
                        } else {
                            msg = (response.data.message) ? response.data.message : 'undifined error!'
                            Swal.fire('Sukses!', msg, 'error')
                        }
                    }).catch(err => {
                        msg = 'undifined error!'
                        if (err.response.data.message)
                            msg = err.response.data.message
                        else
                            msg = err

                        Swal.fire('Sukses!', msg, 'error')
                        setTimeout(function () {
                            window.location.reload
                        }, 2000)
                    })
                } 
            })
        },

    },
    components: {
        'table-list': TableList
    }
})
