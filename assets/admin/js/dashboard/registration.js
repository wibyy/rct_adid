var BASE_URL = $constant.BASE_URL + 'registration/'
var API_URL = $constant.API_URL


Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);
Vue.component('label-payment', labelPayment);

var TableList = {
    props: ['listData', 'paginationData', 'baseUrl', 'isLoad'],
    template: `
                <tbody>
                    <tr v-if="!listData.length && !isLoad">
                        <td colspan="9" style="text-align:center">
                            <p>No data record</p>
                        </td>
                    </tr>
                    <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
                        <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
                        <td>
                            <a style="cursor:pointer" :href="baseUrl+'detail/'+data.id"><b>{{data.no_registration}}</b></a>
                        </td>
                        <td>
                            <p><b>{{data.no_start}}</b></p>
                        </td>
                        <td>
                            <p><b>{{data.racer_nama}}</b></p>
                            <p style="font-size:11px">{{data.team}}</p>
                        </td>
                        <td>
                            <p>{{data.manager | valueCheck}}</p>
                            <p style="font-size:11px">{{data.email}}</p>
                        </td>                        
                        <td>
                            <p>{{data.kendaraan_merek+" "+data.kendaraan_tipe}}</p>
                        </td>
                        <td>
                            <p>{{data.class}}</p>
                            <p>{{data.category}}</p>
                        </td>
                        <td>
                            <label-payment :status-data="data.status_pembayaran "></label-payment>
                        </td>
                        <td>
                            <label-status :status-data="data.status"></label-status>
                        </td>
                    </tr>
                </tbody>`
}

new Vue({
    el: '#app',
    data: {
        listData: {},
        paginationData: {},
        selectedEvent: [],
        curent_page: 1,
        isLoad: true,
        baseUrl: BASE_URL,
        imageURL: '',
        imageEditURL: '',
        listEvent: '',
        buttonSubmit: 'Save',
        buttonEditSubmit: 'Save change',
        buttonSearch: 'Search',
        params: {
            limit: 10
        },
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        option: [],
        optionsClass: [],
        progress: {
            value: 0,
            max: 100
        }
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage() {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] != '')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getEvent()
            this.getList()
        },
        selectPage(page) {
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        filterProcess() {
            if (!this.params.id) {
                delete this.params.id
            }
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        eventProcess() {
            if (!this.params.id) {
                delete this.params.id
            }
            delete this.params.class
            delete this.params.no_reg
            delete this.params.nama
            delete this.params.tim
            this.optionsClass = []
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
            this.getClass()
        },
        resetFormFilter() {
            var id = this.params.id
            this.params = {}
            this.params.limit = 10
            this.params.id = id
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        resetForm() {
            this.form = {}
            this.imageURL = ''
            this.$refs.imageFile.value = null
            this.buttonSubmit = 'Save'
        },
        getEvent() {
            var a = this
            axios.get(API_URL + 'get_event', {})
                .then(response => {
                    if (response.data.status == 'success') {
                        a.listEvent = response.data.data.list
                        for (i = 0; i < a.listEvent.length; i++) {
                            a.option.push({
                                label: a.listEvent[i].label,
                                code: a.listEvent[i].id,
                            })
                        }
                        a.getClass()
                    } else {
                        a.listEvent = {}
                    }
                })
                .catch(error => {
                    a.listEvent = {}
                    //this.submitClaim(type)
                })
        },
        getList() {
            this.isLoad = true
            var a = this
            var params = jQuery.param(this.params);
            a.progress.value = 0
            axios({
                method: 'get',
                url: API_URL + 'get_registration?' + params,
                headers: { 'Content-Type': 'multipart/form-data' }
            })
                .then(response => {
                    if (response.data.status == 'success') {
                        a.progress.value = 100
                        setTimeout(function () {
                            a.listData = response.data.data.list
                            a.paginationData = response.data.data.pagination
                            a.isLoad = false
                        }, 500)
                    } else {
                        a.isLoad = false
                        a.listData = {}
                        a.paginationData = {}
                    }
                })
                .catch(error => {
                    a.isLoad = false
                    a.listData = {}
                    a.paginationData = {}
                    //this.submitClaim(type)
                })
        },
        getClass() {
            this.optionsClass = []
            var cla = {};
            for (i = 0; i < this.listEvent.length; i++) {
                if (this.params.id == this.listEvent[i].id) {
                    cla = this.listEvent[i].kelas
                }
            }
            for (i = 0; i < cla.length; i++) {
                this.optionsClass.push({
                    code: cla[i].id,
                    label: cla[i].label
                })
            }
        }

    },
    components: {
        'table-list': TableList
    }
})
