var BASE_URL = $constant.BASE_URL + 'registration/new'
var API_URL = $constant.API_URL

Vue.component('v-select', VueSelect.VueSelect);

new Vue({
    el: '#app',
    data: {
        isLoad: false,
        isLoadEvent: true,
        isOut: false,
        eventCheck:'',
        baseUrl: BASE_URL,
        listUrl: $constant.BASE_URL + 'registration?limit=10&id=',
        EventSelectedName: 'No event selected.',
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        option: [],
        optionsClass: [],
        optionsCategory: [],
        progress: {
            value: 0,
            max: 100
        },
        listClass: {},
        form: {},
        eventSelected:{},
        listEvent: false,
        listPembalap: {},
        listTim: {},
        listEntrant: {},
        listManager: {},
        optionPembalap: [],
        optionTim: [],
        optionEntrant: [],
        optionManager: [],

    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage() {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] != '')
                    this.form[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            
            this.getEvent()
            this.getPembalap()
            this.getTim()
        },
        //FORM EVENT
        eventProcess() {
            this.optionsClass = []
            this.optionsCategory = []
            this.eventSelected = {}

            if (!this.listEvent) {
                delete this.form.kelas
                delete this.form.kategori
            } 
            
            if (this.form.code)
                this.eventSelected.code = this.form.code
            if (this.form.kelas)
                this.eventSelected.kelas = this.form.kelas
            if (this.form.kategori)
                this.eventSelected.kategori = this.form.kategori

            var a = jQuery.param(this.eventSelected);
            window.history.pushState({}, '', BASE_URL + '?' + a);

            
            if(this.listEvent.kelas)
                this.getClass() 
            if(this.listEvent.label){
                this.EventSelectedName = this.listEvent.label
            } else {
                this.EventSelectedName = 'No event found!'
            }
            this.isLoadEvent = false
        },
        getEvent() {
            var a = this
            a.isLoadEvent = true
            if(this.form.code){
            axios.get(API_URL + 'get_event_bycode/'+a.form.code, {})
                .then(response => {
                    if (response.data.status == 'success') {
                        a.listEvent = response.data.data
                        a.eventCheck = true
                        a.eventProcess()
                    } else {
                        a.eventCheck = false
                        a.listEvent = false
                        a.eventProcess()
                    }
                })
                .catch(error => {
                    a.eventCheck = false
                    a.listEvent = false
                    a.eventProcess()
                })
            } else {
                a.eventCheck = false
                a.listEvent = false
                a.eventProcess()
            }
        },
        getClass() {
            this.listClass = this.listEvent.kelas

            for (i = 0; i < this.listClass.length; i++) {
                this.optionsClass.push({
                    code: this.listClass[i].id,
                    label: this.listClass[i].label
                })
            }

            this.getCategory()
        },
        classProcess() {
            this.optionsCategory = []
            this.eventSelected = {}
            if (!this.form.kelas) {
                delete this.form.kelas
                delete this.form.kategori
            }

            if (this.form.code)
                this.eventSelected.code = this.form.code
            if (this.form.kelas)
                this.eventSelected.kelas = this.form.kelas
            if (this.form.kategori)
                this.eventSelected.kategori = this.form.kategori

            var a = jQuery.param(this.eventSelected);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getCategory()
        },
        categoryProcess() {
            
            if (!this.form.kategori) {
                delete this.form.kategori
            }
            if (this.form.code)
                this.eventSelected.code = this.form.code
            if (this.form.kelas)
                this.eventSelected.kelas = this.form.kelas
            if (this.form.kategori)
                this.eventSelected.kategori = this.form.kategori

            var a = jQuery.param(this.eventSelected);
            window.history.pushState({}, '', BASE_URL + '?' + a);
        },
        getCategory() {
            var cla = {};
            for (i = 0; i < this.listClass.length; i++) {
                if (this.eventSelected.kelas == this.listClass[i].id) {
                    cla = this.listClass[i].kategori
                }
            }
            for (i = 0; i < cla.length; i++) {
                this.optionsCategory.push({
                    code: cla[i].id,
                    label: cla[i].label
                })
            }
        },

        //FORM
        getPembalap() {
            var a = this
            axios.get(API_URL + 'get_pembalap', {})
                .then(response => {
                    if (response.data.status == 'success') {
                        a.listPembalap = response.data.data.list
                        for (i = 0; i < a.listPembalap.length; i++) {
                            a.optionPembalap.push({
                                label: a.listPembalap[i].full_name,
                                code: a.listPembalap[i].id,
                            })
                        }
                    } else {
                        a.listPembalap = {}
                    }
                })
                .catch(error => {
                    a.listPembalap = {}
                    //this.submitClaim(type)
                })
        },
        getTim() {
            var a = this
            axios.get(API_URL + 'get_tim', {})
                .then(response => {
                    if (response.data.status == 'success') {
                        a.listTim = response.data.data.list
                        for (i = 0; i < a.listTim.length; i++) {
                            a.optionTim.push({
                                label: a.listTim[i].name,
                                code: a.listTim[i].id,
                            })
                        }
                    } else {
                        a.listTim = {}
                    }
                })
                .catch(error => {
                    a.listTim = {}
                    //this.submitClaim(type)
                })
        },

        //FORM PROCESS
        saveForm() {
            this.isOut = false
            this.registrationProcess()
        },
        saveOutForm() {
            this.isOut = true
            this.registrationProcess()
        },
        resetForm() {
            this.form = {}
            this.form = this.eventSelected
        },
        registrationProcess() {
            var a = this
            a.isLoad = true
            a.progress.value = 0
            
            var data = new FormData();
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                data: data,
                url: API_URL + 'set_registration'
            }).then(response => {
                console.log(response)
                a.csrf.name = response.data.csrf.name
                a.csrf.value = response.data.csrf.hash
                a.resetForm()
                if (response.data.status == 'success') {
                    a.progress.value = 100
                    setTimeout(function () {
                        a.isLoad = false
                        a.progress.value = 0
                        if(a.isOut){
                            window.location.href = $constant.BASE_URL + 'registration?id=' + a.listEvent.id
                        }
                    }, 1500)
                    Swal.fire('Sukses!',response.data.message, 'success')
                } else {
                    Swal.fire('Gagal!',(response.data.message)?response.data.message:'undifined error!','error' )
                    a.isLoad = false
                }
            }).catch(err => {
                a.isLoad = false
                console.log(err)
                var message = 'undifined error!'
                if(err.response.data.message)
                    message       = err.response.data.message
                else
                    message       = err
                Swal.fire('Gagal!',message,'error' )
            })
        }
    },
    components: {
        // 'detail-registration' : DetailRegistration
    }
})
