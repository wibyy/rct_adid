var BASE_URL = $constant.BASE_URL + 'event/category'
var API_URL = $constant.API_URL

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        curent_page: 1,
        baseUrl: API_URL,
        isLoad: true,
        listEvent:'',
        listClass:{
            class:{}
        },
        buttonSubmit:'Save',
        buttonEditSubmit:'Save change',
        buttonSearch : 'Search', 
        params:{
            limit:10,
            id:CLASS_ID,
            status:'ALL'
        },
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        form:{},
        formEdit:{}
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
            this.getEvent()
            if(this.params.id!='')
                this.getClass(this.params.id)
                // this.getClass(this.params.id)
        },  
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()
        },
        filterProcess(){
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetFormFilter(){
            this.params = {}
            this.params.limit = 10
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetForm(){
            this.form = {}
            this.buttonSubmit = 'Save'
        },
        getList(){
            this.isLoad = true
            var a = jQuery.param(this.params);
            axios.get(API_URL + 'get_category?'+a, {})
            .then(response => {

                if (response.data.status == 'success') {
                    this.listData = response.data.data.list
                    this.paginationData = response.data.data.pagination
                    this.isLoad = false
                } else {
                    this.isLoad = false
                    this.listData = {}
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        },
        getEvent(){
            axios.get(API_URL + 'get_event', {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.listEvent = response.data.data.list
                } else {
                    this.listEvent = {}
                }
            })
            .catch(error => {
                this.listEvent = {}
                //this.submitClaim(type)
            })
        },
        getClass(data){
            if(data)
                this.listClass = this.listEvent[data].class
            else 
                this.listClass = {}
        },
        submitProcess(){
            this.buttonSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();

            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            data.append('id', a.params.id);
            axios({
                method: 'post',
                url: API_URL + '/set_category',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.resetForm()
                    a.getList()
                    if (response.data.status == 'success') {
                        $('#newItems').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.resetForm()
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        updateItems(data){
            this.formEdit = data
            console.log(data)
            $('#editItem').modal('show')
        },
        updateItemProcess(){
            this.buttonEditSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.formEdit, function( i, v ) {
                data.append(i, v)
            })
            if(a.imageEditFile){
                data.append('imageedit', a.imageEditFile);
            }
            axios({
                method: 'post',
                url: API_URL + '/update_category',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.buttonEditSubmit = 'Save change';
                    a.getList()
                    
                    if (response.data.status == 'success') {
                        $('#editItem').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    console.log( err)
                    a.buttonEditSubmit = 'Save change';
                    Swal.fire(
                        'Error!',
                        'Error',
                        'error'
                    )
                });
        }
    }
})

Vue.component('table-class', {
    props: ['listData','paginationData'],
    template: `
        <tbody>
        <tr v-if="!listData.length">
            <td colspan="5" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <b>{{data.name}}</b>
            </td>
            <td>
                <label-status :status-data="data.status"></label-status>
            </td>
            <td style="text-align: center;">
                <button class="btn btn-sm btn-info mr-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
            </td>
        </tr>
        </tbody>
        `
})
