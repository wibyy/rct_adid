var BASE_URL = $constant.BASE_URL + 'event/detail/kelas/'
var API_URL = $constant.API_URL
var ID_EVENT = PAGE_ID

Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);

var TableList = {
    props: ['listData','paginationData', 'baseUrl', 'isLoad', 'isPerm','colSpan'],
    template: `
        <tbody>
        <tr v-if="!listData.length && !isLoad">
            <td :colspan="colSpan" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <a style="cursor:pointer" :href="baseUrl+'?kelas='+data.id"><b>{{data.label}}</b></a>
                <p><small>{{data.sub_label}}</small></p>
            </td>
            <td>
                <p>{{data.biaya | rupiah}}</p>
            </td>
            <td>
                <label-status :status-data="data.status"></label-status>
            </td>
            <td style="text-align: center;" v-if="isPerm.isEdited">
                <button class="btn btn-sm btn-info mr-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
            </td>
        </tr>
        </tbody>
        `}

new Vue({
    el: '#app',
    data: {
        baseUrl: BASE_URL+ID_EVENT,
        kategoriUrl: $constant.BASE_URL + 'event/detail/kategori/'+ID_EVENT,
        isLoad:false,
        perm:{
            isEdited:'',
            isApproved:'',
        },
        eventData:{},
        listData:{},
        paginationData:{},
        buttonSubmit:'Save',
        buttonEditSubmit:'Save change',
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        params:{
            event: ID_EVENT,
            limit: 10,
            status:''
        },
        paramsEvent:{
            id: ID_EVENT
        },
        progress: {
            value: 0,
            max: 100
        },
        form:{
            status : 1,
            event : ID_EVENT
        },
        formNewMessage: '',
        formEdit:{
            status : 1,
            event : ID_EVENT
        },
        formEditMessage: '',
        optionsStatus: [
            { value: '', text: '-- Pilih Status --' },
            { value: 'ALL', text: 'ALL' },
            { value: '1', text: 'Active' },
            { value: '0', text: 'Inactive' },
            { value: '2', text: 'Need Approval' }
          ]
    },
    mounted() {
        this.getEvent()
        this.initPage()
        this.perm.isEdited = this.getPermission('edit');
        this.perm.isApproved = this.getPermission('approval');
    },
    methods: {
        getEvent(){
            var a = this
            var params = jQuery.param(a.paramsEvent);
            a.progress.value = 30
            a.isLoad = true
            axios.get(API_URL + 'get_event?'+params, {})
            .then(response => {
                if (response.data.status == 'success') {
                    a.progress.value = 80
                    setTimeout(function () {
                        a.progress.value = 100
                        a.eventData = response.data.data.list[0]
                        a.isLoad = false
                    }, 500)
                } else {
                    a.isLoad = false
                    a.eventData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.eventData = {}
            })
        },
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', this.baseUrl+'?'+a);
            this.getList()
        },
        filterProcess(){
            delete  this.params.limit
            delete  this.params.event
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', this.baseUrl+'?'+a);
            this.params.limit = 10
            this.params.event = ID_EVENT
            this.getList()           
        },
        resetForm(){
            delete this.form.label
            delete this.form.sub_label
            delete this.form.biaya
        },
        resetFormEdit(){
            delete this.formEdit.label
            delete this.formEdit.sub_label
            delete this.formEdit.biaya
        },
        resetFormFilter(){
            this.params = {}
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', this.baseUrl+'?'+a);
            this.params.limit = 10
            this.params.event = ID_EVENT
            this.getList()           
        },
        getList(){
            var a = this
            a.isLoad = true
            a.progress.value = 0
            var params = jQuery.param(a.params);
            axios.get(API_URL + 'get_kelas?'+params, {})
            .then(response => {
                a.progress.value = 100
                if (response.data.status == 'success') {
                    setTimeout(function () {
                        a.listData = response.data.data.list
                        a.paginationData = response.data.data.pagination
                        a.isLoad = false
                    }, 500)
                } else {
                    a.isLoad = false
                    a.listData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.listData = {}
                //this.submitClaim(type)
            })
        },
        addNew() {
            $('#newItem').modal('show')
        },
        processNew(){
            var a = this
            a.isLoad = true
            var data = new FormData();
            $.each(a.form, function (i, v) {
                data.append(i, v)
            })
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                data: data,
                url: API_URL + 'set_kelas'
            }).then(response => {
                a.resetForm()
                a.csrf.name = response.data.csrf.name
                a.csrf.value = response.data.csrf.hash
                if (response.data.status == 'success') {
                    Swal.fire('Sukses!', response.data.message, 'success')
                    a.isLoad = false
                    $('#newItem').modal('hide')
                    a.resetFormFilter()
                } else {
                    a.formNewMessage = (response.data.message) ? response.data.message : 'undifined error!'
                    a.isLoad = false
                }
            }).catch(err => {
                a.isLoad = false
                a.formNewMessage = 'undifined error!'
                if (err.response.data.message)
                    a.formNewMessage = err.response.data.message
                else
                    a.formNewMessage = err

                setTimeout(function () {
                    window.location.reload
                }, 2000)
            })
        },
        updateItems(data){
            this.formEdit = data
            $('#editItem').modal('show')
        },
        updateItemProcess(){
            var a = this
            a.isLoad = true
            var data = new FormData();
            $.each(a.formEdit, function (i, v) {
                data.append(i, v)
            })
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                data: data,
                url: API_URL + 'update_kelas'
            }).then(response => {
                a.csrf.name = response.data.csrf.name
                a.csrf.value = response.data.csrf.hash
                if (response.data.status == 'success') {
                    Swal.fire('Sukses!', response.data.message, 'success')
                    a.isLoad = false
                    $('#editItem').modal('hide')
                    a.resetFormFilter()
                } else {
                    a.formEditMessage = (response.data.message) ? response.data.message : 'undifined error!'
                    a.isLoad = false
                    a.resetFormFilter()
                }
            }).catch(err => {
                a.isLoad = false
                a.formEditMessage = 'undifined error!'
                if (err.response.data.message)
                    a.formEditMessage = err.response.data.message
                else
                    a.formEditMessage = err

                setTimeout(function () {
                    window.location.reload
                }, 2000)
            })
        },
    },
    components: {
        'table-list': TableList
    }
})

