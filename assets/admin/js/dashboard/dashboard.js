var BASE_URL = $constant.BASE_URL + 'dashboard/'
var API_URL = $constant.API_URL


Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);

var listPendaftaran = {
    props: ['listData'],
    template : `
    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-dark text-uppercase mb-1">Total Pendaftaran</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{listData.pendaftaran.total}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-success shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1"> Pembayaran Lunas</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{listData.pendaftaran.lunas}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-warning shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Menunggu Pembayaran
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{listData.pendaftaran.menunggu_pembayaran}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6 mb-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Sedang Diproses</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">{{listData.pendaftaran.sedang_diproses}}</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-comments fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `
};

new Vue({
    el: '#app',
    data: {
        isLoad: true,
        userInfo: JSON.parse($constant.USER),
        listData:{},
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        progress: {
            value: 0,
            max: 100
        },
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList() {
            this.isLoad = true
            var a = this
            a.progress.value = 0
            axios({
                method: 'get',
                url: API_URL + 'dashboard'
            })
                .then(response => {
                    if (response.data.status == 'success') {
                        a.progress.value = 100
                        a.listData = response.data.data

                        if (typeof a.listData.pendaftaran_cart != 'undefined') {
                            var data4 = {
                                label: 'Grafik Pendaftaran',
                                htmlId: 'pendaftaranCart',
                                type: 'bar',
                                palette: 'cb-Set2',
                                series: a.listData.pendaftaran_cart.rows,
                                caption: a.listData.pendaftaran_cart.labels
                            }
                            a.multiBarCart(data4);
                        }

                        setTimeout(function () {
                            a.isLoad = false
                        }, 500)
                    } else {
                        a.isLoad = false
                        a.listData = {}
                    }
                })
                .catch(error => {
                    a.isLoad = false
                    a.listData = {}
                    //this.submitClaim(type)
                })
        },
        multiBarCart(data) {
            var ctx = document.getElementById(data.htmlId);
            var a = this
            var arrData = data.series
            var datasetsValue = []

            console.log(arrData);

            $.each(arrData, function( i, v ) {
                var color = palette(data.palette, arrData.length).map(function(hex) {return '#' + hex; })
                    datasetsValue[i] = {
                        label: v.label,
                        backgroundColor: color[i],
                        borderColor: color[i],
                        data: v.data,
                      }   
        
            })
            var myBarChart = new Chart(ctx, {
              type: data.type,
              data: {
                labels: data.caption,
                datasets: datasetsValue
              },
              options: {
                maintainAspectRatio: false,
                tooltips: {
                    mode: 'index',
                    intersect: false
                },
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }
            });
        },
    },
    components: {
        'list-pendaftaran': listPendaftaran
    }
})
