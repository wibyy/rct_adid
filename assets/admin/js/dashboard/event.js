var BASE_URL = $constant.BASE_URL + 'event/'
var API_URL = $constant.API_URL

Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);

var TableList = {
    props: ['listData','paginationData', 'baseUrl', 'isLoad','isPerm','colSpan'],
    template: `
        <tbody>
        <tr v-if="!listData.length && !isLoad">
            <td :colspan="colSpan" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <img :src="data.logo_thumb " style="max-width:114px;max-height:42px;">
            </td>
            <td>
                <a style="cursor:pointer" :href="baseUrl+'detail/info/'+data.id"><b>{{data.event_code}}</b></a>
            </td>
            <td>
                <a style="cursor:pointer" :href="baseUrl+'detail/info/'+data.id"><b>{{data.label}}</b></a>
                <p><small>{{data.sub_label}}</small></p>
            </td>
            <td>
                <p>{{data.lokasi}}</p>
            </td>
            <td>
                <p>{{data.date_start | localShortDate}} - {{data.date_end | localShortDate}}</p>
            </td>
            <td style="text-align:center">
                <label-status :status-data="data.status"></label-status>
            </td>
            <td style="text-align: center;" v-if="(isPerm.isEdited && isPerm.isApproved)">
                <button class="btn btn-sm btn-success mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="(data.status==2||data.status==0) && isPerm.isApproved"><i class="fas fa-check"></i></button>
                <button class="btn btn-sm btn-danger mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="data.status==1 && isPerm.isApproved"><i class="fas fa-ban"></i></button>
                <button class="btn btn-sm btn-info mr-2 mb-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit" v-if="isPerm.isEdited"></i></button>
            </td>
        </tr>
        </tbody>
        `}

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        selectedEvent:'',
        perm:{
            isEdited:'',
            isApproved:'',
        },
        curent_page: 1,
        isLoad: true,
        baseUrl: BASE_URL,
        imageURL:'',
        imageEditURL:'',
        listEvent:'',
        buttonSubmit:'Save',
        buttonEditSubmit:'Save change',
        buttonSearch : 'Search', 
        params:{
            limit:10,
            status:''
        },
        progress: {
            value: 0,
            max: 100
        },
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        form:{},
        formEdit:{},
        optionsStatus: [
            { value: '', text: '-- Pilih Status --' },
            { value: 'ALL', text: 'ALL' },
            { value: '1', text: 'Active' },
            { value: '0', text: 'Inactive' },
            { value: '2', text: 'Need Approval' }
          ]
    },
    mounted() {
        this.initPage()
        this.perm.isEdited = this.getPermission('edit');
        this.perm.isApproved = this.getPermission('approval');
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()
        },
        filterProcess(){
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetFormFilter(){
            this.params = {}
            this.params.limit = 10
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetForm(){
            this.form = {}
            this.imageURL = ''
            this.$refs.imageFile.value=null
            this.buttonSubmit = 'Save'
        },
        getList(){
            this.isLoad = true
            var a = this
            var params = jQuery.param(a.params);
            axios.get(API_URL + 'get_event?'+params, {})
            .then(response => {
                a.listEvent = (response.data.list_event)?response.data.list_event:{}
                if (response.data.status == 'success') {
                    a.progress.value = 100
                    setTimeout(function () {
                        a.listData = response.data.data.list
                        a.paginationData = response.data.data.pagination
                        a.isLoad = false
                        a.progress.value = 0
                    }, 500)
                } else {
                    a.isLoad = false
                    a.listData = {}
                    a.paginationData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.listData = {}
                a.paginationData = {}
            })
        },
        addNew() {
            $('#newItems').modal('show')
        },
        submitProcess(){
            this.buttonSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();

            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            data.append('image', a.imageFile);
            axios({
                method: 'post',
                url: API_URL + '/set_event',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.params.v = 'list'
                    a.resetForm()
                    a.getList()
                    if (response.data.status == 'success') {
                        $('#newItems').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.resetForm()
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        updateItems(data){
            this.formEdit = data
            this.imageEditURL = data.logo
            $('#editItem').modal('show')
        },
        updateItemProcess(){
            this.buttonEditSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.formEdit, function( i, v ) {
                data.append(i, v)
            })
            if(a.imageEditFile){
                data.append('imageedit', a.imageEditFile);
            }
            axios({
                method: 'post',
                url: API_URL + '/update_event',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.buttonEditSubmit = 'Save change';
                    a.getList()
                    
                    if (response.data.status == 'success') {
                        $('#editItem').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    console.log( err)
                    a.buttonEditSubmit = 'Save change';
                    Swal.fire(
                        'Error!',
                        'Error',
                        'error'
                    )
                });
        },
        onFileChangeImage(e){
            const file = e.target.files[0];
            this.imageURL = URL.createObjectURL(file);          
            this.imageFile = this.$refs.imageFile.files[0];   
        },        
        onFileChangeEditImage(e){
            const file = e.target.files[0];
            this.imageEditURL = URL.createObjectURL(file);          
            this.imageEditFile = this.$refs.imageEditFile.files[0];   
        },
        processUpdateStatus(){

        }
    },
    components: {
        'table-list': TableList
    }
})

