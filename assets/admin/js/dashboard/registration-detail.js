var BASE_URL = $constant.BASE_URL + 'registration/detail/'
var API_URL = $constant.API_URL

var DetailRegistration = {
    props: ['listData', 'isLoad'],
    template: ` 
        <div class="row justify-content-center" v-if="!isLoad">
            <div class="col-9">
                <div class="row align-items-center event-title">
                    <div class="col-12 col-sm-3 logo">
                        <img :src="listData.event.event_logo" alt="">
                    </div>
                    <div class="col-12 col-sm-4 title">
                        <h1 style="margin:0;padding:0" class="event-title-big">{{listData.event.event}}</h1>
                        <p style="margin:0;padding:0" class="text-muted">{{listData.event.event_sub}}</p>
                        <p class="lokasi-event" >{{listData.event.lokasi}}</p>
                    </div>
                    <div class="col-12 col-sm-3 title">
                        <p class="mt-2">Class</p>
                        <b>{{listData.event.kelas}}</b>
                        <p class="mt-2">Category</p>
                        <b>{{listData.event.kategori}} </b>
                    </div>
                    <div class="col-12 col-sm-2 title">
                        <div class="no-start">
                            <div class="box-no-start">
                                <h3>{{listData.no_start}}</h3>
                            </div>
                            <p>No. <b>{{listData.no_registration}}</b></p>
                        </div>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-12">
                        <div class="row title-row">
                            <div class="col-12">
                                <p>Data Pembalap</p>
                            </div>
                        </div>
                        <div class="row detail-info">
                            <div class="col-12">
                                <div class="info-item">
                                    <p>Nama</p>
                                    <b>{{listData.pembalap.full_name}}</b>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="info-item">
                                    <p>Email / Hp.</p>
                                    <b>{{listData.pembalap.email +' / '+ listData.pembalap.phone}}</b>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="info-item">
                                    <p>KIS IMI</p>
                                    <b>{{listData.pembalap.kis_imi | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="info-item">
                                    <p>PengProv IMI</p>
                                    <b>{{listData.pembalap.pengprov_imi | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="info-item">
                                    <p>Tim</p>
                                    <b>{{listData.tim.name | valueCheck}}</b>
                                </div>
                            </div>
                        </div>
                        <div class="row detail-info">
                            <div class="col-5">
                                <div class="info-item">
                                    <p>Manager</p>
                                    <b>{{listData.tim.mng_name | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="info-item">
                                    <p>Lisensi</p>
                                    <b>{{listData.tim.mng_lisensi | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="info-item">
                                    <p>Hp.</p>
                                    <b>{{listData.tim.mng_phone | valueCheck}}</b>
                                </div>
                            </div>
                        </div>
                        <div class="row detail-info">
                            <div class="col-5">
                                <div class="info-item">
                                    <p>Entrant</p>
                                    <b>{{listData.tim.entrant_name | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="info-item">
                                    <p>Lisensi</p>
                                    <b>{{listData.tim.entrant_license | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-3">
                                <div class="info-item">
                                    <p>Hp.</p>
                                    <b>{{listData.tim.entrant_phone | valueCheck}}</b>
                                </div>
                            </div>
                        </div>
                        <div class="row title-row mt-4">
                            <div class="col-12">
                                <p>Data Kendaraan</p>
                            </div>
                        </div>
                        <div class="row detail-info">
                            <div class="col-12">
                                <div class="info-item">
                                    <p>Merek / Tipe</p>
                                    <b>{{listData.kendaraan_merek+' '+listData.kendaraan_tipe}}</b>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="info-item">
                                    <p>Tahun</p>
                                    <b>{{listData.kendaraan_tahun | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="info-item">
                                    <p>No Rangka</p>
                                    <b>{{listData.kendaraan_no_rangka | valueCheck}}</b>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="info-item">
                                    <p>No Mesin</p>
                                    <b>{{listData.kendaraan_no_mesin | valueCheck}}</b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`
}

new Vue({
    el: '#app',
    data: {
        listData: {
            event: {},
            racer: {},
            team: {},
            user: {},
        },
        isLoad: true,
        baseUrl: BASE_URL,
        listUrl: $constant.BASE_URL + 'registration?id=',
        idDetail: DETAIL,
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        progress: {
            value: 0,
            max: 100
        }
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage() {
            this.getList()
        },
        getList() {
            this.isLoad = true
            var a = this
            a.progress.value = 10
            axios({
                method: 'get',
                url: API_URL + 'get_registration-detail/' + a.idDetail
            })
                .then(response => {
                    if (response.data.status == 'success') {
                        a.progress.value = 100
                        setTimeout(function () {
                            a.listData = response.data.data
                            a.isLoad = false
                            a.progress.value = 0
                        }, 500)
                    } else {
                        a.isLoad = false
                        a.listData = {}
                    }
                })
                .catch(error => {
                    a.isLoad = false
                    a.listData = {}
                })
        },

    },
    components: {
        'detail-registration': DetailRegistration
    }
})
