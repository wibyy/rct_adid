var BASE_URL = $constant.BASE_URL + 'payment/'
var API_URL = $constant.API_URL

Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);
Vue.component('label-payment', labelPayment);

var TableList = {
    props: ['listData','paginationData', 'baseUrl', 'isLoad','isPerm','colSpan'],
    template: `
        <tbody>
        <tr v-if="!listData.length && !isLoad">
            <td :colspan="colSpan" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
            <th scope="row" style="text-align: center;">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td >
                <b>{{data.no_registration}}</b>
            </td>
            <td style="text-align: center;">
                <b>{{data.no_start}}</b>
            </td>
            <td>
                <b>{{data.racer_nama}}</b>
            </td>
            <td>
                <b>{{data.event}}</b>
                <p><small>{{data.class +' / '+ data.category }}</small></p>
            </td>
            <td>
                <p>{{data.biaya | rupiah}}</p>
            </td>
            <td>
                <label-payment :status-data="data.status_pembayaran "></label-payment>
            </td>
            <td>
                <p>{{ data.konfirmasi | statusKonfirmasi }}</p>
            </td>
            <td  v-if="isPerm.isExport" style="text-align:center">
                <a class="btn btn-sm btn-dark mr-2 mb-2"  :href="baseUrl+'pdf/'+data.id"><i class="fas fa-print"></i></a>
            </td>
            <td style="text-align: center;" v-if="isPerm.isConfirm">
                <button class="btn btn-sm btn-success mr-2 mb-2"  @click="$emit('click-edit', data)" v-if="!parseInt(data.konfirmasi)">Konfirmasi</button>
                <p v-if="parseInt(data.konfirmasi)">-</p>
            </td>
            <td style="text-align: center;" v-if="isPerm.isApproved">
                <button class="btn btn-sm btn-success mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="(data.status_pembayaran==2||data.status_pembayaran==0)"><i class="fas fa-check"></i></button>
                <button class="btn btn-sm btn-danger mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="data.status_pembayaran==1"><i class="fas fa-ban"></i></button>
            </td>
        </tr>
        </tbody>
        `}

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        selectedEvent:'',
        perm:{
            isConfirm:'',
            isApproved:'',
            isExport:'',
        },
        curent_page: 1,
        isLoad: true,
        baseUrl: BASE_URL,
        params:{
            limit:10,
            status:''
        },
        progress: {
            value: 0,
            max: 100
        },
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        optionsStatus: [
            { value: '', text: '-- Pilih Status --' },
            { value: 'ALL', text: 'ALL' },
            { value: '1', text: 'Lunas' },
            { value: '0', text: 'Dibatalkan' },
            { value: '2', text: 'Menunggu Pembayaran' },
            { value: '3', text: 'Sedang Diproses' }
          ]
    },
    mounted() {
        this.initPage()
        this.perm.isConfirm = this.getPermission('konfirmasi');
        this.perm.isApproved = this.getPermission('approval');
        this.perm.isExport = this.getPermission('export');
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()
        },
        filterProcess(){
            this.params.page = 1
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetFormFilter(){
            this.params = {}
            this.params.limit = 10
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetForm(){
            this.form = {}
            this.imageURL = ''
            this.$refs.imageFile.value=null
            this.buttonSubmit = 'Save'
        },
        getList(){
            this.isLoad = true
            var a = this
            var params = jQuery.param(a.params);
            axios.get(API_URL + 'get_payment_list?'+params, {})
            .then(response => {
                
                if (response.data.status == 'success') {
                    a.progress.value = 100
                    setTimeout(function () {
                        a.listData = response.data.data.list
                        a.paginationData = response.data.data.pagination
                        a.isLoad = false
                        a.progress.value = 0
                    }, 500)
                } else {
                    a.isLoad = false
                    a.listData = {}
                    a.paginationData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.listData = {}
                a.paginationData = {}
            })
        },
        addNew() {
            $('#newItems').modal('show')
        },
        submitProcess(){
            this.buttonSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();

            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            data.append('image', a.imageFile);
            axios({
                method: 'post',
                url: API_URL + '/set_event',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.params.v = 'list'
                    a.resetForm()
                    a.getList()
                    if (response.data.status == 'success') {
                        $('#newItems').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.resetForm()
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        updateItems(data){
            this.formEdit = data
            this.imageEditURL = data.logo
            $('#editItem').modal('show')
        },
        updateItemProcess(){
            this.buttonEditSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.formEdit, function( i, v ) {
                data.append(i, v)
            })
            if(a.imageEditFile){
                data.append('imageedit', a.imageEditFile);
            }
            axios({
                method: 'post',
                url: API_URL + '/update_event',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.buttonEditSubmit = 'Save change';
                    a.getList()
                    
                    if (response.data.status == 'success') {
                        $('#editItem').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    console.log( err)
                    a.buttonEditSubmit = 'Save change';
                    Swal.fire(
                        'Error!',
                        'Error',
                        'error'
                    )
                });
        },
        onFileChangeImage(e){
            const file = e.target.files[0];
            this.imageURL = URL.createObjectURL(file);          
            this.imageFile = this.$refs.imageFile.files[0];   
        },        
        onFileChangeEditImage(e){
            const file = e.target.files[0];
            this.imageEditURL = URL.createObjectURL(file);          
            this.imageEditFile = this.$refs.imageEditFile.files[0];   
        },
        processUpdateStatus(){

        },
        printInvoice(){

        }
    },
    components: {
        'table-list': TableList
    }
})

