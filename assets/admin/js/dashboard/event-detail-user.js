var BASE_URL = $constant.BASE_URL + 'event/detail/kelas/'
var API_URL = $constant.API_URL
var ID_EVENT = PAGE_ID

Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);

var TableList = {
    props: ['listData','paginationData', 'baseUrl', 'isLoad'],
    template: `
        <tbody>
        <tr v-if="!listData.length && !isLoad">
            <td colspan="8" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <b>{{data.email}}</b>
            </td>
            <td>
                <p><b>{{data.full_name | valueCheck}}</b></p>
            </td>
            <td>
                <p>{{data.eo_name | valueCheck}}</p>
            </td>
            <td>
                <p>{{data.user_group | userGroup}}</p>
            </td>
            <td>
                <p>{{data.phone | valueCheck}}</p>
            </td>                      
            <td>
                <label-status :status-data="data.status"></label-status>
            </td>
            <td style="text-align: center;">
                <button class="btn btn-sm btn-info mr-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
            </td>
        </tr>
        </tbody>
        `}

new Vue({
    el: '#app',
    data: {
        baseUrl: BASE_URL+ID_EVENT,
        kategoriUrl: $constant.BASE_URL + 'event/detail/kategori/'+ID_EVENT,
        isLoad:false,
        eventData:{},
        listData:{},
        paginationData:{},
        buttonSubmit:'Save',
        buttonEditSubmit:'Save change',
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        params:{
            event: ID_EVENT,
            limit: 10,
            status:''
        },
        paramsEvent:{
            id: ID_EVENT
        },
        progress: {
            value: 0,
            max: 100
        },
        form: {
            event: ID_EVENT
        }
    },
    mounted() {
        this.getEvent()
        this.initPage()
    },
    methods: {
        getEvent(){
            var a = this
            var params = jQuery.param(a.paramsEvent);
            a.progress.value = 30
            a.isLoad = true
            axios.get(API_URL + 'get_event?'+params, {})
            .then(response => {
                if (response.data.status == 'success') {
                    a.progress.value = 80
                    setTimeout(function () {
                        a.progress.value = 100
                        a.eventData = response.data.data.list[0]
                        a.isLoad = false
                    }, 500)
                } else {
                    a.isLoad = false
                    a.eventData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.eventData = {}
            })
        },
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', this.baseUrl+'?'+a);
            this.getList()
        },
        filterProcess(){
            delete  this.params.limit
            delete  this.params.event
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', this.baseUrl+'?'+a);
            this.params.limit = 10
            this.params.event = ID_EVENT
            this.getList()           
        },
        resetFormFilter(){
            this.params = {}
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', this.baseUrl+'?'+a);
            this.params.limit = 10
            this.params.event = ID_EVENT
            this.getList()           
        },
        getList(){
            var a = this
            a.isLoad = true
            a.progress.value = 0
            var params = jQuery.param(a.params);
            axios.get(API_URL + 'get_user?'+params, {})
            .then(response => {
                a.progress.value = 100
                if (response.data.status == 'success') {
                    setTimeout(function () {
                        a.listData = response.data.data.list
                        a.paginationData = response.data.data.pagination
                        a.isLoad = false
                    }, 500)
                } else {
                    a.isLoad = false
                    a.listData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.listData = {}
                //this.submitClaim(type)
            })
        },
        addNewUser() {
            this.form = {
                user_group: ''
            }
            $('#newUser').modal('show')
        },
        processAddUser() {
            var a = this
            var data = new FormData();
            $.each(a.form, function (i, v) {
                data.append(i, v)
            })
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                data: data,
                url: API_URL + 'set_user_event'
            }).then(response => {
                a.csrf.name = response.data.csrf.name
                a.csrf.value = response.data.csrf.hash
                if (response.data.status == 'success') {
                    Swal.fire('Sukses!', response.data.message, 'success')
                    $('#newUser').modal('hide')
                    a.resetFormFilter()
                } else {
                    a.resetFormFilter()
                    a.formNewMessage = (response.data.message) ? response.data.message : 'undifined error!'
                    a.isLoad = false
                }
            }).catch(err => {
                a.isLoad = false
                a.formNewMessage = 'undifined error!'
                if (err.response.data.message)
                    a.formNewMessage = err.response.data.message
                else
                    a.formNewMessage = err

                setTimeout(function () {
                    window.location.reload
                }, 2000)
            })
        },
    },
    components: {
        'table-list': TableList
    }
})

