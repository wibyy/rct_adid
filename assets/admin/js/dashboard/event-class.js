var BASE_URL = $constant.BASE_URL + 'event/class'
var API_URL = $constant.API_URL

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        curent_page: 1,
        selectedEvent:{},
        baseUrl: BASE_URL,
        isLoad: true,
        imgUrl:$constant.BASE_URL+'assets/uploads/images/',
        imageURL:'',
        imageEditURL:'',
        listEvent:'',
        buttonSubmit:'Save',
        buttonEditSubmit:'Save change',
        buttonSearch : 'Search', 
        params:{
            limit:10,
            id:EVENT_ID,
            status:'ALL'
        },
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        form:{},
        formEdit:{}
    },
    mounted() {
        this.getEvent()
        this.initPage()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()
        },
        filterProcess(){
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetFormFilter(){
            this.params = {}
            this.params.limit = 10
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL+'?'+a);
            this.getList()           
        },
        resetForm(){
            this.form = {}
            this.imageURL = ''
            this.$refs.imageFile.value=null
            this.buttonSubmit = 'Save'
        },
        getList(){
            this.isLoad = true
            this.getEventDetail(this.params.id)
            var a = jQuery.param(this.params);
            axios.get(API_URL + 'get_class?'+a, {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.listData = response.data.data.list
                    this.paginationData = response.data.data.pagination
                    this.isLoad = false
                } else {
                    this.isLoad = false
                    this.listData = {}
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        },
        getEvent(){
            axios.get(API_URL + 'get_event', {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.listEvent = response.data.data.list
                    this.getEventDetail(this.params.id)
                } else {
                    this.listEvent = {}
                }
            })
            .catch(error => {
                this.listEvent = {}
                //this.submitClaim(type)
            })
        },
        getEventDetail(id){
            var a = this
            $.each(a.listEvent, function( i, v ) {
                if(v.id == id){
                    a.selectedEvent = v
                }
            })
        },
        submitProcess(){
            this.buttonSubmit = 'Please wait ...';
            var a = this
            var data = new FormData();

            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            data.append('id', a.params.id);
            data.append('image', a.imageFile);
            axios({
                method: 'post',
                url: API_URL + '/set_class',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.resetForm()
                    a.getList()
                    if (response.data.status == 'success') {
                        $('#newItems').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    a.resetForm()
                    Swal.fire(
                        'Error!',
                        err.response.data.message,
                        'error'
                    )
                });
        },
        updateItems(data){
            this.formEdit = data
            this.imageEditURL = data.logo
            $('#editItem').modal('show')
        },
        updateItemProcess(){
            this.buttonEditSubmit = 'Please wait ...';
            var a = this
            
            console.log(this.formEdit)
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.formEdit, function( i, v ) {
                data.append(i, v)
            })
            if(a.imageEditFile){
                data.append('imageedit', a.imageEditFile);
            }
            axios({
                method: 'post',
                url: API_URL + '/update_class',
                data: data,
                headers: {'Content-Type': 'multipart/form-data' }})
                .then(function (response) {
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    a.buttonEditSubmit = 'Save change';
                    a.getList()
                    
                    if (response.data.status == 'success') {
                        $('#editItem').modal('hide')
                        Swal.fire(
                            'Success!',
                            response.data.message,
                            'success'
                        )
                    } else {
                        a.button = 'Save';
                        Swal.fire(
                            'Error!',
                            response.data.message,
                            'error'
                        )
                    }
                })
                .catch(err => {
                    console.log( err)
                    a.buttonEditSubmit = 'Save change';
                    Swal.fire(
                        'Error!',
                        'Error',
                        'error'
                    )
                });
        },
        onFileChangeImage(e){
            const file = e.target.files[0];
            this.imageURL = URL.createObjectURL(file);          
            this.imageFile = this.$refs.imageFile.files[0];   
        },        
        onFileChangeEditImage(e){
            const file = e.target.files[0];
            this.imageEditURL = URL.createObjectURL(file);          
            this.imageEditFile = this.$refs.imageEditFile.files[0];   
        },
    }
})

Vue.component('table-class', {
    props: ['listData','paginationData','baseUrl'],
    template: `
        <tbody>
        <tr v-if="!listData.length">
            <td colspan="8" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <img :src="data.logo " style="max-width:114px;max-height:42px;">
            </td>
            <td>
                <b>{{data.name}}</b>
                <p><small>{{data.sub_name}}</small></p>
            </td>
            <td>
                <p>{{data.price | rupiah}}</p>
            </td>
            <td>
                <p>{{data.scrutineering_admin | valueCheck}}</p>
            </td>
            <td>
                <p>{{data.scrutineering_teknis | valueCheck}}</p>
            </td>
            <td>
                <label-status :status-data="data.status"></label-status>
            </td>
            <td style="text-align: center;">
                <button class="btn btn-sm btn-info mr-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
            </td>
        </tr>
        </tbody>
        `
})
