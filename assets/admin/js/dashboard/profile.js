var BASE_URL = $constant.BASE_URL + 'profile/'
var API_URL = $constant.API_URL

Vue.component('v-select', VueSelect.VueSelect);
Vue.component('label-status', labelStatus);

new Vue({
    el: '#app',
    data: {
        profileData:{},
        isLoad: true,
        progress: {
            value: 0,
            max: 100
        },
    },
    mounted() {
        this.getList()
    },
    methods: {
        getList(){
            var a = this
            
            a.isLoad = true
            axios.get(API_URL + 'get_user_detail/'+a.g_userData.id, {})
            .then(response => {

                if (response.data.status == 'success') {
                    a.progress.value = 100
                    setTimeout(function () {
                        a.profileData = response.data.data
                        a.isLoad = false
                        a.progress.value = 0
                    }, 500)

                } else {
                    a.isLoad = false
                    a.profileData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.profileData = {}
            })
        },
    },
    components: {
  
    }
})

