var BASE_URL = $constant.BASE_URL + 'pembalap/'
var API_URL = $constant.API_URL


Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);

var TableList = {
    props: ['listData', 'paginationData', 'baseUrl', 'isLoad','isPerm','colSpan'],
    template: `
                <tbody>
                    <tr v-if="!listData.length && !isLoad">
                        <td :colspan="colSpan" style="text-align:center">
                            <p>No data record</p>
                        </td>
                    </tr>
                    <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
                        <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
                        <td>
                            <a style="cursor:pointer" :href="baseUrl+'detail/'+data.id"><b>{{data.full_name}}</b></a>
                        </td>
                        <td>
                            <p><b>{{data.email | valueCheck}}</b></p>
                        </td>
                        <td>
                            <p>{{data.kis_imi | valueCheck}}</p>
                        </td>
                        <td>
                            <p>{{data.pengprov_imi | valueCheck}}</p>
                        </td>                        
                        <td>
                            <p>{{data.phone | valueCheck}}</p>
                        </td>
                        <td v-if="isPerm.isEdited">
                            <button class="btn btn-sm btn-danger mr-2 mb-2"  @click="$emit('click-inactive', data)"><i class="fas fa-ban"></i></button>
                            <button class="btn btn-sm btn-info mr-2 mb-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
                        </td>
                    </tr>
                </tbody>`
}

new Vue({
    el: '#app',
    data: {
        listData: {},
        paginationData: {},
        curent_page: 1,
        isLoad: true,
        perm:{
            isEdited:'',
            isApproved:'',
        },
        baseUrl: BASE_URL,
        params: {
            limit: 10
        },
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        progress: {
            value: 0,
            max: 100
        },
        form: {
            user_group: ''
        },
        buttonSubmit: 'Simpan',
        formNewMessage: '',
        listProvinsi: '',
        optionProvinsi:[]
    },
    mounted() {
        this.initPage()
        this.perm.isEdited = this.getPermission('edit');
        this.perm.isApproved = this.getPermission('approval');
    },
    methods: {
        initPage() {
            var sPageURL = window.location.search.substring(1),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] != '')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
            this.getListProvinsi()
        },
        selectPage(page) {
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        filterProcess() {
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        resetFormFilter() {
            this.params = {}
            this.params.limit = 10
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', BASE_URL + '?' + a);
            this.getList()
        },
        getList() {
            this.isLoad = true
            var a = this
            var params = jQuery.param(this.params);
            a.progress.value = 0
            axios({
                method: 'get',
                url: API_URL + 'get_pembalap?' + params
            })
                .then(response => {
                    if (response.data.status == 'success') {
                        a.progress.value = 100
                        setTimeout(function () {
                            a.listData = response.data.data.list
                            a.paginationData = response.data.data.pagination
                            a.isLoad = false
                        }, 200)
                    } else {
                        a.isLoad = false
                        a.listData = {}
                        a.paginationData = {}
                    }
                })
                .catch(error => {
                    a.isLoad = false
                    a.listData = {}
                    a.paginationData = {}
                    //this.submitClaim(type)
                })
        },
        addNew() {
            $('#newItem').modal({backdrop: 'static', keyboard: false})
        },
        processNew() {
            var a = this
            a.isLoad = true
            var data = new FormData();
            $.each(a.form, function (i, v) {
                data.append(i, v)
            })
            data.append(a.csrf.name, a.csrf.value)
            axios({
                method: 'post',
                data: data,
                url: API_URL + 'set_pembalap'
            }).then(response => {
                a.csrf.name = response.data.csrf.name
                a.csrf.value = response.data.csrf.hash
                if (response.data.status == 'success') {
                    Swal.fire('Sukses!', response.data.message, 'success')
                    a.isLoad = false
                    $('#newItem').modal('hide')
                    a.resetFormFilter()
                } else {
                    a.resetFormFilter()
                    a.formNewMessage = (response.data.message) ? response.data.message : 'undifined error!'
                    a.isLoad = false
                }
            }).catch(err => {
                a.isLoad = false
                a.formNewMessage = 'undifined error!'
                if (err.response.data.message)
                    a.formNewMessage = err.response.data.message
                else
                    a.formNewMessage = err

                setTimeout(function () {
                    window.location.reload
                }, 2000)
            })
        },
        processUpdateStatus(e) {
        //     var a = this
        //     var data = new FormData();
        //     var status = e.status
        //     var msg 

        //     if(status=='1')
        //         status = 0
        //     else
        //         status = 1

        //     data.append('id', e.id_users)
        //     data.append('status', status)
        //     data.append(a.csrf.name, a.csrf.value)
        //     Swal.fire({
        //         title: 'Anda yakin?',
        //         text: "Status user akan dirubah!",
        //         icon: 'warning',
        //         showCancelButton: true,
        //         confirmButtonColor: '#3085d6',
        //         cancelButtonColor: '#d33',
        //         confirmButtonText: 'Ya, lanjutkan!'
        //     }).then((result) => {
        //         if(result.isConfirmed){
        //             axios({
        //                 method: 'post',
        //                 data: data,
        //                 url: API_URL + 'set_pembalap_status'
        //             }).then(response => {
        //                 a.csrf.name = response.data.csrf.name
        //                 a.csrf.value = response.data.csrf.hash
        //                 if (response.data.status == 'success') {
        //                     msg = response.data.message
        //                     Swal.fire('Sukses!',  msg, 'success')
        //                     a.getList()
        //                 } else {
        //                     msg = (response.data.message) ? response.data.message : 'undifined error!'
        //                     Swal.fire('Sukses!', msg, 'error')
        //                 }
        //             }).catch(err => {
        //                 msg = 'undifined error!'
        //                 if (err.response.data.message)
        //                     msg = err.response.data.message
        //                 else
        //                     msg = err

        //                 Swal.fire('Sukses!', msg, 'error')
        //                 setTimeout(function () {
        //                     window.location.reload
        //                 }, 2000)
        //             })
        //         } 
        //     })
        },
        getListProvinsi(){
            var a = this
            axios.get(API_URL + 'get_provinsi', {})
            .then(response => {
                if (response.data.status == 'success') {
                    a.listProvinsi = response.data.data.list
                    for (i = 0; i < a.listProvinsi.length; i++) {
                        a.optionProvinsi.push({
                            label: a.listProvinsi[i].nama,
                            code: a.listProvinsi[i].nama,
                        })
                    }
                }else {
                    a.listProvinsi = {}
                }
            })
            .catch(error => {
                a.listProvinsi = {}
            })
        }

    },
    components: {
        'table-list': TableList
    }
})
