var BASE_URL = $constant.BASE_URL + 'event/detail/info/'
var API_URL = $constant.API_URL
var ID_EVENT = PAGE_ID

Vue.component('v-select', VueSelect.VueSelect);
Vue.component('pagination', pagination);
Vue.component('label-status', labelStatus);

var InfoDetail = {
    props: ['listData','paginationData', 'baseUrl', 'isLoad'],
    template: `
        <tbody>
        <tr v-if="!listData.length && !isLoad">
            <td colspan="7" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData" v-bind:class="{'loading' : isLoad}">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <img :src="data.logo " style="max-width:114px;max-height:42px;">
            </td>
            <td>
                <a style="cursor:pointer" :href="baseUrl+'detail/'+data.id"><b>{{data.label}}</b></a>
                <p><small>{{data.sub_label}}</small></p>
            </td>
            <td>
                <p>{{data.lokasi}}</p>
            </td>
            <td>
                <p>{{data.date_start | localShortDate}} - {{data.date_end | localShortDate}}</p>
            </td>
            <td style="text-align:center">
                <label-status :status-data="data.status"></label-status>
            </td>
            <td style="text-align: center;">
                <button class="btn btn-sm btn-success mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="data.status==2||data.status==0"><i class="fas fa-check"></i></button>
                <button class="btn btn-sm btn-danger mr-2 mb-2"  @click="$emit('click-inactive', data)" v-if="data.status==1"><i class="fas fa-ban"></i></button>
                <button class="btn btn-sm btn-info mr-2 mb-2"  @click="$emit('click-edit', data)"><i class="fas fa-edit"></i></button>
            </td>
        </tr>
        </tbody>
        `}

new Vue({
    el: '#app',
    data: {
        baseUrl: BASE_URL,
        isLoad:false,
        eventData:{},
        csrf: {
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        params:{
            id: ID_EVENT
        },
        progress: {
            value: 0,
            max: 100
        },
    },
    mounted() {
        this.getEvent()
    },
    methods: {
        getEvent(){
            var a = this
            var params = jQuery.param(a.params);
            a.progress.value = 30
            a.isLoad = true
            axios.get(API_URL + 'get_event?'+params, {})
            .then(response => {
                if (response.data.status == 'success') {
                    a.progress.value = 80
                    setTimeout(function () {
                        a.progress.value = 100
                        a.eventData = response.data.data.list[0]
                        a.isLoad = false
                    }, 500)
                } else {
                    a.isLoad = false
                    a.eventData = {}
                }
            })
            .catch(error => {
                a.isLoad = false
                a.eventData = {}
            })
        }
    },
    components: {
        'info-detail': InfoDetail
    }
})

