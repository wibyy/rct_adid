var API_URL = $constant.API_URL + 'region/'

new Vue({
    el: '#app',
    data: {
        listData:{},
        paginationData:{},
        curent_page: 1,
        isLoad: true,
        params:{},
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        }
    },
    mounted() {
        this.initPage()
    },
    methods: {
        initPage(){
            var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;
            for (i = 0; i < sURLVariables.length; i++) {
                sParameterName = sURLVariables[i].split('=');
                if(sParameterName[0]!='')
                    this.params[sParameterName[0]] = sParameterName[1] === undefined ? '' : decodeURIComponent(sParameterName[1])

            }
            this.getList()
        },
        selectPage(page){
            this.params.page = page
            var a = jQuery.param(this.params);
            window.history.pushState({}, '', API_URL+'?'+a);
            this.getList()
        },
        getList(){
            this.isLoad = true
            var a = jQuery.param(this.params);
            axios.get(API_URL + 'get_provinsi_ajax?'+a, {})
            .then(response => {
                if (response.data.status == 'success') {
                    this.listData = response.data.data.list
                    this.paginationData = response.data.data.pagination
                    this.isLoad = false
                }else {
                    this.isLoad = false
                    this.listData = {}
                }
            })
            .catch(error => {
                this.isLoad = false
                this.listData = {}
                //this.submitClaim(type)
            })
        }
    }
})

Vue.component('table-list', {
    props: ['listData','paginationData'],
    template: `
        <tbody>
        <tr v-if="!listData.length">
            <td colspan="3" style="text-align:center">
                <p>No data record</p>
            </td>
        </tr>
        <tr v-for="(data,index) in listData">
            <th scope="row">{{((paginationData.current - 1 ) *  paginationData.per_page) + index+1}}</th>
            <td>
                <p>{{data.id}}</p>
            </td>
            <td>
                <p>{{data.nama}}</p>
            </td>
        </tr>
        </tbody>


        `
})
