var API_URL = $constant.API_URL

new Vue({
    el: '#app',
    data: {
        isLoad: false,
        buttonSubmit:'Next',
        message:'',
        csrf:{
            name: $constant.CSRF_NAME,
            value: $constant.CSRF_VALUE
        },
        progress: {
            value: 0,
            max: 100
        },
        form:{}
    },
    mounted() {
    },
    methods: {
        submitProcess(){
            this.buttonSubmit = 'Loading...';
            var a = this
            var data = new FormData();
            data.append(a.csrf.name, a.csrf.value)
            $.each(a.form, function( i, v ) {
                data.append(i, v)
            })
            a.isLoad = true
            a.progress.value = 30
            axios({
                method: 'post',
                url: API_URL + 'sign_in',
                data: data
            })
                .then(function (response) {
                    a.progress.value = 60
                    a.csrf.name = response.data.csrf.name
                    a.csrf.value = response.data.csrf.hash
                    if(response.data.status=='success'){
                        a.progress.value = 80
                        const parsed = JSON.stringify(response.data.data);
                        setTimeout(function() {
                            a.buttonSubmit = 'Success...';
                            a.progress.value = 100
                            localStorage.setItem('userdata', parsed);
                            window.location.href = $constant.BASE_URL
                        }, 1500)
                    } else {
                        a.buttonSubmit = 'Next'
                        a.isLoad = false
                        a.progress.value = 0
                        a.form = {}
                        a.message       = response.data.message
                    }
                })
                .catch(err => {
                    a.buttonSubmit = 'Next'
                    a.isLoad = false
                    a.form = {}
                    if(err.response.data.message)
                        a.message       = err.response.data.message
                    else
                        a.message       = err
                });
        }
    }
})